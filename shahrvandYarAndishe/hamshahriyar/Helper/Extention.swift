//
//  Extention.swift
//  hamshahriyar
//
//  Created by apple on 10/22/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit
import SystemConfiguration

extension UIColor
{
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat)
    {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
}

extension UIColor {
    
    struct lightColor {
        static let light = UIColor(r: 96, g: 125, b: 139)
    }
    
    struct darkColor {
        static let dark = UIColor(r: 69, g: 90, b: 100)
    }
    
    struct purpuleColor {
        static let purple = UIColor(r: 124, g: 77, b: 255)
    }
    
    struct blueColor {
        static let blue = UIColor(r: 10, g: 96, b: 238)
    }
    struct greenColor {
        static let green = UIColor(r: 68, g: 180, b: 42)
    }
    struct orangeColor {
        static let orange = UIColor(r: 236, g: 193, b: 44)
    }
    struct baseColor {
        static let base = UIColor(r: 40, g: 121, b: 89)
    }
    struct baseColor2 {
        static let base = UIColor(r: 78, g: 174, b: 136)
    }
    
}

extension UIView
{
    func addConstraintWithFormat(format: String, views: UIView...)
    {
        var viewsDic = [String: UIView]()
        for (index, view) in views.enumerated()
        {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDic[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDic))
    }
}

extension UIImage {
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(origin: CGPoint(x: 0,y :size.height - (lineWidth) ), size: CGSize(width: size.width, height: lineWidth)))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
extension UIApplication {
    
    /// Returns the status bar UIView
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
}

let imageCatch = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    func loadImageUsingURLString(urlString: String) {
        
        
        guard let url = URL(string: urlString) else {return}
        
        if let imageFromCatch = imageCatch.object(forKey: urlString as AnyObject) as? UIImage {
            
            DispatchQueue.main.async {
                self.image = imageFromCatch
                return
            }
            
        }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            DispatchQueue.main.async {
                
                if let dataa = data {
                    
                    if let imageToCatch = UIImage(data: dataa) {
                        imageCatch.setObject(imageToCatch, forKey: urlString as AnyObject)
                        self.image = imageToCatch
                    }
                    
                }
                
            }
            
        })
        task.resume()
    }
}

extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.boldSystemFont(ofSize: 18)]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 16)]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
}

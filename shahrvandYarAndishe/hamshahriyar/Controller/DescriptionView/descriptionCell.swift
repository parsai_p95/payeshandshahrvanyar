//
//  descriptionCell.swift
//  hamshahriyar
//
//  Created by apple on 10/22/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit

class descriptionCell: baseCell {
    
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 22)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    override func setupViews() {
        super.setupViews()
        
        backgroundColor = .white
        
        addSubview(titleLabel)
        addSubview(imageView)
        addSubview(descriptionLabel)
        
        titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: getSizeScreen().height / 10).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 10).isActive = true
        titleLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width).isActive = true
        
        imageView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: getSizeScreen().height / 8).isActive = true
        imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 2).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2).isActive = true
        
        descriptionLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: getSizeScreen().height / 8).isActive = true
        descriptionLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        descriptionLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 8).isActive = true
        descriptionLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width).isActive = true
        
        
        
    }
    
}

//
//  sendMessageController.swift
//  hamshahriyar
//
//  Created by apple on 10/22/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit
import CDAlertView
import Alamofire
import SwiftyJSON

class sendMessageController: UIViewController, UITextFieldDelegate {
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    let whiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.purpuleColor.purple
        label.text = "شماره تلفن خود را وارد کنید"
        label.font = UIFont.systemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }()
    
    lazy var phoneNumTextField: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 18)
        tf.textColor = UIColor.purpuleColor.purple
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.keyboardType = .phonePad
        tf.textAlignment = .center
        tf.delegate = self
        return tf
    }()
    
    let bottomTextFieldLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.purpuleColor.purple
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let footerLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "ما برای شما کد تایید ارسال می کنیم"
        label.font = UIFont.systemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }()
    
    let sendButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor.purpuleColor.purple
        btn.tintColor = .white
        btn.layer.masksToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "rightArrow")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        return btn
    }()
    
    lazy var VefyingCodeView: verfyingCodeView = {
        let view = verfyingCodeView()
        view.SendMessageController = self
        view.isHidden = true
        return view
    }()
    
    var bottomConstraint: NSLayoutConstraint?
    var bottomPadding: CGFloat?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handelKeyBoardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handelKeyBoardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        view.backgroundColor = UIColor.baseColor.base
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handelDismissKeyboard)))
        whiteView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handelDismissKeyboard)))
        setupStatusBar()
        setupViews()
        
        
    }
    
    @objc fileprivate func handelDismissKeyboard() {
        phoneNumTextField.endEditing(true)
    }
    
    fileprivate func setupStatusBar() {
        
        let statusBarView = UIView()
        statusBarView.backgroundColor = .black
        view.addSubview(statusBarView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: statusBarView)
        view.addConstraintWithFormat(format: "V:|[v0(\(getSizeScreen().height / 25))]", views: statusBarView)
        UIApplication.shared.statusBarStyle = .lightContent
        
    }
    
    fileprivate func setupViews() {
        
        
        
        view.addSubview(whiteView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: whiteView)
        view.addConstraintWithFormat(format: "V:[v0(\(getSizeScreen().height / 2.3))]", views: whiteView)
        bottomConstraint = NSLayoutConstraint(item: whiteView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)
        
        titleLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 45)
        whiteView.addSubview(titleLabel)
        titleLabel.centerXAnchor.constraint(equalTo: whiteView.centerXAnchor).isActive = true
        titleLabel.topAnchor.constraint(equalTo: whiteView.topAnchor, constant: getSizeScreen().height / 20).isActive = true
        titleLabel.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: view.frame.height / 20 ).isActive = true
        
        whiteView.addSubview(phoneNumTextField)
        phoneNumTextField.centerXAnchor.constraint(equalTo: whiteView.centerXAnchor).isActive = true
        phoneNumTextField.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
        phoneNumTextField.widthAnchor.constraint(equalToConstant: view.frame.width / 1.5).isActive = true
        phoneNumTextField.heightAnchor.constraint(equalToConstant: view.frame.height / 20 ).isActive = true
        
        whiteView.addSubview(bottomTextFieldLine)
        bottomTextFieldLine.centerXAnchor.constraint(equalTo: whiteView.centerXAnchor).isActive = true
        bottomTextFieldLine.topAnchor.constraint(equalTo: phoneNumTextField.bottomAnchor).isActive = true
        bottomTextFieldLine.widthAnchor.constraint(equalToConstant: view.frame.width / 1.5).isActive = true
        bottomTextFieldLine.heightAnchor.constraint(equalToConstant: 1 ).isActive = true
        
        footerLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 45)
        whiteView.addSubview(footerLabel)
        footerLabel.centerXAnchor.constraint(equalTo: whiteView.centerXAnchor).isActive = true
        footerLabel.topAnchor.constraint(equalTo: bottomTextFieldLine.bottomAnchor, constant: 20).isActive = true
        footerLabel.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        footerLabel.heightAnchor.constraint(equalToConstant: view.frame.height / 20 ).isActive = true
        
        whiteView.addSubview(sendButton)
        sendButton.addTarget(self, action: #selector(handeSendMessage), for: .touchUpInside)
        sendButton.centerXAnchor.constraint(equalTo: whiteView.centerXAnchor).isActive = true
        sendButton.bottomAnchor.constraint(equalTo: whiteView.bottomAnchor, constant: -(view.frame.height / 20)).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: view.frame.height / 12).isActive = true
        sendButton.heightAnchor.constraint(equalToConstant: view.frame.height / 12 ).isActive = true
        sendButton.layer.cornerRadius = view.frame.height / 24
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            self.phoneNumTextField.becomeFirstResponder()
        }
        
    }
    
    fileprivate func showErrorPhoneNumAlert(message: String, titleAction: String) {
        
        let alert = CDAlertView(title: "", message: message, type: .warning)
        alert.messageTextColor = UIColor.purpuleColor.purple
        let doneAction = CDAlertViewAction(title: titleAction, font: UIFont.systemFont(ofSize: 14), textColor: .white, backgroundColor: UIColor.purpuleColor.purple) { (action) -> Bool in
            
            self.phoneNumTextField.text = ""
            return true
            
        }
        alert.add(action: doneAction)
        alert.show()
    }
    
    @objc private func handeSendMessage() {
        
        if phoneNumTextField.text == "" {
            showErrorPhoneNumAlert(message: "شماره تلفن را فراموش کرده اید", titleAction: "تأیید")
        }
        else if (phoneNumTextField.text?.count)! < 11 {
            showErrorPhoneNumAlert(message: "شماره تلفن صحیح نیست", titleAction: "امتحان مجدد")
        }
        else {
            if let phoneNum = phoneNumTextField.text {
                shareInterface.SendSMS(mobile: phoneNum)
                phoneNumber = phoneNum
                let userDefult = UserDefaults()
                userDefult.setValue(phoneNumber, forKeyPath: "phoneNum")
                view.addSubview(VefyingCodeView)
                VefyingCodeView.frame = view.frame
                VefyingCodeView.isHidden = false
                handelDismissKeyboard()
                
                
            }
        }
        
    }
    
    
    
//    lazy var profileC: profileController = {
//        let pr = profileController()
//        pr.sendMessage = self
//        return pr
//    }()
    var phoneNumber = ""
    
    
    
    func showProfileController(code: String) {
        
        let userDefult = UserDefaults()
        userDefult.setValue(phoneNumber, forKeyPath: "phoneNum")
        verfyingCode(code: code, phoneNumL: phoneNumber)
        
    }
    
    
    
    fileprivate func verfyingCode(code: String, phoneNumL: String)  {
        
        let userDefult = UserDefaults()
        let sendSmsUrlString = baseURLString + "codechk.php?code=\(code)&mobile=\(phoneNumL)"
        guard let url = URL(string: sendSmsUrlString) else {return}
        Alamofire.request(url, method: .get).responseJSON { (response) in
            
            
            
            guard response.result.isSuccess else {
                let err = String(describing: response.result.error)
                print("Error while fetching remote rooms: \(err)")
                return
            }
            
            
            if let jsons = response.data {
                
                let Json = JSON(jsons)
                let array = Json["array"]
                print(array)
                for key in array {
                    let userInfoJson = key.1
                    let id = userInfoJson["id"].string
                    let code = userInfoJson["code"].string
                    let status = userInfoJson["status"].string
                    let idd = userInfoJson["id"].string
                    let userid = userInfoJson["userid"].string
                    
                    if userid == "" {
                        userDefult.setValue(id, forKey: "userID")
                        let pc = profileController()
                        userDefult.setValue(true, forKey: "startAppMode")
                        userDefult.setValue(true, forKey: "startHelpTicketMode")
                        let root = UINavigationController(rootViewController: pc)
                        self.present(root, animated: true, completion: nil)
                    }
                    else {
                        userDefult.setValue(idd, forKey: "id")
                        userDefult.setValue(id, forKey: "userID")
                        userDefult.setValue(code, forKey: "userCode")
                        userDefult.setValue(status, forKey: "userStatus")
                        userDefult.setValue(userid, forKey: "userid")
                        userDefult.setValue(true, forKey: "userSignIn")
                        userDefult.setValue(true, forKey: "startAppMode")
                        userDefult.setValue(true, forKey: "startHelpTicketMode")
                        let home = HomeController()
                        self.present(home, animated: true, completion: nil)
                    }
                    

                    
                }
                
                let faild = Json["array"]
                if faild == "failed" {
                    
                    let alert = CDAlertView(title: "", message: "کد اشتباه است، دوباره امتحان کنید", type: .warning)
                    let action = CDAlertViewAction(title: "تأیید")
                    alert.add(action: action)
                    alert.show()
                    
                }
//                else {
//
//                    let checkStatusUrlString = baseURLString + "chkstatus.php?mobile=\(phoneNumL)"
//                    guard let StatusUrl = URL(string: checkStatusUrlString) else {return}
//                    Alamofire.request(StatusUrl, method: .get).responseJSON(completionHandler: { (responeee) in
//
//                        if let data = responeee.data {
//                            let json = JSON(data)
//                            let arrayyy = json["array"].string
//                            print(arrayyy)
//                            if arrayyy == nil {
////                                let pc = profileController()
////                                userDefult.setValue(true, forKey: "startAppMode")
////                                let root = UINavigationController(rootViewController: pc)
////                                self.present(root, animated: true, completion: nil)
//                            }
//                            else {

//                            }
//
//                        }
//
//                    })
//
//
//                }
                
            }
        }
        
        
    }
    
    
    
    @objc func handelKeyBoardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let keyBoardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
            let keyboardRectangle = keyBoardFrame.cgRectValue
            
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            
            bottomPadding = 0
            
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                bottomPadding = window?.safeAreaInsets.bottom
            }
            
            bottomConstraint?.constant = isKeyboardShowing ? -keyboardRectangle.height + bottomPadding!: 0
            
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
                
            }) { (completion) in
                
                
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 11
    }
    
}

//
//  ViewController.swift
//  hamshahriyar
//
//  Created by apple on 10/22/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import CDAlertView

class launchViewController: UIViewController {
    
    let loader: NVActivityIndicatorView = {
        let loader = NVActivityIndicatorView(frame: .zero)
        let loaderType = NVActivityIndicatorType.ballRotateChase
        loader.type = loaderType
        loader.color = .darkGray
        loader.padding = 5
        loader.translatesAutoresizingMaskIntoConstraints = false
        return loader
    }()
    
    let imageView: UIImageView = {
        let image = UIImage(named: "bkg")
        let iv = UIImageView(image: image)
        return iv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        if !isVPNConnected() {
            getVersionOfApp()
        }
        else {
            showAlert(title: "لطفا فیلتر شکن خود را خاموش کنید", message: "چنانچه در ایران هستید از قطع بودن فیلتر شکن خود مطمئن شوید")
        }
    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    fileprivate func  setupViews(){

        view.addSubview(imageView)
        imageView.frame = view.frame
        
        
        view.addSubview(loader)
        loader.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loader.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -getSizeScreen().height / 8).isActive = true
        loader.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        loader.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        
    }
    
    fileprivate func showAboutAppViewController() {

        //        let layout = UICollectionViewFlowLayout()
        let cv  = DescriptionController()
        let userDefult = UserDefaults()
        let userSignIN = userDefult.value(forKey: "userSignIn") as? Bool

        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {

            if userSignIN ?? false {
                let home = HomeController()
                self.present(home, animated: true, completion: nil)
            }
            else {
                self.present(cv, animated: true, completion: nil)
            }

        }
    }
    
    fileprivate func showAlertForUpdate() {
        let alert = UIAlertController(title: "", message: "ورژن جدید برنامه موجود است، آیا مایل به نصب آن هستید؟", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "بله", style: .default, handler: { action in
            if let url = URL(string: "https://new.sibapp.com/applications/hamshahriyar") {
                UIApplication.shared.open(url, options: [:])
            }
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "خیر", style: .default, handler: { action in
            self.showAboutAppViewController()
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    func isVPNConnected() -> Bool {
        let cfDict = CFNetworkCopySystemProxySettings()
        let nsDict = cfDict!.takeRetainedValue() as NSDictionary
        let keys = nsDict["__SCOPED__"] as! NSDictionary
        
        for key: String in keys.allKeys as! [String] {
            if (key == "tap" || key == "tun" || key == "ppp" || key == "ipsec" || key == "ipsec0" || key == "l2tp") {
                return true
            }
        }
        return false
    }
    
    fileprivate func getVersionOfApp() {
        loader.startAnimating()
        let urlString =  baseURLString + "show_version.php"
        guard let url = URL(string: urlString) else {return}
        Alamofire.request(url, method: .get).responseJSON { (response) in
            if let data = response.result.value {
                let json = JSON(data)
                if let version = json["version"].string {
                    if let ver = UserDefaults.standard.value(forKey: "version") as? String {
                        if ver == version {
                            self.showAboutAppViewController()
                        }
                        else {
                            UserDefaults.standard.setValue(version, forKey: "version")
                            self.showAlertForUpdate()
                        }
                    }
                    else {
                        UserDefaults.standard.setValue(version, forKey: "version")
                        self.showAboutAppViewController()
                    }
                }
                self.loader.stopAnimating()
            }
            if let err = response.result.error {
                let error = "\(err.localizedDescription)"
                self.loader.stopAnimating()
                if error == "An SSL error has occurred and a secure connection to the server cannot be made." {
                    self.showAlert(title: "لطفا فیلتر شکن خود را خاموش کنید", message: "چنانچه در ایران هستید از قطع بودن فیلتر شکن خود مطمئن شوید")
                }
                else if error == "A server with the specified hostname could not be found." {
                    self.showAlert(title: "اخطار", message: "دسترسی امکان پذیر نیست، دوباره امتحان کنید.")
                }
                else if error == "The Internet connection appears to be offline." {
                    self.showAlert(title: "اخطار", message: "اینترنت موبایل خود را چک کنید")
                }
                else if error == "The operation couldn't be completed." {
                    self.showAlert(title: "اخطار", message: "ارتباط برقرار نشد، دوباره امتحان کنید")
                }
                else if error == "The request timed out." {
                    self.showAlert(title: "اخطار", message: "ارتباط برقرار نشد، دوباره امتحان کنید")
                }
            }
        }
    }
    
    func showAlert(title: String, message: String) {
        
        let alert = CDAlertView(title: title, message: message, type: .warning)
        let action = CDAlertViewAction(title: "تلاش مجدد", font: UIFont.systemFont(ofSize: getSizeScreen().height / 57), textColor: UIColor.purpuleColor.purple, backgroundColor: .white) { (action) -> Bool in
            self.getVersionOfApp()
            return true
        }
        alert.add(action: action)
        alert.show()
        
    }
    
    
}


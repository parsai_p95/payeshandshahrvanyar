//
//  editUnsendReqView.swift
//  hamshahriyar
//
//  Created by apple on 11/2/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit
import CDAlertView
import AVFoundation

class editUnsendReqView: UIView, AVAudioRecorderDelegate {
    
    var unsendView: unsendSelectCellWhiteView?
    
    let issuerRequsestTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.textColor = .black
        tf.textAlignment = .right
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.subjectColor = UIColor.blueColor.blue
        tf.underLineColor = UIColor.blueColor.blue
        return tf
    }()
    
    let addressTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.textColor = .black
        tf.textAlignment = .right
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.subjectColor = UIColor.blueColor.blue
        tf.underLineColor = UIColor.blueColor.blue
        return tf
    }()
    
    let descriptionTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.textColor = .black
        tf.textAlignment = .right
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.subjectColor = UIColor.blueColor.blue
        tf.underLineColor = UIColor.blueColor.blue
        return tf
    }()
    
    let pinFileView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2.5
        return view
    }()
    
    let titleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 87, g: 155, b: 159)
        view.layer.cornerRadius = 10
        return view
    }()
    
    let pinFileLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "پیوست فایل"
        lbl.textColor = .white
        lbl.textAlignment = .left
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let pinImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "verticalPin")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .white
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let imageFileLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "عکس"
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let imageFileLine: UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let imageBudgeView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let imageBudgeLabel: UILabel = {
        let lbl = UILabel()
        lbl.isHidden = true
        lbl.textColor = .white
        lbl.backgroundColor = .red
        lbl.layer.masksToBounds = true
        lbl.textAlignment = .center
        return lbl
    }()
    
    let chooseImageFileButton: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "cameraFill")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .black
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    //***********
    let videoFileLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "فیلم"
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let videoFileLine: UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let videoBudgeView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let videoBudgeLabel: UILabel = {
        let lbl = UILabel()
        lbl.isHidden = true
        lbl.textColor = .white
        lbl.backgroundColor = .red
        lbl.layer.masksToBounds = true
        lbl.textAlignment = .center
        return lbl
    }()
    
    let chooseVideoFileButton: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "videoFill")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .black
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    //***********
    let voiceFileLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "صدا"
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let voiceBudgeView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let voiceBudgeLabel: UILabel = {
        let lbl = UILabel()
        lbl.isHidden = true
        lbl.textColor = .white
        lbl.backgroundColor = .red
        lbl.layer.masksToBounds = true
        lbl.textAlignment = .center
        return lbl
    }()
    
    let chooseVoiceFileButton: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "voiceFill")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .black
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let addRequestButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor(r: 10, g: 96, b: 238)
        btn.tintColor = .white
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 5
        btn.setTitle("ثبت درخواست", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let returnButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.tintColor = .gray
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 5
        btn.setTitle("برگشت", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
        
    }
    
    let userDefult = UserDefaults()
    
    var selectIndex: Int?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        
        getInfoRequest()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handelKeyBoardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handelKeyBoardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        backgroundColor = .white
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handelDismissKeyboard))
        addGestureRecognizer(gesture)
        setupViews()
    }
    
    var subject = ""
    var problemType = ""
    var address = ""
    var descriptions = ""
    
    var subjectArray: [String] = []
    var adressArray: [String] = []
    var DesArray: [String] = []
    var problemTypeArray: [String] = []
    var sendRequsetImagesArray: [Data] = []
    var SendRequsetImagesArray: [[Data]] = []
    var voiceDataArray: [Data] = []
    
    func getInfoRequest() {
        
        if let index = SelectIndexCell {
            selectIndex = index
        }
        subjectArray = userDefult.value(forKey: "titleArray") as! [String]
        
        adressArray = userDefult.value(forKey: "addressArray") as! [String]
        DesArray = userDefult.value(forKey: "desArray") as! [String]
        problemTypeArray = userDefult.value(forKey: "ProblemTypeArray") as! [String]
        SendRequsetImagesArray = userDefult.value(forKey: "imageSelectArray") as! [[Data]]
        voiceDataArray = userDefult.value(forKey: "voiceSelectArray") as! [Data]
        
        subject = subjectArray[selectIndex!]
        address = adressArray[selectIndex!]
        descriptions = DesArray[selectIndex!]
        
        issuerRequsestTextField.text = subject
        addressTextField.text = address
        descriptionTextField.text = descriptions
    }
    
    fileprivate func editRequst() {
        
        problemType = (unsendView?.problemType)!
        subjectArray[selectIndex!] = issuerRequsestTextField.text!
        problemTypeArray[selectIndex!] = problemType
        adressArray[selectIndex!] = addressTextField.text!
        DesArray[selectIndex!] = descriptionTextField.text!
        sendRequsetImagesArray = (unsendView?.unsendView?.FllowUp?.sendRequsetImagesArray)!
        SendRequsetImagesArray[selectIndex!] = sendRequsetImagesArray
        
        if let myVoice = voiceData {
            voiceDataArray[selectIndex!] = myVoice
        }
        
        
        
        userDefult.setValue(subjectArray, forKey: "titleArray")
        userDefult.setValue(adressArray, forKey: "addressArray")
        userDefult.setValue(DesArray, forKey: "desArray")
        userDefult.setValue(problemTypeArray, forKey: "ProblemTypeArray")
        userDefult.setValue(SendRequsetImagesArray, forKey: "imageSelectArray")
        userDefult.setValue(voiceDataArray, forKey: "voiceSelectArray")
        
        
        
        unsendView?.unsendView?.getCoreDataUnSendReq()
        unsendView?.handelDismissBlackView()
        
        
    }
    
    @objc func handelKeyBoardNotification(notification: NSNotification) {
        
        let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
        if isKeyboardShowing {
            //            CityProblem?.goBottomOfScrollView(mode: isKeyboardShowing)
        }
        else {
            //            CityProblem?.goBottomOfScrollView(mode: isKeyboardShowing)
        }
        UIView.animate(withDuration: 1.5, delay: 0, options: .curveEaseOut, animations: {
            
            self.layoutIfNeeded()
            
        }) { (completion) in
            
            
        }
        
    }
    @objc func handelDismissKeyboard() {
        self.endEditing(true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupViews() {
        
        addSubview(issuerRequsestTextField)
        issuerRequsestTextField.attributedPlaceholder = NSAttributedString(string: "عنوان درخواست", attributes: [
            .foregroundColor: UIColor.gray,
            .font: UIFont.boldSystemFont(ofSize: getSizeScreen().height / 45)
            ])
        issuerRequsestTextField.topAnchor.constraint(equalTo: self.topAnchor, constant: 30).isActive = true
        issuerRequsestTextField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        issuerRequsestTextField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        issuerRequsestTextField.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 25).isActive = true
        
        let personalInfoViewHeight = (7.5 * (getSizeScreen().height / 20))
        
        addSubview(pinFileView)
        pinFileView.topAnchor.constraint(equalTo: issuerRequsestTextField.bottomAnchor, constant: (getSizeScreen().height / 30)).isActive = true
        pinFileView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: (getSizeScreen().height / 70)).isActive = true
        pinFileView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -(getSizeScreen().height / 70)).isActive = true
        pinFileView.heightAnchor.constraint(equalToConstant: personalInfoViewHeight).isActive = true
        
        pinFileView.addSubview(titleView)
        pinFileView.addConstraintWithFormat(format: "H:|[v0]|", views: titleView)
        pinFileView.addConstraintWithFormat(format: "V:|[v0(\(getSizeScreen().height / 18))]", views: titleView)
        
        titleView.addSubview(pinFileLabel)
        pinFileLabel.rightAnchor.constraint(equalTo: titleView.rightAnchor).isActive = true
        pinFileLabel.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
        pinFileLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3.5).isActive = true
        
        titleView.addSubview(pinImageView)
        pinImageView.rightAnchor.constraint(equalTo: pinFileLabel.leftAnchor).isActive = true
        pinImageView.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
        pinImageView.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 30).isActive = true
        pinImageView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 30).isActive = true
        
        pinFileView.addSubview(imageBudgeView)
        imageBudgeView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: getSizeScreen().height / 50).isActive = true
        imageBudgeView.leftAnchor.constraint(equalTo: pinFileView.leftAnchor, constant: getSizeScreen().width / 20).isActive = true
        imageBudgeView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        imageBudgeView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        let imageGesture = UITapGestureRecognizer(target: self, action: #selector(handelImagePicker))
        imageBudgeView.addGestureRecognizer(imageGesture)
        
        pinFileView.addSubview(imageFileLabel)
        imageFileLabel.centerYAnchor.constraint(equalTo: imageBudgeView.centerYAnchor).isActive = true
        imageFileLabel.rightAnchor.constraint(equalTo: pinFileView.rightAnchor, constant: -getSizeScreen().width / 20).isActive = true
        imageFileLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4.5).isActive = true
        imageFileLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 40)
        
        imageBudgeView.addSubview(imageBudgeLabel)
        imageBudgeView.addSubview(chooseImageFileButton)
        
        chooseImageFileButton.leftAnchor.constraint(equalTo: imageBudgeView.leftAnchor, constant: 5).isActive = true
        chooseImageFileButton.centerYAnchor.constraint(equalTo: imageBudgeView.centerYAnchor).isActive = true
        chooseImageFileButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        chooseImageFileButton.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        
        
        
        imageBudgeLabel.frame = CGRect(x: getSizeScreen().height / 19, y: -5, width: getSizeScreen().width / 18, height: getSizeScreen().width / 18)
        imageBudgeLabel.layer.cornerRadius = getSizeScreen().width / 36
        imageBudgeLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        pinFileView.addSubview(imageFileLine)
        imageFileLine.rightAnchor.constraint(equalTo: pinFileView.rightAnchor, constant: -7).isActive = true
        imageFileLine.leftAnchor.constraint(equalTo: pinFileView.leftAnchor, constant: 7).isActive = true
        imageFileLine.topAnchor.constraint(equalTo: imageBudgeView.bottomAnchor, constant: getSizeScreen().height / 50).isActive = true
        imageFileLine.heightAnchor.constraint(equalToConstant: 1.5).isActive = true
        
        pinFileView.addSubview(videoBudgeView)
        videoBudgeView.topAnchor.constraint(equalTo: imageFileLine.bottomAnchor, constant: getSizeScreen().height / 50).isActive = true
        videoBudgeView.leftAnchor.constraint(equalTo: pinFileView.leftAnchor, constant: getSizeScreen().width / 20).isActive = true
        videoBudgeView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        videoBudgeView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        //        let videoGesture = UITapGestureRecognizer(target: self, action: #selector(handelVideoPicker))
        //        videoBudgeView.addGestureRecognizer(videoGesture)
        
        pinFileView.addSubview(videoFileLabel)
        videoFileLabel.centerYAnchor.constraint(equalTo: videoBudgeView.centerYAnchor).isActive = true
        videoFileLabel.rightAnchor.constraint(equalTo: pinFileView.rightAnchor, constant: -getSizeScreen().width / 20).isActive = true
        videoFileLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4.5).isActive = true
        videoFileLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 40)
        
        videoBudgeView.addSubview(videoBudgeLabel)
        videoBudgeView.addSubview(chooseVideoFileButton)
        
        chooseVideoFileButton.leftAnchor.constraint(equalTo: videoBudgeView.leftAnchor, constant: 5).isActive = true
        chooseVideoFileButton.centerYAnchor.constraint(equalTo: videoBudgeView.centerYAnchor).isActive = true
        chooseVideoFileButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        chooseVideoFileButton.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        
        videoBudgeLabel.frame = CGRect(x: getSizeScreen().height / 19, y: -5, width: getSizeScreen().width / 18, height: getSizeScreen().width / 18)
        videoBudgeLabel.layer.cornerRadius = getSizeScreen().width / 36
        videoBudgeLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        pinFileView.addSubview(videoFileLine)
        videoFileLine.rightAnchor.constraint(equalTo: pinFileView.rightAnchor, constant: -7).isActive = true
        videoFileLine.leftAnchor.constraint(equalTo: pinFileView.leftAnchor, constant: 7).isActive = true
        videoFileLine.topAnchor.constraint(equalTo: videoBudgeView.bottomAnchor, constant: getSizeScreen().height / 50).isActive = true
        videoFileLine.heightAnchor.constraint(equalToConstant: 1.5).isActive = true
        
        pinFileView.addSubview(voiceBudgeView)
        voiceBudgeView.topAnchor.constraint(equalTo: videoFileLine.bottomAnchor, constant: getSizeScreen().height / 50).isActive = true
        voiceBudgeView.leftAnchor.constraint(equalTo: pinFileView.leftAnchor, constant: getSizeScreen().width / 20).isActive = true
        voiceBudgeView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        voiceBudgeView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        let voiceGesture = UITapGestureRecognizer(target: self, action: #selector(handelVoicePicker))
        voiceBudgeView.addGestureRecognizer(voiceGesture)
        
        pinFileView.addSubview(voiceFileLabel)
        voiceFileLabel.centerYAnchor.constraint(equalTo: voiceBudgeView.centerYAnchor).isActive = true
        voiceFileLabel.rightAnchor.constraint(equalTo: pinFileView.rightAnchor, constant: -getSizeScreen().width / 20).isActive = true
        voiceFileLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4.5).isActive = true
        voiceFileLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 40)
        
        voiceBudgeView.addSubview(voiceBudgeLabel)
        voiceBudgeView.addSubview(chooseVoiceFileButton)
        
        chooseVoiceFileButton.leftAnchor.constraint(equalTo: voiceBudgeView.leftAnchor, constant: 5).isActive = true
        chooseVoiceFileButton.centerYAnchor.constraint(equalTo: voiceBudgeView.centerYAnchor).isActive = true
        chooseVoiceFileButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        chooseVoiceFileButton.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        
        voiceBudgeLabel.frame = CGRect(x: getSizeScreen().height / 22, y: -5, width: getSizeScreen().width / 18, height: getSizeScreen().width / 18)
        voiceBudgeLabel.layer.cornerRadius = getSizeScreen().width / 36
        voiceBudgeLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        addSubview(addressTextField)
        addressTextField.attributedPlaceholder = NSAttributedString(string: "آدرس دقیق", attributes: [
            .foregroundColor: UIColor.gray,
            .font: UIFont.boldSystemFont(ofSize: getSizeScreen().height / 45)
            ])
        addressTextField.topAnchor.constraint(equalTo: pinFileView.bottomAnchor, constant: 40).isActive = true
        addressTextField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        addressTextField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        addressTextField.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 25).isActive = true
        
        addSubview(descriptionTextField)
        descriptionTextField.attributedPlaceholder = NSAttributedString(string: "افزودن توضیحات", attributes: [
            .foregroundColor: UIColor.gray,
            .font: UIFont.boldSystemFont(ofSize: getSizeScreen().height / 45)
            ])
        descriptionTextField.topAnchor.constraint(equalTo: addressTextField.bottomAnchor, constant: 40).isActive = true
        descriptionTextField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        descriptionTextField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        descriptionTextField.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 25).isActive = true
        
        addSubview(addRequestButton)
        
        addSubview(returnButton)
        
        addRequestButton.addTarget(self, action: #selector(handelUpdate), for: .touchUpInside)
        returnButton.addTarget(self, action: #selector(handelReturn), for: .touchUpInside)
        
        addRequestButton.topAnchor.constraint(equalTo: descriptionTextField.bottomAnchor, constant: 20).isActive = true
        addRequestButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        addRequestButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        addRequestButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
        
        
        returnButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: (getSizeScreen().height / 70)).isActive = true
        returnButton.centerYAnchor.constraint(equalTo: addRequestButton.centerYAnchor).isActive = true
        
    }
    
    @objc fileprivate func handelReturn() {
        
        unsendView?.returnSendRequestView()
    }
    
    @objc fileprivate func handelUpdate() {
        
        editRequst()
        
    }
    
    var budgeValue = 0
    
    @objc fileprivate func handelImagePicker() {
        
        if budgeValue < 3 {
            //            CityProblem?.disMissBlackViewForSendRequestView()
            //            CityProblem?.showImagePicker()
            
            unsendView?.unsendView?.FllowUp?.showImagePicker()
            //            print(unsendView?.unsendView?.FllowUp?.sendRequsetImagesArray.count!)
        }
        else {
            let alert = CDAlertView(title: "", message: "شما مجاز به انتخاب ۳ عکس هستید", type: CDAlertViewType.warning)
            let action = CDAlertViewAction(title: "فهیمدم")
            alert.add(action: action)
            alert.show()
        }
        
        
    }
    
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audiPlayer: AVAudioPlayer!
    
    let voiceBlackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.65)
        return view
    }()
    
    let voiceWhiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    let playAndStopAudioButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .lightGray
        btn.tintColor = .red
        btn.layer.masksToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "record")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.imageView?.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        btn.imageEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        return btn
    }()
    
    let pauseAudioButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .lightGray
        btn.tintColor = .red
        btn.layer.masksToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "pause")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.imageView?.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        btn.imageEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        return btn
    }()
    
    @objc fileprivate func handelVoicePicker() {
        
        //        let blackViewGesture = UITapGestureRecognizer(target: self, action: #selector(handelDismissVoiceBlackView))
        //        let whiteViewGesture = UITapGestureRecognizer(target: self, action: #selector(handelUndismissWhiteViewWhenTapOnIt))
        //        voiceBlackView.addGestureRecognizer(blackViewGesture)
        //        voiceWhiteView.addGestureRecognizer(whiteViewGesture)
        
        if let windows = UIApplication.shared.keyWindow {
            windows.addSubview(voiceBlackView)
            voiceBlackView.frame = windows.frame
            voiceBlackView.addSubview(voiceWhiteView)
            voiceWhiteView.frame.size.height = windows.frame.height / 5
            voiceWhiteView.frame.size.width = windows.frame.width / 1.5
            voiceWhiteView.layer.cornerRadius = 10
            voiceWhiteView.layer.masksToBounds = true
            voiceWhiteView.center = voiceBlackView.center
            
            voiceWhiteView.addSubview(playAndStopAudioButton)
            playAndStopAudioButton.centerXAnchor.constraint(equalTo: voiceWhiteView.centerXAnchor).isActive = true
            playAndStopAudioButton.centerYAnchor.constraint(equalTo: voiceWhiteView.centerYAnchor).isActive = true
            playAndStopAudioButton.heightAnchor.constraint(equalToConstant: windows.frame.height / 10).isActive = true
            playAndStopAudioButton.widthAnchor.constraint(equalToConstant: windows.frame.height / 10).isActive = true
            playAndStopAudioButton.layer.cornerRadius = windows.frame.height / 20
            playAndStopAudioButton.addTarget(self, action: #selector(voiceRecordingOperation), for: .touchUpInside)
            
            voiceWhiteView.addSubview(pauseAudioButton)
            pauseAudioButton.leftAnchor.constraint(equalTo: playAndStopAudioButton.rightAnchor, constant: getSizeScreen().width / 20).isActive = true
            pauseAudioButton.centerYAnchor.constraint(equalTo: playAndStopAudioButton.centerYAnchor).isActive = true
            pauseAudioButton.heightAnchor.constraint(equalToConstant: windows.frame.height / 16).isActive = true
            pauseAudioButton.widthAnchor.constraint(equalToConstant: windows.frame.height / 16).isActive = true
            pauseAudioButton.layer.cornerRadius = windows.frame.height / 32
            pauseAudioButton.addTarget(self, action: #selector(handelPauseAudio), for: .touchUpInside)
            
            let cancelButton = UIButton(type: .system)
            cancelButton.backgroundColor = .white
            cancelButton.tintColor = .red
            cancelButton.setTitle("لفو", for: .normal)
            cancelButton.translatesAutoresizingMaskIntoConstraints = false
            cancelButton.addTarget(self, action: #selector(handelDismissVoiceBlackView), for: .touchUpInside)
            voiceWhiteView.addSubview(cancelButton)
            cancelButton.leftAnchor.constraint(equalTo: voiceWhiteView.leftAnchor, constant: 10).isActive = true
            cancelButton.bottomAnchor.constraint(equalTo: voiceWhiteView.bottomAnchor, constant: -10).isActive = true
            cancelButton.heightAnchor.constraint(equalToConstant: windows.frame.width / 10).isActive = true
            cancelButton.widthAnchor.constraint(equalToConstant: windows.frame.width / 5).isActive = true
            
            
            voiceBlackView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.voiceBlackView.alpha = 1
                if self.audioRecorder != nil {
                    self.audioRecorder.stop()
                    self.audioRecorder = nil
                    let image = UIImage(named: "record")?.withRenderingMode(.alwaysTemplate)
                    self.playAndStopAudioButton.setImage(image, for: .normal)
                    let image1 = UIImage(named: "pause")?.withRenderingMode(.alwaysTemplate)
                    self.pauseAudioButton.setImage(image1, for: .normal)
                }
                
            }, completion: nil)
        }
        
    }
    
    var recordMode = true
    var budgeVoiceValue = 0
    
    var voiceData: Data?
    
    @objc fileprivate func handelDismissVoiceBlackView() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.voiceBlackView.alpha = 0
            
            
        }, completion: nil)
    }
    
    @objc fileprivate func voiceRecordingOperation() {
        recordingSession = AVAudioSession.sharedInstance()
        
        AVAudioSession.sharedInstance().requestRecordPermission { (hasPermission) in
            
            if hasPermission {
                print("Accepter")
            }
            
        }
        
        if audioRecorder == nil {
            
            let fileName = getDocumentsDirectory().appendingPathComponent("1.m4a")
            let setting = [AVFormatIDKey: Int(kAudioFormatMPEG4AAC), AVSampleRateKey: 12000, AVNumberOfChannelsKey: 1, AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue]
            do {
                audioRecorder = try AVAudioRecorder(url: fileName, settings: setting)
                audioRecorder.delegate = self
                audioRecorder.record()
                let image = UIImage(named: "stopRecord")?.withRenderingMode(.alwaysTemplate)
                playAndStopAudioButton.setImage(image, for: .normal)
                
            }
            catch {
                
                let alert = CDAlertView(title: "", message: "ضبط صدا بدرستی صورت نکرفت، دوباره امتحان کنید", type: .warning)
                let action = CDAlertViewAction(title: "تأیید")
                alert.add(action: action)
                alert.show()
                audioRecorder = nil
            }
        }
        else {
            audioRecorder.stop()
            audioRecorder = nil
            budgeVoiceValue = 1
            voiceBudgeLabel.text = "۱"
            voiceBudgeLabel.isHidden = false
            let path = getDocumentsDirectory().appendingPathComponent("1.m4a")
            do {
                voiceData = try Data(contentsOf: path)
            }catch {
                
            }
            
            let image = UIImage(named: "record")?.withRenderingMode(.alwaysTemplate)
            playAndStopAudioButton.setImage(image, for: .normal)
        }
    }
    
    func addBudgeToImageBudgeLabel() {
        imageBudgeLabel.isHidden = false
        let budgeString = String(budgeValue)
        imageBudgeLabel.text = convertEngNumToPersianNum(num: budgeString)
    }
    
    func convertEngNumToPersianNum(num: String) -> String {
        //let number = NSNumber(value: Int(num)!)
        
        let format = NumberFormatter()
        format.locale = Locale(identifier: "fa_IR")
        let number =   format.number(from: num)
        let faNumber = format.string(from: number!)
        return faNumber!
        
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    @objc fileprivate func handelPauseAudio() {
        
        if recordMode {
            audioRecorder.pause()
            recordMode = false
            let image = UIImage(named: "play")?.withRenderingMode(.alwaysTemplate)
            pauseAudioButton.setImage(image, for: .normal)
        }
        else {
            audioRecorder.record()
            recordMode = true
            let image = UIImage(named: "pause")?.withRenderingMode(.alwaysTemplate)
            pauseAudioButton.setImage(image, for: .normal)
        }
        
    }
    
}

//
//  tourismLocationsController.swift
//  hamshahriyar
//
//  Created by apple on 11/10/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit
import Mapbox
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import CDAlertView
import MapboxCoreNavigation
import MapboxNavigation
import MapboxDirections

class tourismLocationsController: UIViewController, MGLMapViewDelegate, CLLocationManagerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {

    let loader: NVActivityIndicatorView = {
        let loader = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        let loaderType = NVActivityIndicatorType.ballRotateChase
        loader.type = loaderType
        loader.color = .white
        loader.padding = 5
        loader.translatesAutoresizingMaskIntoConstraints = false
        return loader
    }()
    
    let cellId = "cellId"
    
    let catImages = ["city","bus_stop","car_parking","coffe_shop","prayer_beads","restaurant_signboard","park","petrol","toy","urban_bike","public_wc","fast_food"]
    let catName = ["مکان های شهر شما","ایستگاه اتوبوس","پارکینگ عمومی","عابر بانک","اماکن مذهبی(مسجد و...)","رستوران","پارک","پمپ بنزین","بیمارستان","ایستگاه دوچرخه شهری","سرویس بهداشتی عمومی","فست فود"]
    
    var mapView: MGLMapView!
    
    lazy var CV: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        cv.backgroundColor = .white
        return cv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupMap()
        showDetailAnnotation()
        setupViews()
    }
    
    let detailAnnoteWhiteView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.baseColor.base
        return view
    }()
    let detailAnnotImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
//        imageView.clipsToBounds = true
        imageView.backgroundColor = .white
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    let detailTypeLabel: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.text = "ایستگاه اتوبوس"
        return lbl
    }()
    let detailTitleLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.text = "ایستگاه اتوبوسsadad"
        return lbl
    }()
    
    let routeButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor.baseColor.base
        btn.setImage(UIImage(named: "route")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.tintColor = .white
        btn.layer.shadowColor = UIColor.white.cgColor
        btn.layer.shadowOpacity = 1
        btn.imageView?.clipsToBounds = true
        btn.imageView?.contentMode = .scaleAspectFit
        btn.layer.shadowOffset = CGSize.zero
        btn.layer.shadowRadius = 5
        return btn
    }()
    
    fileprivate func showDetailAnnotation() {
        view.addSubview(detailAnnoteWhiteView)
        detailAnnoteWhiteView.addSubview(detailAnnotImageView)
        detailAnnoteWhiteView.addSubview(detailTypeLabel)
        detailAnnoteWhiteView.addSubview(detailTitleLabel)
        detailAnnoteWhiteView.addSubview(routeButton)
        detailAnnoteWhiteView.frame = CGRect(x: 0, y: getSizeScreen().height, width: getSizeScreen().width, height: getSizeScreen().height / 6)
        
        detailAnnotImageView.centerXAnchor.constraint(equalTo: detailAnnoteWhiteView.centerXAnchor).isActive = true
        detailAnnotImageView.centerYAnchor.constraint(equalTo: detailAnnoteWhiteView.centerYAnchor, constant: -getSizeScreen().height / 18 - getSizeScreen().height / 30).isActive = true
        detailAnnotImageView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 10).isActive = true
        detailAnnotImageView.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 10).isActive = true
        detailAnnotImageView.layer.cornerRadius = getSizeScreen().height / 20
        
        detailAnnoteWhiteView.addConstraintWithFormat(format: "H:[v0(\(getSizeScreen().height / 16))]-\(getSizeScreen().width / 20)-|", views: routeButton)
        detailAnnoteWhiteView.addConstraintWithFormat(format: "V:|-\(getSizeScreen().width / 20)-[v0(\(getSizeScreen().height / 16))]", views: routeButton)
        routeButton.layer.cornerRadius = getSizeScreen().height / 32
        routeButton.addTarget(self, action: #selector(handelRouting), for: .touchUpInside)
        
        detailTypeLabel.topAnchor.constraint(equalTo: detailAnnotImageView.bottomAnchor, constant: 5).isActive = true
        detailTypeLabel.centerXAnchor.constraint(equalTo: detailAnnoteWhiteView.centerXAnchor).isActive = true
        detailTypeLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 30).isActive = true
        detailTypeLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width).isActive = true
        detailTypeLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 60)
        
        detailTitleLabel.topAnchor.constraint(equalTo: detailTypeLabel.bottomAnchor, constant: 10).isActive = true
        detailTitleLabel.centerXAnchor.constraint(equalTo: detailAnnoteWhiteView.centerXAnchor).isActive = true
        detailTitleLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 25).isActive = true
        detailTitleLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width).isActive = true
        detailTitleLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 70)
    }
    
    @objc fileprivate func handelRouting() {
        
        calculateRoute(from: (mapView.userLocation!.coordinate), to: selectedAnnotation.coordinate) { (route, error) in
            if error != nil {
                print("Error calculating route")
            }
        }
        
        let alert = CDAlertView(title: "", message: "مسیریابی", type: CDAlertViewType.custom(image: UIImage(named: "route")!))
        alert.messageFont = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 60)
        alert.alertBackgroundColor = UIColor.baseColor.base
        alert.messageTextColor = .white
        let routeAction = CDAlertViewAction(title: "شروع", font: UIFont.systemFont(ofSize: getSizeScreen().height / 75), textColor: UIColor.baseColor.base, backgroundColor: .white) { (action) -> Bool in
            if let source = self.mapView.style?.source(withIdentifier: "route-source") as? MGLShapeSource {
                source.shape = nil
            }
            let navigationViewController = NavigationViewController(for: self.directionsRoute!)
            self.present(navigationViewController, animated: true, completion: nil)
            return true
        }
        let cancelAction = CDAlertViewAction(title: "لغو", font: UIFont.systemFont(ofSize: getSizeScreen().height / 75), textColor: UIColor.red, backgroundColor: .white) { (action) -> Bool in
            return true
        }
        alert.add(action: routeAction)
        alert.add(action: cancelAction)
        alert.show()
        
    }
    
    fileprivate func setupViews() {
        let blackVIewGesture = UITapGestureRecognizer(target: self, action: #selector(handelDismissBlackView))
        blackVIewGesture.delegate = self
        blackView.addGestureRecognizer(blackVIewGesture)
//        let menuViewGesture = UITapGestureRecognizer(target: self, action: #selector(handelMenuViewGesture))
//        menuView.addGestureRecognizer(menuViewGesture)
        view.addSubview(blackView)
        blackView.frame = mapView.frame
        blackView.alpha = 0
        blackView.addSubview(menuView)
        menuView.frame = CGRect(x: getSizeScreen().width, y: 0, width: getSizeScreen().width / 1.3, height: mapView.frame.size.height)
        setupCollectionView()
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blackView.alpha = 1
            self.menuView.frame = CGRect(x: self.getSizeScreen().width - self.getSizeScreen().width / 1.6, y: 0, width: self.getSizeScreen().width / 1.6, height: self.mapView.frame.size.height)
            
        }, completion: nil)
    }
    
    
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "دسته بندی مکان ها"
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    @objc func handelMenuViewGesture() {
        
    }
    
    fileprivate func setupCollectionView() {
        menuView.addSubview(CV)
        menuView.addConstraintWithFormat(format: "H:|[v0]|", views: CV)
        menuView.addConstraintWithFormat(format: "V:|-\(getSizeScreen().height / 10)-[v0]|", views: CV)
        CV.register(locationClassificationCell.self, forCellWithReuseIdentifier: cellId)
        CV.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: getSizeScreen().height / 15, right: 0)
        
        menuView.addSubview(titleLabel)
        menuView.addConstraintWithFormat(format: "H:|[v0]|", views: titleLabel)
        menuView.addConstraintWithFormat(format: "V:|[v0(\(getSizeScreen().height / 10))]", views: titleLabel)
        titleLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
    }
    
    fileprivate func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.baseColor.base
        navigationController?.navigationBar.tintColor = .white
        
        let image = UIImage(named: "menu")?.withRenderingMode(.alwaysTemplate)
        let menu = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handelMenu))
        
        let imageLoc = UIImage(named: "locationFill")?.withRenderingMode(.alwaysTemplate)
        let location = UIBarButtonItem(image: imageLoc, style: .plain, target: self, action: #selector(showLocationUser))
        
        self.navigationItem.setRightBarButtonItems([menu, location], animated: true)
        
        let backImage = UIImage(named: "back")?.withRenderingMode(.alwaysTemplate)
        let back = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(handelDismiss))
        self.navigationItem.setLeftBarButton(back, animated: true)
        
        self.navigationItem.titleView = loader
        
    }
    
    @objc fileprivate func showLocationUser() {
        
        isStartLoading = true
        setupMap()
//        getAllLocations()
    }
    
    @objc fileprivate func handelDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
    
    fileprivate func setupMap() {
        
        let url = URL(string: "mapbox://styles/mapbox/streets-v11")
        mapView = MGLMapView(frame: view.bounds, styleURL: url)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //        mapView.showsUserLocation = true
        mapView.userTrackingMode = .follow
        mapView.showsUserLocation = true
        mapView.tintColor = .darkGray
        mapView.delegate = self
        
        // Set the map’s center coordinate and zoom level.
        mapView.setCenter(CLLocationCoordinate2D(latitude: 35.712130, longitude: 51.310988), zoomLevel: 12, animated: false)
        view.addSubview(mapView)
    }
    
    var isStartLoading = true
    
    func mapView(_ mapView: MGLMapView, didUpdate userLocation: MGLUserLocation?) {
        
        if isStartLoading {
            isStartLoading = false
            mapView.setCenter((userLocation?.coordinate)!, animated: true)
        }
        
    }
    
    var isOperation = false
    
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        isOperation = true
        getAllLocations()
    }
    
    var titleList: [String] = []
    var typeList: [String] = []
    var AnnorationList: [MGLPointAnnotation] = []
    var imageList: [String] = []
    var latList: [Double] = []
    var longList: [Double] = []
    
    fileprivate func getAllLocations() {
        
        numItem = 0

        titleList.removeAll()
        AnnorationList.removeAll()
        typeList.removeAll()
        imageList.removeAll()
        latList.removeAll()
        longList.removeAll()
        loader.startAnimating()
        let centerPointX = view.center.x
        let centerPointY = view.center.y
        let centerPoint = CGPoint(x: centerPointX, y: centerPointY)
        let centerOfMapPoint = mapView.convert(centerPoint, toCoordinateFrom: mapView)
        let l_lat = Double(centerOfMapPoint.latitude)
        let l_long = Double(centerOfMapPoint.longitude)
        let redius = getRedius()
        let distance = 111.045
        
        let param: Parameters = [
            "l_lat" : l_lat,
            "l_long" : l_long,
            "redius" : redius,
            "distance" : distance,
        ]

        
        let sendSmsUrlString = baseURLString + "explore.php"
        Alamofire.request(sendSmsUrlString, method: .post, parameters: param).responseJSON { (response) in
            if let data = response.result.value {
                let json = JSON(data).arrayValue
                for items in json {
                    if let titles = items["title"].string {
                        self.titleList.append(titles)
                        if let lats = items["lat"].string {
                            if let longs = items["lnt"].string {
                                let annot = MGLPointAnnotation()
                                self.latList.append(Double(lats)!)
                                self.longList.append(Double(longs)!)
                                annot.coordinate = CLLocationCoordinate2D(latitude: Double(lats)!, longitude: Double(longs)!)
                                annot.title = "\(self.numItem)"
                                self.AnnorationList.append(annot)
                                if let type = items["type"].string {
                                    self.typeList.append(type)
                                }
                                if let image = items["image"].string {
                                    self.imageList.append(image)
                                }
                            }
                        }
                    }
                    self.numItem += 1
                }
                
                self.loader.stopAnimating()
            }
            if let err = response.result.error {
                let error = "\(err.localizedDescription)"
                self.loader.stopAnimating()
                if error == "An SSL error has occurred and a secure connection to the server cannot be made." {
                    self.showAlert(title: "لطفا فیلتر شکن خود را خاموش کنید", message: "چنانچه در ایران هستید از قطع بودن فیلتر شکن خود مطمئن شوید")
                }
                else if error == "The network connerction was lost." {
                    self.showAlert(title: "اخطار", message: "اینترنت موبایل خود را چک کنید")
                }
                else if error == "The Internet connection appears to be offline." {
                    self.showAlert(title: "اخطار", message: "اینترنت موبایل خود را چک کنید")
                }
                else if error == "The operation couldn't be completed." {
                    self.showAlert(title: "اخطار", message: "ارتباط برقرار نشد، دوباره امتحان کنید")
                }
                else if error == "The request timed out." {
                    self.showAlert(title: "اخطار", message: "ارتباط برقرار نشد، دوباره امتحان کنید")
                }
            }

            
            DispatchQueue.main.async {
                self.mapView.addAnnotations(self.AnnorationList)
            }
        }
    }
    
    func showAlert(title: String, message: String) {
        
        let alert = CDAlertView(title: title, message: message, type: .warning)
        let action = CDAlertViewAction(title: "تلاش مجدد", font: UIFont.systemFont(ofSize: getSizeScreen().height / 57), textColor: UIColor.purpuleColor.purple, backgroundColor: .white) { (action) -> Bool in
            if self.isSelectType {
                self.getAllLocationsWithType(type: self.typeSelect)
            }
            else {
                self.getAllLocations()
            }
            return true
        }
        alert.add(action: action)
        alert.show()
        
    }

    var selectedAnnotation = MGLPointAnnotation()
    
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        
        
        
        loader.startAnimating()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.detailAnnoteWhiteView.frame = CGRect(x: 0, y: self.getSizeScreen().height - self.getSizeScreen().height / 4, width: self.getSizeScreen().width, height: self.getSizeScreen().height / 6)
        }, completion: nil)
        if let numStr = annotation.title {
            let num = Int(numStr!)
            if let nums = num {
                selectedAnnotation.coordinate = CLLocationCoordinate2D(latitude: latList[nums], longitude: longList[nums])
                detailAnnotImageView.loadImageUsingURLString(urlString: imageList[nums])
                detailTypeLabel.text = typeList[nums]
                detailTitleLabel.text = titleList[nums]
                loader.stopAnimating()
            }
        }
    }
    
    func topCenterCoordinate() -> CLLocationCoordinate2D {
        return mapView.convert(CGPoint(x: mapView.frame.size.width / 2.0, y: 0), toCoordinateFrom: mapView)
    }
    
    fileprivate func getRedius() -> Double {
        let centerLocation = CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: self.topCenterCoordinate().longitude)
        let topCenterCoordinate = self.topCenterCoordinate()
        let topCenterLocation = CLLocation(latitude: topCenterCoordinate.latitude, longitude: topCenterCoordinate.longitude)
        return centerLocation.distance(from: topCenterLocation) / 1000
    }
    
    var isSelectType = false
    var typeSelect = ""
    var numItem = 0
    func mapView(_ mapView: MGLMapView, regionDidChangeAnimated animated: Bool) {
        if let annotations = mapView.annotations {
            mapView.removeAnnotations(annotations)
        }
        
        numItem = 0
        titleList.removeAll()
        AnnorationList.removeAll()
        typeList.removeAll()
        imageList.removeAll()
        latList.removeAll()
        longList.removeAll()
        if isSelectType {
            getAllLocationsWithType(type: typeSelect)
        }
        else {
            getAllLocations()
        }
    }
    
    let blackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.75)
        return view
    }()
    
    let menuView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    var isSelect = true
    @objc fileprivate func handelMenu() {
        
        if !isSelect {
            isSelect = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 1
                self.menuView.frame = CGRect(x: self.getSizeScreen().width - self.getSizeScreen().width / 1.6, y: 0, width: self.getSizeScreen().width / 1.6, height: self.mapView.frame.size.height)
                
            }, completion: nil)
        }
        else {
            handelDismissBlackView()
            isSelect = false
        }
        
    }
    

    
    @objc fileprivate func handelDismissBlackView() {
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.menuView.frame = CGRect(x: self.getSizeScreen().width, y: 0, width: self.getSizeScreen().width / 1.6, height: self.mapView.frame.size.height)
            self.blackView.alpha = 0
            self.isSelect = false
        }, completion: nil)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! locationClassificationCell
        let imageString = catImages[indexPath.item]
        cell.imageView.image = UIImage(named: imageString)?.withRenderingMode(.alwaysOriginal)
        cell.CatLabel.text = catName[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: getSizeScreen().width / 1.63, height: getSizeScreen().height / 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        handelDismissBlackView()
        if indexPath.item == 0 {
            mapView.removeAnnotations(AnnorationList)
            isSelectType = false
            getAllLocations()
        }
        else {
            typeSelect = catName[indexPath.item]
            isSelectType = true
            getAllLocationsWithType(type: catName[indexPath.item])
        }
        
        
    }
    
    var directionsRoute: Route?
    
    func calculateRoute(from origin: CLLocationCoordinate2D,
                        to destination: CLLocationCoordinate2D,
                        completion: @escaping (Route?, Error?) -> ()) {
        
        // Coordinate accuracy is the maximum distance away from the waypoint that the route may still be considered viable, measured in meters. Negative values indicate that a indefinite number of meters away from the route and still be considered viable.
        let origin = Waypoint(coordinate: origin, coordinateAccuracy: -1, name: "Start")
        let destination = Waypoint(coordinate: destination, coordinateAccuracy: -1, name: "Finish")
        
        // Specify that the route is intended for automobiles avoiding traffic
        let options = NavigationRouteOptions(waypoints: [origin, destination], profileIdentifier: .automobileAvoidingTraffic)
        
        // Generate the route object and draw it on the map
        _ = Directions.shared.calculate(options) { [unowned self] (waypoints, routes, error) in
            self.directionsRoute = routes?.first
            // Draw the route on the map after creating it
            self.drawRoute(route: self.directionsRoute!)
        }
    }
    
    func drawRoute(route: Route) {
        guard route.coordinateCount > 0 else { return }
        // Convert the route’s coordinates into a polyline
        var routeCoordinates = route.coordinates!
        let polyline = MGLPolylineFeature(coordinates: &routeCoordinates, count: route.coordinateCount)
        
        // If there's already a route line on the map, reset its shape to the new route
        if let source = mapView.style?.source(withIdentifier: "route-source") as? MGLShapeSource {
            source.shape = polyline
        } else {
            let source = MGLShapeSource(identifier: "route-source", features: [polyline], options: nil)
            
            // Customize the route line color and width
            let lineStyle = MGLLineStyleLayer(identifier: "route-style", source: source)
            lineStyle.lineColor = NSExpression(forConstantValue: #colorLiteral(red: 0.1897518039, green: 0.3010634184, blue: 0.7994888425, alpha: 1))
            lineStyle.lineWidth = NSExpression(forConstantValue: 3)
            
            // Add the source and style layer of the route line to the map
            mapView.style?.addSource(source)
            mapView.style?.addLayer(lineStyle)
        }
    }
    
//    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
//        return true
//    }
    
    // Present the navigation view controller when the callout is selected
    func mapView(_ mapView: MGLMapView, tapOnCalloutFor annotation: MGLAnnotation) {
        let navigationViewController = NavigationViewController(for: directionsRoute!)
        self.present(navigationViewController, animated: true, completion: nil)
    }
    
    fileprivate func getAllLocationsWithType(type: String) {
        
        if let annotations = mapView.annotations {
            mapView.removeAnnotations(annotations)
        }
        
        numItem = 0
        titleList.removeAll()
        AnnorationList.removeAll()
        typeList.removeAll()
        imageList.removeAll()
        latList.removeAll()
        longList.removeAll()
        loader.startAnimating()
        
        let centerPointX = view.center.x
        let centerPointY = view.center.y
        let centerPoint = CGPoint(x: centerPointX, y: centerPointY)
        let centerOfMapPoint = mapView.convert(centerPoint, toCoordinateFrom: mapView)
        let l_lat = Double(centerOfMapPoint.latitude)
        let l_long = Double(centerOfMapPoint.longitude)
        let redius = getRedius()
        let distance = 111.045
        
        let param: Parameters = [
            "l_lat" : l_lat,
            "l_long" : l_long,
            "redius" : redius,
            "distance" : distance,
            "type" : type
            ]
        
        
        let sendSmsUrlString = baseURLString + "explore.php"
        Alamofire.request(sendSmsUrlString, method: .post, parameters: param).responseJSON { (response) in
            if let data = response.result.value {
                let json = JSON(data).arrayValue
                for items in json {
                    if let titles = items["title"].string {
                        self.titleList.append(titles)
                        if let lats = items["lat"].string {
                            if let longs = items["lnt"].string {
                                let annot = MGLPointAnnotation()
                                self.latList.append(Double(lats)!)
                                self.longList.append(Double(longs)!)
                                annot.coordinate = CLLocationCoordinate2D(latitude: Double(lats)!, longitude: Double(longs)!)
                                annot.title = "\(self.numItem)"
                                self.AnnorationList.append(annot)
                                if let type = items["type"].string {
                                    self.typeList.append(type)
                                }
                                if let image = items["image"].string {
                                    self.imageList.append(image)
                                }
                            }
                        }
                    }
                    self.numItem += 1
                }
                
                self.loader.stopAnimating()
            }
            if let err = response.result.error {
                self.loader.stopAnimating()
                let alert = CDAlertView(title: "اخطار", message: "\(err.localizedDescription)", type: .warning)
                let action = CDAlertViewAction(title: "تأیید")
                alert.add(action: action)
                alert.show()
                
            }
            
            DispatchQueue.main.async {
                self.mapView.addAnnotations(self.AnnorationList)
            }
        }
    }

 

}

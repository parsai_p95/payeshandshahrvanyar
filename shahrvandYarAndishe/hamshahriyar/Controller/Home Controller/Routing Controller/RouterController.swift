//
//  RouterController.swift
//  hamshahriyar
//
//  Created by apple on 10/23/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit
import Mapbox
import MapboxCoreNavigation
import MapboxNavigation
import MapboxDirections
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class RouterController: UIViewController, MGLMapViewDelegate, CLLocationManagerDelegate, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate {

    let cellId = "cellId"
    
    var isSource = true
    var isDestination = false
    
    lazy var tblView: UITableView = {
        let tbv = UITableView(frame: .zero, style: .grouped)
        tbv.dataSource = self
        tbv.delegate = self
        tbv.backgroundColor = .white
        return tbv
    }()
    
    let loader: NVActivityIndicatorView = {
        let loader = NVActivityIndicatorView(frame: .zero)
        let loaderType = NVActivityIndicatorType.ballRotateChase
        loader.type = loaderType
        loader.color = .black
        loader.padding = 5
        loader.translatesAutoresizingMaskIntoConstraints = false
        return loader
    }()
    
    let pinImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "pinSou")?.withRenderingMode(.alwaysOriginal)
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let choosePlaceButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.layer.masksToBounds = false
        btn.tintColor = .black
        btn.setTitle("تعیین مبدا", for: .normal)
        btn.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        btn.layer.shadowOpacity = 1
        btn.layer.shadowOffset = CGSize.zero
        btn.layer.shadowRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    //"تعیین مقصد"
    let locButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.layer.masksToBounds = false
        btn.tintColor = .black
        btn.setImage(UIImage(named: "locationFill")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        btn.layer.shadowOpacity = 1
        btn.layer.shadowOffset = CGSize.zero
        btn.layer.shadowRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    var mapView: NavigationMapView!
    var coordinate: CLLocationCoordinate2D?
    var sourceCoordinate: CLLocationCoordinate2D?
    let sourceAnnotation =  MGLPointAnnotation()
    var destinationCoordinate: CLLocationCoordinate2D?
    let destinationAnnotation =  MGLPointAnnotation()
    let annotation =  MGLPointAnnotation()
    var directionsRoute: Route?

    override func viewDidLoad() {
        super.viewDidLoad()

//        if CLLocationManager.locationServicesEnabled() {
//            switch CLLocationManager.authorizationStatus() {
//            case .notDetermined, .restricted, .denied:
//                print("No access")
//            case .authorizedAlways, .authorizedWhenInUse:
//                print("Access")
//            }
//        } else {
//            print("Location services are not enabled")
//        }
        
//        if !CLLocationManager.locationServicesEnabled() {
//            if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION") {
//                // If general location settings are disabled then open general location settings
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            }
//        }
        
        setupNavigationBar()
        setupMap()
        setupViews()
        setupResultSearchView()
    }

     func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        
    }

    fileprivate func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.baseColor.base
        navigationController?.navigationBar.tintColor = .white
        
        let image = UIImage(named: "locationFill")?.withRenderingMode(.alwaysTemplate)
        let location = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(showLocationUser))
        
        let searchImage = UIImage(named: "searchFill")?.withRenderingMode(.alwaysTemplate)
        let search = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(handelSearch))
        
        navigationItem.setRightBarButtonItems([location, search], animated: true)
        
        let backImage = UIImage(named: "back")?.withRenderingMode(.alwaysTemplate)
        let back = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(handelDismiss))
        navigationItem.setLeftBarButton(back, animated: true)
    }
    
    let searchResultView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return namePlace.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.textLabel?.text = namePlace[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationItem.rightBarButtonItem = nil
        let image = UIImage(named: "locationFill")?.withRenderingMode(.alwaysTemplate)
        let location = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(showLocationUser))
        let searchImage = UIImage(named: "searchFill")?.withRenderingMode(.alwaysTemplate)
        let search = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(handelSearch))
        navigationItem.setRightBarButtonItems([location, search], animated: true)
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.searchResultView.alpha = 0
        }, completion: nil)
        
        let selectCoordinatePlaceString = coordinatePlace[indexPath.row]
        let coordinatePlaceString = selectCoordinatePlaceString.components(separatedBy: " ")
        let long = coordinatePlaceString[0]
        let lat = coordinatePlaceString[1]
        mapView.setCenter(CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!), zoomLevel: 15, animated: false)
        coordinatePlace.removeAll()
        namePlace.removeAll()
        tblView.reloadData()
    }
    
    @objc fileprivate func handelSearch() {
        self.navigationItem.rightBarButtonItem = nil
        let searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: getSizeScreen().width / 1.3, height: 20))
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        searchBar.placeholder = "جستجو مکان..."
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.rightBarButtonItem = leftNavBarButton
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handelDismissKeyBoardAndSearchBar))
        mapView.addGestureRecognizer(gesture)
        
        
    }
    
    fileprivate func setupResultSearchView() {
        view.addSubview(searchResultView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: searchResultView)
        view.addConstraintWithFormat(format: "V:|[v0(\(getSizeScreen().height / 8))]", views: searchResultView)
        searchResultView.alpha = 0
        
        tblView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        searchResultView.addSubview(tblView)
        searchResultView.addConstraintWithFormat(format: "H:|[v0]|", views: tblView)
        searchResultView.addConstraintWithFormat(format: "V:|[v0]|", views: tblView)
        
        searchResultView.addSubview(loader)
        loader.centerXAnchor.constraint(equalTo: searchResultView.centerXAnchor).isActive = true
        loader.centerYAnchor.constraint(equalTo: searchResultView.centerYAnchor).isActive = true
        loader.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        loader.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
    }
    
    @objc fileprivate func handelDismissKeyBoardAndSearchBar() {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationItem.rightBarButtonItem = nil
        let image = UIImage(named: "locationFill")?.withRenderingMode(.alwaysTemplate)
        let location = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(showLocationUser))
        let searchImage = UIImage(named: "searchFill")?.withRenderingMode(.alwaysTemplate)
        let search = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(handelSearch))
        navigationItem.setRightBarButtonItems([location, search], animated: true)
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.searchResultView.alpha = 0
        }, completion: nil)
        coordinatePlace.removeAll()
        namePlace.removeAll()
    }
    
    @objc fileprivate func handelDismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if let place = searchBar.text {
            UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.searchResultView.alpha = 1
            }, completion: nil)
            getPlace(place: place)
        }
    }
    
    var namePlace: [String] = []
    var coordinatePlace: [String] = []
    
    fileprivate func getPlace(place: String)  {
        
        loader.startAnimating()
        
        let param: Parameters = [
            "format" : "json",
            "geocode" : place + "تهران",
            "results" : 1,
            "lang" : "en-US"
        ]
        
        let userInfoURL = "https://geocode-maps.yandex.ru/1.x/"
        let getUrl = userInfoURL.data(using: String.Encoding.ascii, allowLossyConversion: true)
        let urlString = String(data: getUrl!, encoding: String.Encoding.ascii)
        guard let url = URL(string: urlString!) else{return}
        Alamofire.request(url, method: .get, parameters: param).responseJSON { (response) in
            
            if let data = response.result.value {
                let json = JSON(data)
                let featureMember = json["response"]["GeoObjectCollection"]["featureMember"].arrayValue
                for items in featureMember {
                    if let CoordinatePlace = items["GeoObject"]["Point"]["pos"].string {
                        self.coordinatePlace.append(CoordinatePlace)
                    }
                    if let NamePlace = items["GeoObject"]["name"].string {
                        self.namePlace.append(NamePlace)
                    }
                }
                DispatchQueue.main.async {
                    self.loader.stopAnimating()
                    self.tblView.reloadData()
                }
            }
            
        }
    }
    
    @objc fileprivate func showLocationUser() {
        
//        mapView.showsUserLocation = true
//        mapView.setUserTrackingMode(.follow, animated: true)
//        mapView.setCenter((mapView.userLocation?.coordinate)!, animated: false)
        isStartLoading = true
        handelCancelNavigating()
        setupMap()
        setupViews()
    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    fileprivate func setupViews() {
        
        view.addSubview(pinImageView)
        pinImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -getSizeScreen().height / 18).isActive = true
        pinImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pinImageView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 18).isActive = true
        pinImageView.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 18).isActive = true
        //, constant: -getSizeScreen().height / 30
        
        view.addSubview(choosePlaceButton)
        choosePlaceButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        choosePlaceButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -getSizeScreen().height / 30).isActive = true
        choosePlaceButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 25).isActive = true
        choosePlaceButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
        choosePlaceButton.addTarget(self, action: #selector(handleLongPress), for: .touchUpInside)
    }
    
    
    
    
    fileprivate func setupMap() {

        let url = URL(string: "mapbox://styles/mapbox/streets-v11")
        mapView = NavigationMapView(frame: view.bounds, styleURL: url)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .follow
//        mapView.showsUserHeadingIndicator = true
        mapView.tintColor = .darkGray
        mapView.delegate = self
        
        // Set the map’s center coordinate and zoom level.
        mapView.setCenter(CLLocationCoordinate2D(latitude: 35.712130, longitude: 51.310988), zoomLevel: 14, animated: false)
        view.addSubview(mapView)

        
    }

    var isStartLoading = true
    
    func mapView(_ mapView: MGLMapView, didUpdate userLocation: MGLUserLocation?) {

        if isStartLoading {
            isStartLoading = false
            mapView.setCenter((userLocation?.coordinate)!, animated: true)
        }
        
    }
    var start = true
    
    @objc func handleLongPress() {
        
        if isSource {
            
            let centerPointX = view.center.x
            let centerPointY = view.center.y - getSizeScreen().height / 10
            let centerPoint = CGPoint(x: centerPointX, y: centerPointY)
            sourceCoordinate =  mapView.convert(centerPoint, toCoordinateFrom: mapView)
            //            coordinate = mapView.convert(gestureRecognizer?.location(in: mapView) ?? CGPoint.zero, toCoordinateFrom: mapView)
            sourceAnnotation.coordinate = CLLocationCoordinate2D(latitude: (sourceCoordinate?.latitude)!, longitude: (sourceCoordinate?.longitude)!)
            //        annotation.title = "شروع مسیریابی"
            choosePlaceButton.setTitle("تعیین مقصد", for: .normal)
            mapView.addAnnotation(sourceAnnotation)
//            mapView.setCenter(coordinate!, animated: true)
            pinImageView.image = UIImage(named: "pinDes")?.withRenderingMode(.alwaysOriginal)
            let imageCancel = UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate)
            let cancelBarbuttonItem = UIBarButtonItem(image: imageCancel, style: .plain, target: self, action: #selector(handelCancelNavigating))
            
            let image = UIImage(named: "locationFill")?.withRenderingMode(.alwaysTemplate)
            let location = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(showLocationUser))
            
            navigationItem.setRightBarButtonItems([cancelBarbuttonItem, location], animated: true)

        }
        
        else if isDestination {
            if start {
                start = false
                let centerPointX = view.center.x
                let centerPointY = view.center.y - getSizeScreen().height / 10
                let centerPoint = CGPoint(x: centerPointX, y: centerPointY)
                coordinate =  mapView.convert(centerPoint, toCoordinateFrom: mapView)
                //            coordinate = mapView.convert(gestureRecognizer?.location(in: mapView) ?? CGPoint.zero, toCoordinateFrom: mapView)
                annotation.coordinate = CLLocationCoordinate2D(latitude: (coordinate?.latitude)!, longitude: (coordinate?.longitude)!)
                //        annotation.title = "شروع مسیریابی"
                choosePlaceButton.setTitle("شروع مسیریابی", for: .normal)
                mapView.addAnnotation(annotation)
//                mapView.setCenter(coordinate!, animated: true)
                pinImageView.isHidden = true
                let imageCancel = UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate)
                let cancelBarbuttonItem = UIBarButtonItem(image: imageCancel, style: .plain, target: self, action: #selector(handelCancelNavigating))
                
                let image = UIImage(named: "locationFill")?.withRenderingMode(.alwaysTemplate)
                let location = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(showLocationUser))
                
                navigationItem.setRightBarButtonItems([cancelBarbuttonItem, location], animated: true)
                
                calculateRoute(from: (sourceAnnotation.coordinate), to: annotation.coordinate) { (route, error) in
                    if error != nil {
                        print("Error calculating route")
                    }
                }
            }
            else {
                
                handelCancelNavigating()
                let navigationViewController = NavigationViewController(for: directionsRoute!)
                self.present(navigationViewController, animated: true, completion: nil)
            }
        }
        
    }
    
    @objc fileprivate func handelCancelNavigating() {
        
        mapView.removeAnnotation(sourceAnnotation)
        mapView.removeAnnotation(annotation)
        if let source = mapView.style?.source(withIdentifier: "route-source") as? MGLShapeSource {
            source.shape = nil
        }
        choosePlaceButton.setTitle("تعیین مبدا", for: .normal)
        start = true
        isSource = true
        isDestination = false
        pinImageView.image = UIImage(named: "pinSou")?.withRenderingMode(.alwaysOriginal)
        pinImageView.isHidden = false
        navigationItem.rightBarButtonItem = nil
        let image = UIImage(named: "locationFill")?.withRenderingMode(.alwaysTemplate)
        let location = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(showLocationUser))
        
        let searchImage = UIImage(named: "searchFill")?.withRenderingMode(.alwaysTemplate)
        let search = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(handelSearch))
        
        navigationItem.setRightBarButtonItems([location, search], animated: true)
    }
    
    func calculateRoute(from origin: CLLocationCoordinate2D,
                        to destination: CLLocationCoordinate2D,
                        completion: @escaping (Route?, Error?) -> ()) {
        
        // Coordinate accuracy is the maximum distance away from the waypoint that the route may still be considered viable, measured in meters. Negative values indicate that a indefinite number of meters away from the route and still be considered viable.
        let origin = Waypoint(coordinate: origin, coordinateAccuracy: -1, name: "Start")
        let destination = Waypoint(coordinate: destination, coordinateAccuracy: -1, name: "Finish")
        
        // Specify that the route is intended for automobiles avoiding traffic
        let options = NavigationRouteOptions(waypoints: [origin, destination], profileIdentifier: .automobileAvoidingTraffic)
        
        // Generate the route object and draw it on the map
        _ = Directions.shared.calculate(options) { [unowned self] (waypoints, routes, error) in
            self.directionsRoute = routes?.first
            // Draw the route on the map after creating it
            self.drawRoute(route: self.directionsRoute!)
        }
    }
    
    func drawRoute(route: Route) {
        guard route.coordinateCount > 0 else { return }
        // Convert the route’s coordinates into a polyline
        var routeCoordinates = route.coordinates!
        let polyline = MGLPolylineFeature(coordinates: &routeCoordinates, count: route.coordinateCount)
        
        // If there's already a route line on the map, reset its shape to the new route
        if let source = mapView.style?.source(withIdentifier: "route-source") as? MGLShapeSource {
            source.shape = polyline
        } else {
            let source = MGLShapeSource(identifier: "route-source", features: [polyline], options: nil)
            
            // Customize the route line color and width
            let lineStyle = MGLLineStyleLayer(identifier: "route-style", source: source)
            lineStyle.lineColor = NSExpression(forConstantValue: #colorLiteral(red: 0.1897518039, green: 0.3010634184, blue: 0.7994888425, alpha: 1))
            lineStyle.lineWidth = NSExpression(forConstantValue: 3)
            
            // Add the source and style layer of the route line to the map
            mapView.style?.addSource(source)
            mapView.style?.addLayer(lineStyle)
        }
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
    // Present the navigation view controller when the callout is selected
    func mapView(_ mapView: MGLMapView, tapOnCalloutFor annotation: MGLAnnotation) {
        let navigationViewController = NavigationViewController(for: directionsRoute!)
        self.present(navigationViewController, animated: true, completion: nil)
    }
    
    func mapView(_ mapView: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage? {
        // Try to reuse the existing ‘pisa’ annotation image, if it exists.
        

        
        if isDestination {
            var annotationImage = mapView.dequeueReusableAnnotationImage(withIdentifier: "pisaa")
            annotationImage = nil
            var image = UIImage(named: "pinDes")!
            image = image.withAlignmentRectInsets(UIEdgeInsets(top: 0, left: 0, bottom: image.size.height/2, right: 0))
            annotationImage = MGLAnnotationImage(image: image, reuseIdentifier: "pisaa")
            return annotationImage
        }
        else if isSource {
            var annotationImage = mapView.dequeueReusableAnnotationImage(withIdentifier: "pisa")
            annotationImage = nil
            isSource = false
            isDestination = true
            var image = UIImage(named: "pinSou")!
            image = image.withAlignmentRectInsets(UIEdgeInsets(top: 0, left: 0, bottom: image.size.height/2, right: 0))
            
            annotationImage = MGLAnnotationImage(image: image, reuseIdentifier: "pisa")
            return annotationImage
        }
        else {
            let annotationImage = MGLAnnotationImage()
            return annotationImage
        }

    }
    
    

}

//
//  aboutUsCell.swift
//  hamshahriyar
//
//  Created by apple on 11/3/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit

class aboutUsCell: baseCell {
    
    let baseView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(baseView)
        addConstraintWithFormat(format: "H:|[v0]|", views: baseView)
        addConstraintWithFormat(format: "V:|[v0]|", views: baseView)
        
    }
    
}

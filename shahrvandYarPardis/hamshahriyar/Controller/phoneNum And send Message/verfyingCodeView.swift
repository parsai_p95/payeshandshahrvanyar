//
//  verfyingCodeView.swift
//  hamshahriyar
//
//  Created by apple on 10/22/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit
import CDAlertView
import Alamofire
import SwiftyJSON

class verfyingCodeView: UIView, UITextFieldDelegate{
    
    var SendMessageController: sendMessageController?
    var bottomConstraint: NSLayoutConstraint?
    var bottomPadding: CGFloat?
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "کد دریافت شده را وارد کنید"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }()
    
    let textFieldsView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var firstCodeTextField: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 32)
        tf.textColor = .white
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.keyboardType = .phonePad
        tf.textAlignment = .center
        tf.backgroundColor = UIColor.darkColor.dark
        tf.delegate = self
        return tf
    }()
    
    lazy var secondCodeTextField: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 32)
        tf.textColor = .white
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.keyboardType = .phonePad
        tf.textAlignment = .center
        tf.backgroundColor = UIColor.darkColor.dark
        tf.delegate = self
        return tf
    }()
    
    lazy var thirdCodeTextField: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 32)
        tf.textColor = .white
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.keyboardType = .phonePad
        tf.textAlignment = .center
        tf.backgroundColor = UIColor.darkColor.dark
        tf.delegate = self
        return tf
    }()
    
    lazy var forthCodeTextField: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.systemFont(ofSize: 32)
        tf.textColor = .white
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.keyboardType = .phonePad
        tf.textAlignment = .center
        tf.backgroundColor = UIColor.darkColor.dark
        tf.delegate = self
        return tf
    }()
    
    let doneButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor.purpuleColor.purple
        btn.tintColor = .white
        btn.layer.masksToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "done")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        return btn
    }()
    
    let resendCodeButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.tintColor = .black
        btn.layer.masksToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("ارسال کد", for: .normal)
        return btn
    }()
    
    let unSendCodeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.text = "کد را دریافت نکرده اید؟"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        UIApplication.shared.statusBarStyle = .default
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handelDismissKeyboard)))
        backgroundColor = .white
        NotificationCenter.default.addObserver(self, selector: #selector(handelKeyBoardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handelKeyBoardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        setupView()
    }
    
    @objc func handelKeyBoardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let keyBoardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
            let keyboardRectangle = keyBoardFrame.cgRectValue
            
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            
            bottomPadding = 0
            
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                bottomPadding = window?.safeAreaInsets.bottom
            }
            
            bottomConstraint?.constant = isKeyboardShowing ? -keyboardRectangle.height - 10 : -doneButtonBottonConstant!
            
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                
                self.layoutIfNeeded()
                
            }) { (completion) in
                
                
            }
        }
    }
    
    @objc fileprivate func handelDismissKeyboard() {
        self.endEditing(true)
    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var doneButtonBottonConstant: CGFloat?
    
    fileprivate func setupView() {
        
        addSubview(titleLabel)
        titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: getSizeScreen().height / 4).isActive = true
        titleLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        titleLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 45)
        resendCodeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        unSendCodeLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        addSubview(textFieldsView)
        textFieldsView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        textFieldsView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: getSizeScreen().height / 40).isActive = true
        textFieldsView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 1.3).isActive = true
        textFieldsView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 8).isActive = true
        
        textFieldsView.addSubview(firstCodeTextField)
        firstCodeTextField.rightAnchor.constraint(equalTo: textFieldsView.rightAnchor, constant: -8).isActive = true
        firstCodeTextField.topAnchor.constraint(equalTo: textFieldsView.topAnchor, constant: 5).isActive = true
        firstCodeTextField.bottomAnchor.constraint(equalTo: textFieldsView.bottomAnchor, constant: -5).isActive = true
        firstCodeTextField.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 6).isActive = true
        
        textFieldsView.addSubview(secondCodeTextField)
        secondCodeTextField.rightAnchor.constraint(equalTo: firstCodeTextField.leftAnchor, constant: -8).isActive = true
        secondCodeTextField.topAnchor.constraint(equalTo: textFieldsView.topAnchor, constant: 5).isActive = true
        secondCodeTextField.bottomAnchor.constraint(equalTo: textFieldsView.bottomAnchor, constant: -5).isActive = true
        secondCodeTextField.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 6).isActive = true
        
        textFieldsView.addSubview(thirdCodeTextField)
        thirdCodeTextField.rightAnchor.constraint(equalTo: secondCodeTextField.leftAnchor, constant: -8).isActive = true
        thirdCodeTextField.topAnchor.constraint(equalTo: textFieldsView.topAnchor, constant: 5).isActive = true
        thirdCodeTextField.bottomAnchor.constraint(equalTo: textFieldsView.bottomAnchor, constant: -5).isActive = true
        thirdCodeTextField.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 6).isActive = true
        
        textFieldsView.addSubview(forthCodeTextField)
        forthCodeTextField.rightAnchor.constraint(equalTo: thirdCodeTextField.leftAnchor, constant: -8).isActive = true
        forthCodeTextField.topAnchor.constraint(equalTo: textFieldsView.topAnchor, constant: 5).isActive = true
        forthCodeTextField.bottomAnchor.constraint(equalTo: textFieldsView.bottomAnchor, constant: -5).isActive = true
        forthCodeTextField.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 6).isActive = true
        
        addSubview(resendCodeButton)
        resendCodeButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -getSizeScreen().height / 16).isActive = true
        resendCodeButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        resendCodeButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        resendCodeButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
        resendCodeButton.addTarget(self, action: #selector(resendCode), for: .touchUpInside)
        
        addSubview(unSendCodeLabel)
        unSendCodeLabel.bottomAnchor.constraint(equalTo: resendCodeButton.topAnchor, constant: -5).isActive = true
        unSendCodeLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        unSendCodeLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        unSendCodeLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width).isActive = true
        
        doneButtonBottonConstant = getSizeScreen().height / 20 + getSizeScreen().height / 16 + getSizeScreen().height / 20 + 15
        
        addSubview(doneButton)
        doneButton.addTarget(self, action: #selector(handelVerfyingCode), for: .touchUpInside)
        doneButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        bottomConstraint = doneButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -(doneButtonBottonConstant!))
        bottomConstraint!.isActive = true
        doneButton.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 12).isActive = true
        doneButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 12 ).isActive = true
        doneButton.layer.cornerRadius = getSizeScreen().height / 24
        
        [firstCodeTextField, secondCodeTextField, thirdCodeTextField, forthCodeTextField].forEach({ $0.addTarget(self, action: #selector(editingChanged), for: .editingChanged) })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            self.forthCodeTextField.becomeFirstResponder()
        }
    }
    
    @objc func editingChanged(_ textField: UITextField) {
        
        if textField.text?.count == 1 && textField.text != "" {
            if textField == self.forthCodeTextField {
                thirdCodeTextField.becomeFirstResponder()
            }
            else if textField == self.thirdCodeTextField {
                secondCodeTextField.becomeFirstResponder()
            }
            else if textField == self.secondCodeTextField {
                firstCodeTextField.becomeFirstResponder()
            }
            else if textField == self.firstCodeTextField {
                handelVerfyingCode()
            }
        }
        else if textField.text?.count == 0 && textField.text == "" {
            if textField == self.firstCodeTextField {
                self.secondCodeTextField.becomeFirstResponder()
            }
            else if textField == self.secondCodeTextField {
                self.thirdCodeTextField.becomeFirstResponder()
            }
            else if textField == self.thirdCodeTextField {
                self.forthCodeTextField.becomeFirstResponder()
            }
        }
        
    }
    
    var fullCode = ""
    
    @objc fileprivate func handelVerfyingCode() {
        
        if firstCodeTextField.text == "" || firstCodeTextField.text == "" || firstCodeTextField.text == "" || firstCodeTextField.text == "" {
            let alert = CDAlertView(title: "", message: "کد را کامل وارد کنید", type: .warning)
            let action = CDAlertViewAction(title: "تأیید")
            alert.add(action: action)
            alert.show()
        }
        else {
            fullCode = forthCodeTextField.text! + thirdCodeTextField.text! + secondCodeTextField.text! + firstCodeTextField.text!
            SendMessageController!.showProfileController(code: fullCode)
            
            
            
        }
        
        
        
        
    }
    
    @objc fileprivate func resendCode() {
        
        let userDefult = UserDefaults()
        if let phoneNum = userDefult.value(forKey: "phoneNum") {
            
            shareInterface.SendSMS(mobile: phoneNum as! String)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 1
    }
    
}

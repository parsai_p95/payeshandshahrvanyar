//
//  slideImageMenuCell.swift
//  hamshahriyar
//
//  Created by apple on 11/2/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit

class slideImageMenuCell: baseCell {
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = .gray
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(imageView)
        imageView.frame = self.frame
        
    }
    
}

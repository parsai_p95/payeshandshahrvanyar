//
//  NewsCell.swift
//  hamshahriyar
//
//  Created by apple on 11/2/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit

class NewsCell: baseCell {
    
    let baseView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 10
        return view
    }()
    
    let newsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 10
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .lightGray
        imageView.image = UIImage(named: "image")
        return imageView
    }()
    
    let subjectLabel: UILabel = {
        let label = UILabel()
        label.text = "تست"
        label.textColor = .black
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let bodyLabel: UILabel = {
        let label = UILabel()
        label.text = "هخثفقمکنتبلمکلرذو.ئرذدتنملانمکرذنمردذنکئمکتبانکمنکئمکتبرذو.ئرذدتنملانمکرذنمردذنکئمکتبانکمنکئمکتبانکمتبلهخعمنباخنانکمتبلهخعمنباخنتتبلهخعمنباخنتت"
        label.textColor = .gray
        label.textAlignment = .right
        label.numberOfLines = 0
        return label
    }()
    
    let importantImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 10
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "important")?.withRenderingMode(.alwaysTemplate)
        imageView.backgroundColor = .white
        imageView.tintColor = .red
        imageView.clipsToBounds = true
        imageView.isHidden = true
        return imageView
    }()
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(baseView)
        addConstraintWithFormat(format: "H:|-10-[v0]-10-|", views: baseView)
        addConstraintWithFormat(format: "V:|-10-[v0]-10-|", views: baseView)
        
        baseView.addSubview(newsImageView)
        baseView.addConstraintWithFormat(format: "H:|[v0]|", views: newsImageView)
        baseView.addConstraintWithFormat(format: "V:|[v0(\(self.frame.height/1.7))]", views: newsImageView)
        
        baseView.addSubview(subjectLabel)
        subjectLabel.topAnchor.constraint(equalTo: newsImageView.bottomAnchor, constant: self.frame.height / 25).isActive = true
        subjectLabel.rightAnchor.constraint(equalTo: baseView.rightAnchor, constant: -self.frame.width / 15).isActive = true
        subjectLabel.heightAnchor.constraint(equalToConstant: self.frame.height / 15).isActive = true
        subjectLabel.widthAnchor.constraint(equalToConstant: self.frame.width ).isActive = true
        subjectLabel.font = UIFont.boldSystemFont(ofSize: self.frame.height / 20)
        
        baseView.addSubview(bodyLabel)
        let topConstraint = NSLayoutConstraint(item: bodyLabel, attribute: .top, relatedBy: .equal, toItem: subjectLabel, attribute: .bottom, multiplier: 1, constant: self.frame.height / 25)
        baseView.addConstraint(topConstraint)
        baseView.addConstraintWithFormat(format: "H:|-\(self.frame.width / 6)-[v0]-\(self.frame.width / 15)-|", views: bodyLabel)
        baseView.addConstraintWithFormat(format: "V:[v0(40)]", views: bodyLabel)
        bodyLabel.font = UIFont.boldSystemFont(ofSize: self.frame.height / 25)
        
        baseView.addSubview(importantImageView)
        baseView.addConstraintWithFormat(format: "H:|-10-[v0(35)]", views: importantImageView)
        baseView.addConstraintWithFormat(format: "V:[v0(35)]-\(self.frame.height / 5.5)-|", views: importantImageView)
    }
    
}

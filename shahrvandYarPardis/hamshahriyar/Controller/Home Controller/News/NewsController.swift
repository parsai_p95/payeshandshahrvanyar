//
//  NewsController.swift
//  hamshahriyar
//
//  Created by apple on 11/2/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class NewsController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    
    
    
    let loader: NVActivityIndicatorView = {
        let loader = NVActivityIndicatorView(frame: .zero)
        let loaderType = NVActivityIndicatorType.ballRotateChase
        loader.type = loaderType
        loader.color = .gray
        loader.padding = 5
        loader.translatesAutoresizingMaskIntoConstraints = false
        return loader
    }()
    
    let cellId = "cellId"
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor(r: 217, g: 220, b: 221)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let titleNavLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 25))
        label.textColor = .white
        label.textAlignment = .center
        label.text = "شهروند یار"
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        setupNavigationBar()
        setupCollectionView()
        setupLoader()
        getNews()
    }
    
    fileprivate func setupLoader() {
        
        view.addSubview(loader)
        loader.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loader.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loader.heightAnchor.constraint(equalToConstant: 50).isActive = true
        loader.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    fileprivate func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.lightColor.light
        navigationController?.navigationBar.tintColor = .white
        
        let image = UIImage(named: "back")
        let cancel = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handelDismiss))
        navigationItem.setLeftBarButton(cancel, animated: true)
        
        navigationItem.titleView = titleNavLabel
        titleNavLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 55)
    }
    
    @objc fileprivate func handelDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    var bodyArray = [""]
    var subjectArray = [""]
    var catArray = [""]
    var imageArray = [""]
    
    private func getNews() {
        
        bodyArray.removeAll()
        subjectArray.removeAll()
        catArray.removeAll()
        imageArray.removeAll()
        
        let urlString = baseURLString + "news.php"
        guard let url = URL(string: urlString) else {return}
        loader.startAnimating()
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            
            if let error = error {
                print(error)
            }
            
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    
                    if let Json = json as? NSDictionary {
                        if let array = Json["array"] as? [NSDictionary] {
                            for (key) in array {
                                if let body = key["body"] as? String {
                                    self.bodyArray.append(body)
                                    if let sunject = key["subject"] as? String  {
                                        self.subjectArray.append(sunject)
                                        if let cat = key["cat"] as? String {
                                            self.catArray.append(cat)
                                            if let img = key["img1"] as? String {
                                                self.imageArray.append(img)
                                            }
                                            
                                        }
                                    }
                                }
                            }
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                                self.loader.stopAnimating()
                            }
                        }
                    }
                }
                catch {
                    print(error)
                }
            }
            
            }.resume()
        
    }
    
    
    
    fileprivate func setupCollectionView() {
        view.addSubview(collectionView)
        collectionView.frame = view.frame
        collectionView.register(NewsCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.contentInset = UIEdgeInsets(top: getSizeScreen().height / 50, left: 0, bottom: getSizeScreen().height / 12, right: 0)
        collectionView.scrollIndicatorInsets = UIEdgeInsets(top: getSizeScreen().height / 40, left: 0, bottom: getSizeScreen().height / 40, right: 0)
        
        
    }
    
//    @objc func handelDismiss() {
//        dismiss(animated: true, completion: nil)
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return subjectArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! NewsCell
        cell.subjectLabel.text = subjectArray[indexPath.item ]
        cell.bodyLabel.text = bodyArray[indexPath.item ]
        let importantMode = catArray[indexPath.item ]
        let cellImage = imageArray[indexPath.item ]
        
        let imgBaseURl = baseURLString + cellImage
        
        cell.newsImageView.loadImageUsingURLString(urlString: imgBaseURl)
        if importantMode == "important" {
            cell.importantImageView.isHidden = false
        }
        else {
            cell.importantImageView.isHidden = true
        }
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height / 2.3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    var indexSelect = 0
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionView {
            //            getOneNews(item: indexPath.item)
            selectWhiteView.getOneNews(item: indexPath.item)
            indexSelect = indexPath.item
            setupSelectView()
        }
        
    }
    
    let selectBlackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handelDismissSelectBlackView))
        view.addGestureRecognizer(gesture)
        return view
    }()
    
    lazy var selectWhiteView: selectedShowNewsView = {
        let view = selectedShowNewsView()
        view.newsCollectionView = self
        view.backgroundColor = .white
        return view
    }()
    
    fileprivate func setupSelectView() {
        
        if let windows = UIApplication.shared.keyWindow {
            windows.addSubview(selectBlackView)
            selectBlackView.frame = windows.frame
            
            let gesture = UITapGestureRecognizer(target: self, action: #selector(handelDismissSelectBlackView))
            selectBlackView.addGestureRecognizer(gesture)
            
            let gesture2 = UITapGestureRecognizer(target: self, action: #selector(tempWhiteView))
            selectWhiteView.addGestureRecognizer(gesture2)
            
            selectBlackView.addSubview(selectWhiteView)
            setupShowSelectWhiteView()
            
            selectBlackView.alpha = 0
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.selectBlackView.alpha = 1
                
            }, completion: nil)
        }
        
    }
    
    @objc func tempWhiteView() {
        dismissBlackViewMode = true
    }
    
    var dismissBlackViewMode = false
    
    @objc fileprivate func handelDismissSelectBlackView() {
        dismissBlackViewMode = false
        if !dismissBlackViewMode {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.selectBlackView.alpha = 0
                
            }, completion: nil)
        }
    }
    
    fileprivate func setupShowSelectWhiteView() {
        
        if let windows = UIApplication.shared.keyWindow {
            selectWhiteView.frame.size.height = windows.frame.height / 1.3
            selectWhiteView.frame.size.width = windows.frame.width - 20
            selectWhiteView.layer.cornerRadius = 10
            selectWhiteView.layer.masksToBounds = true
            selectWhiteView.center = selectBlackView.center
        }
    }
    
    
}

//
//  sentRequestView.swift
//  hamshahriyar
//
//  Created by apple on 11/2/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import CDAlertView

public var selectCellRequsetView = 0

class sentRequestView: UIView, UICollectionViewDelegate, UICollectionViewDataSource,  UICollectionViewDelegateFlowLayout, UIScrollViewDelegate{
    
    let cellId = "cellId"
    
    var filterDateMode = false
    var dateSelect = ""
    private var subjectDateFilterArray: [String] = []
    private var statusDateFilterArray: [String] = []
    private var dateDateFilterArray: [String] = []
    private var answerDateFilterArray: [String] = []
    
    private let refreshControl = UIRefreshControl()
    
    var FllowUp: FollowUpTheRequestController?
    
    let loader: NVActivityIndicatorView = {
        let loader = NVActivityIndicatorView(frame: .zero)
        let loaderType = NVActivityIndicatorType.ballRotateChase
        loader.type = loaderType
        loader.color = .black
        loader.padding = 5
        loader.translatesAutoresizingMaskIntoConstraints = false
        return loader
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        if let userId = UserDefaults.standard.value(forKey: "userid") as? String {
            userID = userId
            getAllReq()
        }
        
        
        setupCollectionView()
        setupViews()
        
        addSubview(loader)
        loader.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loader.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        loader.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 8).isActive = true
        loader.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 8).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupCollectionView() {
        
        addSubview(collectionView)
        addConstraintWithFormat(format: "H:|[v0]|", views: collectionView)
        addConstraintWithFormat(format: "V:|[v0]|", views: collectionView)
        collectionView.register(sendReqCell.self, forCellWithReuseIdentifier: cellId)
        let topConstantCV = getSizeScreen().height / 15 + getSizeScreen().height / 17
        collectionView.contentInset = UIEdgeInsets(top: topConstantCV, left: 0, bottom: 15, right: 0)
        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        refreshControl.isHidden = true
    }
    
    @objc private func refreshWeatherData(_ sender: Any) {
        
        RefreshAllReq()
        
    }
    
    fileprivate func setupViews() {
        backgroundColor = .white
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if !filterDateMode {
            return subjectArray.count
        }
        else {
            return subjectDateFilterArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! sendReqCell
        if !filterDateMode {
            
            cell.subjectLabel.text = subjectArray[indexPath.item]
            cell.statusLabel.text = "وضعیت: \n\n " + statusArray[indexPath.item]
            cell.dateLabel.text = "تاریخ: \n\n" + dateArray[indexPath.item]
            let pasokh = answerArray[indexPath.item]
            if pasokh == "" {
                cell.answerAdminView.backgroundColor = UIColor.orangeColor.orange
            }
            else {
                cell.answerAdminView.backgroundColor = UIColor.greenColor.green
            }
        }
        else {
            cell.subjectLabel.text = subjectDateFilterArray[indexPath.item]
            cell.statusLabel.text = "وضعیت: \n\n " + statusDateFilterArray[indexPath.item]
            cell.dateLabel.text = "تاریخ: \n\n" + dateDateFilterArray[indexPath.item]
            let pasokh = answerDateFilterArray[indexPath.item]
            if pasokh == "" {
                cell.answerAdminView.backgroundColor = UIColor.orangeColor.orange
            }
            else {
                cell.answerAdminView.backgroundColor = UIColor.greenColor.green
            }
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: getSizeScreen().width, height: getSizeScreen().height / 4)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    var cellSelectIndex = 0
    var selectMode = false
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectMode = true
        selectCellRequsetView = indexPath.item
        WhiteView.getSelectIndex()
        getAllReq()
        
    }
    
    let blackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return view
    }()
    
    lazy var WhiteView: selectSenReqView = {
        let view = selectSenReqView()
        view.requsrtView = self
        return view
    }()
    
    fileprivate func showSelectItemView() {
        
        if let windows = UIApplication.shared.keyWindow {
            windows.addSubview(blackView)
            blackView.frame = windows.frame
            blackView.addSubview(WhiteView)
            WhiteView.frame.size.height = windows.frame.height / 1.8
            WhiteView.frame.size.width = windows.frame.width - 20
            WhiteView.center = blackView.center
            blackView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 1
                
            }, completion: nil)
        }
        
    }
    
    
    @objc func handelDismissBlackView() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blackView.alpha = 0
            
            self.WhiteView.fullTrackingCodeLabel.text = ""
            self.WhiteView.fullTrackingOrgnizationLabel.text = ""
            self.WhiteView.fullDateOfgetToOrgnizationLabel.text = ""
            self.WhiteView.answerTextView.text = ""
            
        }, completion: nil)
    }
    
    
    private var userID = ""
    private var subjectArray: [String] = []
    private var statusArray: [String] = []
    private var dateArray: [String] = []
    private var answerArray: [String] = []
    private var trackingCodeArray: [String] = []
    private var trackingOrgnizationArray: [String] = []
    private var dateOfgetToOrgnizationArray: [String] = []
    private var detailArray: [String] = []
    
    let dateFormatter = DateFormatter()
    
    
    fileprivate func getAllReq() {
        
        subjectArray.removeAll()
        statusArray.removeAll()
        dateArray.removeAll()
        answerArray.removeAll()
        trackingCodeArray.removeAll()
        trackingOrgnizationArray.removeAll()
        dateOfgetToOrgnizationArray.removeAll()
        detailArray.removeAll()
        
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd"
        

        let userInfoURL = baseURLString + "loadusereq.php?userid=\(userID)"
        let getUrl = userInfoURL.data(using: String.Encoding.ascii, allowLossyConversion: true)
        let urlString = String(data: getUrl!, encoding: String.Encoding.ascii)
        guard let url = URL(string: urlString!) else{return}
        loader.startAnimating()
        Alamofire.request(url, method: .get).responseJSON { (response) in
            
            if let data = response.data {
                let json = JSON(data)
                if let nullValue = json.null {
                    print(nullValue)
                    self.loader.stopAnimating()
                    let alert = CDAlertView(title: "", message: "درخواستی ثبت نشده است", type: CDAlertViewType.alarm)
                    let action = CDAlertViewAction(title: "تأیید")
                    alert.add(action: action)
                    alert.show()
                }
                let array = json["array"]
                self.subjectArray.removeAll()
                self.statusArray.removeAll()
                self.dateArray.removeAll()
                self.answerArray.removeAll()
                for key in array {
                    let userInfoJson = key.1
                    let subject = userInfoJson["subject"].string
                    let status = userInfoJson["status"].string
                    let trackCode = userInfoJson["trackingcode"].string
                    let trackCodeOrgnize = userInfoJson["owner"].string
                    if let dateOfGetToOrgnize = userInfoJson["tarikh-pasokh"].string {
                        self.dateOfgetToOrgnizationArray.append(dateOfGetToOrgnize)
                    }
                    else {
                        self.dateOfgetToOrgnizationArray.append("تاریخ ثبت نشده است")
                    }
                    let detail = userInfoJson["detail"].string
                    
                    let date = userInfoJson["created"].string
                    let persianDate = self.filterDateAndChangeEngToPersian(dateString: date!)
                    if let answer = userInfoJson["pasokh"].string {
                        self.answerArray.append(answer)
                    }
                    else {
                        self.answerArray.append("پاسخی دریافت نشد")
                    }
                    
                    self.subjectArray.append(subject!)
                    self.statusArray.append(status!)
                    self.dateArray.append(persianDate)
                    self.trackingCodeArray.append(trackCode!)
                    self.trackingOrgnizationArray.append(trackCodeOrgnize!)
                    self.detailArray.append(detail!)
                    
                    DispatchQueue.main.async {
                        
                        self.loader.stopAnimating()
                        self.collectionView.reloadData()
                        
                    }
                }
                
                //                ReqSelectItem.trackingCode =
                if self.selectMode {
                    DispatchQueue.main.async {
                        self.WhiteView.showDetailRequest(trackingCode: self.trackingCodeArray, trackingOrgnize: self.trackingOrgnizationArray, date: self.dateOfgetToOrgnizationArray, answer: self.answerArray, issue: self.subjectArray, Des: self.detailArray)
                        self.showSelectItemView()
                        self.selectMode = false
                    }
                }
                
            }
            
        }
        
    }
    
    fileprivate func RefreshAllReq() {
        
        
        
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd"
        let userDef = UserDefaults()
        if let userId = userDef.value(forKey: "userid") as? String {
            userID = userId
            
        }
        
        let userInfoURL = baseURLString + "loadusereq.php?userid=\(userID)"
        let getUrl = userInfoURL.data(using: String.Encoding.ascii, allowLossyConversion: true)
        let urlString = String(data: getUrl!, encoding: String.Encoding.ascii)
        guard let url = URL(string: urlString!) else{return}
        Alamofire.request(url, method: .get).responseJSON { (response) in
            
            if let data = response.data {
                let json = JSON(data)
                if let nullValue = json.null {
                    print(nullValue)
                    self.loader.stopAnimating()
                    let alert = CDAlertView(title: "", message: "درخواستی ثبت نشده است", type: CDAlertViewType.alarm)
                    let action = CDAlertViewAction(title: "تأیید")
                    alert.add(action: action)
                    alert.show()
                }
                let array = json["array"]

                self.subjectArray.removeAll()
                self.statusArray.removeAll()
                self.dateArray.removeAll()
                self.answerArray.removeAll()
                for key in array {
                    let userInfoJson = key.1
                    let subject = userInfoJson["subject"].string
                    let status = userInfoJson["status"].string
                    
                    let date = userInfoJson["created"].string
                    let persianDate = self.filterDateAndChangeEngToPersian(dateString: date!)
                    var answerr = "پاسخی موجود نیست"
                    if let answer = userInfoJson["pasokh"].string {
                        answerr = answer
                    }
                    
                    self.subjectArray.append(subject!)
                    self.statusArray.append(status!)
                    self.dateArray.append(persianDate)
                    self.answerArray.append(answerr)
                    DispatchQueue.main.async {
                        self.refreshControl.endRefreshing()
                        self.loader.stopAnimating()
                        self.collectionView.reloadData()
                        
                    }
                    
                    
                }
            }
            
        }
        
    }
    
    func filterDateAndChangeEngToPersian(dateString: String) -> String {
        var i = 0
        let delimiter = " "
        var token = dateString.components(separatedBy: delimiter)
        let zeroToken = token[0]
        let okChar = Set("1234567890")
        let orgDate = zeroToken.filter {okChar.contains($0) }
        let perOrgDate =  convertEngNumToPersianNum(num: orgDate)
        var temp = ""
        for num in perOrgDate {
            i += 1
            temp = temp + String(num)
            if i == 4 {
                temp = temp + "/"
                
            }
            if i == 6 {
                temp = temp + "/"
                
            }
        }
        
        return temp
    }
    
    func filterDateAndChangePersianToEng(dateString: String) -> String {
        var i = 0
        let delimiter = " "
        var token = dateString.components(separatedBy: delimiter)
        let zeroToken = token[0]
        print(zeroToken)
        var num = 0
        var numArray: [Int] = []
        for j in zeroToken {
            num += 1
            if String(j) == "/" {
                numArray.append(num)
            }
        }
        print(numArray)
        
        let okChar = Set("۱۲۳۴۵۶۷۸۹۰")
        let orgDate = zeroToken.filter {okChar.contains($0) }
        let perOrgDate =  convertPersianNumToEngNum(num: orgDate)
        var temp = ""
        
        
        for num in perOrgDate {
            i += 1
            temp = temp + String(num)
            if i == numArray[0] - 1 {
                temp = temp + "/"
                
            }
            if i == numArray[1] - 2 {
                temp = temp + "/"
                
            }
        }
        
        return temp
    }
    
    func convertEngNumToPersianNum(num: String) -> String {
        //let number = NSNumber(value: Int(num)!)
        
        let format = NumberFormatter()
        format.locale = Locale(identifier: "fa_IR")
        let number =   format.number(from: num)
        let faNumber = format.string(from: number!)
        return faNumber!
        
    }
    
    func convertPersianNumToEngNum(num: String) -> String {
        //let number = NSNumber(value: Int(num)!)
        
        let format = NumberFormatter()
        format.locale = Locale(identifier: "EN")
        let number =   format.number(from: num)
        let faNumber = format.string(from: number!)
        return faNumber!
        
    }
    
    func backToNormal() {
        
        subjectDateFilterArray.removeAll()
        statusDateFilterArray.removeAll()
        dateDateFilterArray.removeAll()
        answerDateFilterArray.removeAll()
        filterDateMode = false
        collectionView.reloadData()
    }
    
    func getFilterDateToShowRequest(dateFilter: String) {
        
        let engDate = filterDateAndChangePersianToEng(dateString: dateFilter)
        filterDateMode = true
        print(engDate)
        let userInfoURL = baseURLString + "datecustom.php?userid=\(userID)&cdate=\(engDate)"
        let getUrl = userInfoURL.data(using: String.Encoding.ascii, allowLossyConversion: true)
        let urlString = String(data: getUrl!, encoding: String.Encoding.ascii)
        guard let url = URL(string: urlString!) else{return}
        
        loader.startAnimating()
        Alamofire.request(url, method: .get).responseJSON { (response) in
            
            if let data = response.data {
                let json = JSON(data)
                if let nullValue = json.null {
                    print(nullValue)
                    self.loader.stopAnimating()
                    let alert = CDAlertView(title: "", message: "در این تاریخ درخواستی ثبت نشده است", type: CDAlertViewType.alarm)
                    let action = CDAlertViewAction(title: "تأیید", font: UIFont.systemFont(ofSize: 14), textColor: UIColor.greenColor.green, backgroundColor: .white, handler: { (action) -> Bool in
                        self.FllowUp?.handelCanelCalender()
                        return true
                    })
                    alert.add(action: action)
                    alert.show()
                }
                let array = json["array"]
                for key in array {
                    let userInfoJson = key.1
                    let subject = userInfoJson["subject"].string
                    let status = userInfoJson["status"].string
                    let filterStatus = self.filterDateAndChangeEngToPersian(dateString: status!)
                    
                    let date = userInfoJson["created"].string
                    let persianDate = self.filterDateAndChangeEngToPersian(dateString: date!)
                    if let answer = userInfoJson["pasokh"].string {
                        self.answerDateFilterArray.append(answer)
                    }
                    else {
                        self.answerDateFilterArray.append("پاسخی ثبت نشده است")
                    }
                    self.subjectDateFilterArray.append(subject!)
                    self.statusDateFilterArray.append(filterStatus)
                    self.dateDateFilterArray.append(persianDate)
                    
                    DispatchQueue.main.async {
                        
                        self.loader.stopAnimating()
                        
                        self.collectionView.reloadData()
                        
                    }
                    
                    
                }
            }
            
        }
        
        
    }
    
    
}

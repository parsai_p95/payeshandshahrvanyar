//
//  unsendSelectCellWhiteView.swift
//  hamshahriyar
//
//  Created by apple on 11/2/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit
import CDAlertView
import Alamofire
import SwiftyJSON

class unsendSelectCellWhiteView: UIView, UIScrollViewDelegate {
    
    var cityProblem: cityProblemsController?
    
    var reqId = ""
    
    var ProblemTypeArray: [String] = []
    var addressArray: [String] = []
    var titleArray: [String] = []
    var desArray: [String] = []
    var latArray: [String] = []
    var lntArray: [String] = []
    var imageSelectArray: [[Data]] = []
    var voiceSelectArray: [Data] = []
    var sendStatusModeArray: [Bool] = []
    
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.textAlignment = .right
        lbl.text = "جزئیات درخواست"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let topLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.blueColor.blue
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let subjectLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .lightGray
        lbl.textAlignment = .right
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let typeProblemLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .lightGray
        lbl.textAlignment = .right
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let adressLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .lightGray
        lbl.textAlignment = .right
        lbl.text = "آدرس : "
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let adressTextView: UITextView = {
        let tv = UITextView()
        tv.textColor = .lightGray
        tv.textAlignment = .right
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.isEditable = false
        return tv
    }()
    
    let descriptionLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .lightGray
        lbl.textAlignment = .right
        lbl.text = "توضیحات : "
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let descriptionTextView: UITextView = {
        let tv = UITextView()
        tv.textColor = .lightGray
        tv.textAlignment = .right
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.isEditable = false
        return tv
    }()
    
    let fileLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .lightGray
        lbl.textAlignment = .right
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let resendReqButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.tintColor = UIColor.blueColor.blue
        btn.setTitle("ارسال دوباره", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let changeReqButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.tintColor = UIColor.blueColor.blue
        btn.setTitle("تغییر درخواست", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let deleteButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.tintColor = UIColor.red
        btn.setTitle("حذف", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    var unsendView: unSentRequestView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        setupViews()
        
    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupViews() {
        
        addSubview(titleLabel)
        addSubview(topLine)
        addSubview(subjectLabel)
        addSubview(typeProblemLabel)
        addSubview(adressLabel)
        addSubview(adressTextView)
        addSubview(descriptionLabel)
        addSubview(descriptionTextView)
        addSubview(fileLabel)
        addSubview(resendReqButton)
        addSubview(deleteButton)
        addSubview(changeReqButton)
        
        resendReqButton.addTarget(self, action: #selector(resendAgain), for: .touchUpInside)
        deleteButton.addTarget(self, action: #selector(deleteItem), for: .touchUpInside)
        changeReqButton.addTarget(self, action: #selector(changeRequest), for: .touchUpInside)
        
        titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 10).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: getSizeScreen().height / 40).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 17).isActive = true
        titleLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 45)
        
        topLine.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5).isActive = true
        topLine.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 5).isActive = true
        topLine.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: getSizeScreen().height / 100).isActive = true
        topLine.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        subjectLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 8).isActive = true
        subjectLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        subjectLabel.topAnchor.constraint(equalTo: topLine.bottomAnchor, constant: getSizeScreen().height / 100).isActive = true
        subjectLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        subjectLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 45)
        
        typeProblemLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 8).isActive = true
        typeProblemLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        typeProblemLabel.topAnchor.constraint(equalTo: subjectLabel.bottomAnchor, constant: getSizeScreen().height / 100).isActive = true
        typeProblemLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        typeProblemLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 45)
        
        adressLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 8).isActive = true
        adressLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        adressLabel.topAnchor.constraint(equalTo: typeProblemLabel.bottomAnchor, constant: getSizeScreen().height / 100).isActive = true
        adressLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 25).isActive = true
        adressLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 45)
        
        adressTextView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 8).isActive = true
        adressTextView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        adressTextView.topAnchor.constraint(equalTo: adressLabel.bottomAnchor).isActive = true
        adressTextView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 10).isActive = true
        adressTextView.font = UIFont.systemFont(ofSize: getSizeScreen().height / 45)
        
        descriptionLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 8).isActive = true
        descriptionLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: adressTextView.bottomAnchor, constant: getSizeScreen().height / 100).isActive = true
        descriptionLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 25).isActive = true
        descriptionLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 45)
        
        descriptionTextView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 8).isActive = true
        descriptionTextView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        descriptionTextView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor).isActive = true
        descriptionTextView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 10).isActive = true
        descriptionTextView.font = UIFont.systemFont(ofSize: getSizeScreen().height / 45)
        
        fileLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 8).isActive = true
        fileLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        fileLabel.topAnchor.constraint(equalTo: descriptionTextView.bottomAnchor, constant: getSizeScreen().height / 100).isActive = true
        fileLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        fileLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 45)
        
        resendReqButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 15).isActive = true
        resendReqButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -getSizeScreen().width / 30).isActive = true
        
        changeReqButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: getSizeScreen().width / 15).isActive = true
        changeReqButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -getSizeScreen().width / 30).isActive = true
        
        deleteButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        deleteButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -getSizeScreen().width / 30).isActive = true
        
    }
    
    func fillItemOfWhiteView(subject: String, typeProblem: String, adress: String, description: String, file: [String]) {
        
        subjectLabel.text = "عنوان درخواست :    " + subject
        typeProblemLabel.text =  "نوع مشکل :    " + typeProblem
        adressTextView.text = adress
        descriptionTextView.text = description
        let numImageString = "عکس : \(file[0])"
        let numvideoString = "فیلم : \(file[1])"
        let numvoiceString = "صدا : \(file[2])"
        fileLabel.text = "پیوست :    " + numImageString + "  " + numvideoString + "  " + numvoiceString
    }
    
    
    
    var IndexDelete = 0
    let userDefult = UserDefaults()
    
    @objc fileprivate func deleteItem() {
        
        if let indexDelete = self.unsendView?.selectCell {
            IndexDelete = indexDelete
        }
        
        if let pta = self.userDefult.value(forKey: "ProblemTypeArray") {
            ProblemTypeArray = pta as! [String]
        }
        if let ar = userDefult.value(forKey: "addressArray") {
            addressArray = ar as! [String]
        }
        if let pta = userDefult.value(forKey: "titleArray") {
            titleArray = pta as! [String]
        }
        if let da = userDefult.value(forKey: "desArray") {
            desArray = da as! [String]
        }
        if let la = userDefult.value(forKey: "latArray") {
            latArray = la as! [String]
        }
        if let lna = userDefult.value(forKey: "lntArray") {
            lntArray = lna as! [String]
        }
        if let pta = userDefult.value(forKey: "imageSelectArray") {
            imageSelectArray = pta as! [[Data]]
        }
        if let pta = userDefult.value(forKey: "voiceSelectArray") {
            voiceSelectArray = pta as! [Data]
        }
        
        if let pta = userDefult.value(forKey: "sendStatusModeArray") as? [Bool] {
            sendStatusModeArray = pta
        }
        
        let alert = CDAlertView(title: "", message: "آیا مطمئن هستید؟", type: CDAlertViewType.warning)
        let action1 = CDAlertViewAction(title: "بله", font: UIFont.systemFont(ofSize: 15), textColor: UIColor.red, backgroundColor: nil) { (action) -> Bool in
            
            
            self.ProblemTypeArray.remove(at: self.IndexDelete)
            self.addressArray.remove(at: self.IndexDelete)
            self.titleArray.remove(at: self.IndexDelete)
            self.desArray.remove(at: self.IndexDelete)
            self.latArray.remove(at: self.IndexDelete)
            self.lntArray.remove(at: self.IndexDelete)
            self.imageSelectArray.remove(at: self.IndexDelete)
            self.voiceSelectArray.remove(at: self.IndexDelete)
            self.sendStatusModeArray.remove(at: self.IndexDelete)
            
            self.userDefult.setValue(self.ProblemTypeArray, forKey: "ProblemTypeArray")
            self.userDefult.setValue(self.addressArray, forKey: "addressArray")
            self.userDefult.setValue(self.titleArray, forKey: "titleArray")
            self.userDefult.setValue(self.desArray, forKey: "desArray")
            self.userDefult.setValue(self.latArray, forKey: "latArray")
            self.userDefult.setValue(self.lntArray, forKey: "lntArray")
            self.userDefult.setValue(self.imageSelectArray, forKey: "imageSelectArray")
            self.userDefult.setValue(self.voiceSelectArray, forKey: "voiceSelectArray")
            self.userDefult.setValue(self.sendStatusModeArray, forKey: "sendStatusModeArray")
            
            self.unsendView?.showNumBudgeOfOfflineReq(ProblemTypeArray: self.ProblemTypeArray)
            self.unsendView?.getCoreDataUnSendReq()
            return true
        }
        let action2 = CDAlertViewAction(title: "خیر", font: UIFont.systemFont(ofSize: 15), textColor: UIColor.greenColor.green, backgroundColor: nil, handler: nil)
        
        alert.add(action: action1)
        alert.add(action: action2)
        alert.show()
        
        
        
    }
    
    @objc fileprivate func changeRequest() {
        
        unsendView?.handelDismissBlackView()
        showProblemView()
    }
    
    var problemType: String?
    
    let blackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)
        return view
    }()
    
    let whiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    var blackViewSelectedMode = true
    
    lazy var TypeOfProblemView: typeOfProblemView = {
        let view = typeOfProblemView()
        view.unsendReqView = self
        view.editReqViewMode = true
        return view
    }()
    
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.delegate = self
        scrollView.isHidden = true
        return scrollView
    }()
    
    lazy var SendRequsetView: editUnsendReqView = {
        let view = editUnsendReqView()
        view.unsendView = self
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func showProblemView() {
        
        if let windows = UIApplication.shared.keyWindow {
            windows.addSubview(blackView)
            blackView.frame = windows.frame
            
            let gesture = UITapGestureRecognizer(target: self, action: #selector(handelDismissBlackView))
            let gesture2 = UITapGestureRecognizer(target: self, action: #selector(handelUnSelectWhiteVIew))
            blackView.addGestureRecognizer(gesture)
            whiteView.addGestureRecognizer(gesture2)
            blackView.addSubview(whiteView)
            setupWhiteView()
            blackView.alpha = 0
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 1
                
            }, completion: nil)
            
        }
    }
    
    func showNextLevelOfTypeOfProblemView() {
        TypeOfProblemView.isHidden = true
        scrollView.isHidden = false
    }
    
    func returnSendRequestView() {
        TypeOfProblemView.isHidden = false
        scrollView.isHidden = true
    }
    
    fileprivate func setupWhiteView() {
        
        if let windows = UIApplication.shared.keyWindow {
            whiteView.frame.size.height = windows.frame.height / 1.5
            whiteView.frame.size.width = windows.frame.width - 20
            whiteView.layer.cornerRadius = 10
            whiteView.layer.masksToBounds = true
            whiteView.center = blackView.center
            whiteView.addSubview(TypeOfProblemView)
            whiteView.addConstraintWithFormat(format: "H:|[v0]|", views: TypeOfProblemView)
            whiteView.addConstraintWithFormat(format: "V:|[v0]|", views: TypeOfProblemView)
            
            whiteView.addSubview(scrollView)
            whiteView.addConstraintWithFormat(format: "H:|[v0]|", views: scrollView)
            whiteView.addConstraintWithFormat(format: "V:|[v0]|", views: scrollView)
            
            //            let gesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
            //            scrollView.addGestureRecognizer(gesture)
            
            scrollView.addSubview(SendRequsetView)
            SendRequsetView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
            SendRequsetView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
            SendRequsetView.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
            SendRequsetView.rightAnchor.constraint(equalTo: scrollView.rightAnchor).isActive = true
            SendRequsetView.heightAnchor.constraint(equalToConstant: getSizeScreen().height/1.2).isActive = true
            SendRequsetView.addConstraint(NSLayoutConstraint(item: SendRequsetView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0, constant: ((getSizeScreen().width))-20))
            
        }
        
        SendRequsetView.getInfoRequest()
        
    }
    
    @objc fileprivate func handelUnSelectWhiteVIew() {
        blackViewSelectedMode = false
    }
    
    @objc func handelDismissBlackView() {
        
        SendRequsetView.budgeValue = 0
        SendRequsetView.budgeVoiceValue = 0
        SendRequsetView.imageBudgeLabel.text = ""
        SendRequsetView.voiceBudgeLabel.text = ""
        SendRequsetView.videoBudgeLabel.isHidden = true
        SendRequsetView.voiceBudgeLabel.isHidden = true
        SendRequsetView.imageBudgeLabel.isHidden = true
        SendRequsetView.addressTextField.text = ""
        SendRequsetView.issuerRequsestTextField.text = ""
        SendRequsetView.descriptionTextField.text = ""
        //        sendRequsetImagesArray.removeAll()
        unsendView?.FllowUp?.sendRequsetImagesArray.removeAll()
        //
        TypeOfProblemView.criticismSwitch.isOn = false
        TypeOfProblemView.appreciationSwitch.isOn = false
        
        TypeOfProblemView.SuggestionSwitch.isOn = false
        
        blackViewSelectedMode = true
        self.endEditing(true)
        if blackViewSelectedMode {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 0
                
            }, completion: nil)
        }
        
    }
    
    
    
    @objc fileprivate func resendAgain() {
        
        let alert = CDAlertView(title: "", message: "آیا برای ارسال مطمئن هسیتید؟", type: CDAlertViewType.notification)
        let action = CDAlertViewAction(title: "بله", font: UIFont.systemFont(ofSize: getSizeScreen().height / 55), textColor: UIColor.red, backgroundColor: .white) { (action) -> Bool in
            self.unsendView?.handelDismissBlackView()
            self.sendProblem()
            
            return true
        }
        let action2 = CDAlertViewAction(title: "خیر")
        alert.add(action: action)
        alert.add(action: action2)
        alert.show()
    }
    
    var uploadMode = false
    var selectIndex: Int?
    
    func updateUserInterface() {
        guard let status = Network.reachability?.status else { return }
        print("Reachability Summary")
        print("Status:", status)
        print("HostName:", Network.reachability?.hostname ?? "nil")
        print("Reachable:", Network.reachability?.isReachable ?? "nil")
        print("Wifi:", Network.reachability?.isReachableViaWiFi ?? "nil")
        
        if !(Network.reachability?.isReachable)! && !Reachabilities.isConnectedToNetwork() {
            
            if uploadMode {
                stopSending()
                showAlertFaile()
                uploadMode = false
            }
            else {
                showInernetAlert()
            }
            
        }
    }
    
    func showInernetAlert() {
        
        if uploadMode {
            stopSending()
            showAlertFaile()
        }
        else {
            
            let alert = CDAlertView(title: "", message: "دسترسی به اینترنت مقدور نیست", type: CDAlertViewType.warning)
            let action = CDAlertViewAction(title: "تأیید", font:UIFont.systemFont(ofSize: 14), textColor: .black, backgroundColor: .white) { (act) -> Bool in
                
                if !Reachabilities.isConnectedToNetwork() {
                    self.showInernetAlert()
                }
                
                return true
            }
            alert.add(action: action)
            alert.show()
            
        }
    }
    
    let progressWhiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    let progressBlackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.6)
        return view
    }()
    
    let titleProgressLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "درحال فشرده سازی و ارسال اطلاعات..."
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let progressLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .gray
        lbl.text = "۱۵٪"
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let progressView: UIProgressView = {
        let progress = UIProgressView(progressViewStyle: .bar)
        progress.progress = 0.5
        progress.trackTintColor = .white
        progress.progressTintColor = UIColor.blueColor.blue
        progress.translatesAutoresizingMaskIntoConstraints = false
        return progress
    }()
    
    let cancelProgressAndUploadProblemButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.tintColor = .red
        btn.setTitle("لفو", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    func showProgressView() {
        
        if let windows = UIApplication.shared.keyWindow {
            
            windows.addSubview(progressBlackView)
            progressBlackView.frame = windows.frame
            progressBlackView.addSubview(progressWhiteView)
            setupProgressWhiteView()
            progressBlackView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.progressBlackView.alpha = 1
                
            }, completion: nil)
        }
        
    }
    
    fileprivate func setupProgressWhiteView() {
        if let windows = UIApplication.shared.keyWindow {
            
            progressWhiteView.frame.size.height = windows.frame.height / 4 - 10
            progressWhiteView.frame.size.width = windows.frame.width - 50
            progressWhiteView.layer.cornerRadius = 10
            progressWhiteView.layer.masksToBounds = true
            progressWhiteView.center = progressBlackView.center
            
            progressWhiteView.addSubview(titleProgressLabel)
            titleProgressLabel.topAnchor.constraint(equalTo: progressWhiteView.topAnchor, constant: 15).isActive = true
            titleProgressLabel.rightAnchor.constraint(equalTo: progressWhiteView.rightAnchor).isActive = true
            titleProgressLabel.leftAnchor.constraint(equalTo: progressWhiteView.leftAnchor).isActive = true
            titleProgressLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
            titleProgressLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
            
            progressWhiteView.addSubview(progressLabel)
            progressLabel.topAnchor.constraint(equalTo: titleProgressLabel.bottomAnchor, constant: 15).isActive = true
            progressLabel.rightAnchor.constraint(equalTo: progressWhiteView.rightAnchor).isActive = true
            progressLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
            progressLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
            progressLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 52)
            
            progressWhiteView.addSubview(progressView)
            progressView.rightAnchor.constraint(equalTo: progressLabel.leftAnchor).isActive = true
            progressView.leftAnchor.constraint(equalTo: progressWhiteView.leftAnchor, constant: 10).isActive = true
            progressView.centerYAnchor.constraint(equalTo: progressLabel.centerYAnchor).isActive = true
            
            progressWhiteView.addSubview(cancelProgressAndUploadProblemButton)
            cancelProgressAndUploadProblemButton.bottomAnchor.constraint(equalTo: progressWhiteView.bottomAnchor, constant: -20).isActive = true
            cancelProgressAndUploadProblemButton.centerXAnchor.constraint(equalTo: progressWhiteView.centerXAnchor).isActive = true
            cancelProgressAndUploadProblemButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
            cancelProgressAndUploadProblemButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
            cancelProgressAndUploadProblemButton.addTarget(self, action: #selector(stopSending), for: .touchUpInside)
            
        }
    }
    
    @objc fileprivate func stopSending() {
        dismissBackProgressView()
        uploadRequest?.cancel()
        uploadRequest = nil
    }
    
    @objc fileprivate func dismissBackProgressView() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.progressBlackView.alpha = 0
            
        }, completion: nil)
    }
    
    private var uploadRequest: Request?
    private var uploadFailed = true
    var urlVoice: URL!
    
    func sendProblem() {
        var index = 0
        if let Index = selectIndex {
            index = Index
        }
        
        uploadMode = true
        self.updateUserInterface()
        let userDefult = UserDefaults()
        let userID = userDefult.value(forKey: "userid") as? String
        let latArray = userDefult.value(forKey: "latArray") as! [String]
        let lat = latArray[index]
        let longArray = userDefult.value(forKey: "lntArray") as! [String]
        let long = longArray[index]
        
        let subject = userDefult.value(forKey: "titleArray") as! [String]
        let issue = subject[index]
        let typeProblem = userDefult.value(forKey: "ProblemTypeArray") as! [String]
        let problemType = typeProblem[index]
        let Address = userDefult.value(forKey: "addressArray") as! [String]
        let address = Address[index]
        let description = userDefult.value(forKey: "desArray") as! [String]
        let Description = description[index]
        let images = userDefult.value(forKey: "imageSelectArray") as! [[Data]]
        let sendRequsetImagesArray = images[index]
        let voice = userDefult.value(forKey: "voiceSelectArray") as! [Data]
        let video = userDefult.value(forKey: "videoSelectArray") as! [Data]
        let videoData = video[index]
        let voiceData = voice[index]
        let userInfoURL = baseURLString + "subrequest.php"
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        
        let parameter = ["ProblemType":problemType, "title": issue, "userid": userID!, "addrss": address, "des": Description, "lat": lat, "lnt": long]
        
        //        let getUrl = userInfoURL.data(using: String.Encoding.ascii, allowLossyConversion: true)
        //        let urlString = String(data: getUrl!, encoding: String.Encoding.ascii)
        guard let url = URL(string: userInfoURL) else{return}
        var counter = 0
        showProgressView()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            
            
            var numimgaeloop = 1
            for im in sendRequsetImagesArray {
                multipartFormData.append(im, withName: "img\(numimgaeloop)", fileName: "image.jpg", mimeType: "image/jpg")
                numimgaeloop+=1
            }
            
            multipartFormData.append(voiceData, withName: "audio", fileName: "1.mp3", mimeType: "audio/mp3")
            multipartFormData.append(videoData, withName: "video", fileName: "1.mp4", mimeType: "video/mp4")
            
            for (key, value) in parameter {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            
            
            switch result {
                
                
            case .success(let upload, _, _):
                
                self.uploadRequest = upload
                //                self.handelDismissBlackView()
                
                upload.uploadProgress(closure: { (progress) in
                    
                    let numProgress = progress.fractionCompleted
                    let roundProgress = self.roundDoubleNumber(number: numProgress)
                    self.progressView.progress = Float(roundProgress)
                    let progressTextLabel = String(roundProgress * 100)  + "%"
                    self.progressLabel.text = progressTextLabel
                    
                    if progressTextLabel == "100.0%" {
                        counter += 1
                        if counter == 1 {
                            self.dismissBackProgressView()
                            self.progressView.progress = 0.0
                            self.progressLabel.text = ""
                            self.showAlertSecsess()
                            
                        }
                        
                        
                    }
                })
                
                
            case .failure(let encodingError):
                print(encodingError.localizedDescription)
                self.showAlertFaile()
            }
            
            
            
        }
        
    }
    
    
    func roundDoubleNumber(number: Double) -> Double {
        
        return Double(round(10*number)/10)
    }
    
    fileprivate func showAlertSecsess() {
        
        let alert = CDAlertView(title: "با موفقیت انجام شد", message: "شما مینوانید از بخش پیگیری درخواست وضعیت درخواست خود را دنیال کنید", type: CDAlertViewType.success)
        alert.titleTextColor = UIColor(r: 25, g: 204, b: 12)
        alert.messageTextColor = .darkGray
        let action = CDAlertViewAction(title: "تایید", font: UIFont.systemFont(ofSize: getSizeScreen().height / 50), textColor: UIColor.blueColor.blue, backgroundColor: .white, handler: nil)
        alert.add(action: action)
        alert.show()
        var sendStatus = userDefult.value(forKey: "sendStatusModeArray") as! [Bool]
        sendStatus[SelectIndexCell!] = true
        userDefult.setValue(sendStatus, forKey: "sendStatusModeArray")
        unsendView?.getCoreDataUnSendReq()
        
    }
    
    fileprivate func showAlertFaile() {
        dismissBackProgressView()
        let alert = CDAlertView(title: "مشکل در ارسال اطلاعات", message: "درخواست شما ارسال نشد، برای ارسال مجدد به بخش پیگیری درخواست مراجعه کنید", type: CDAlertViewType.error)
        alert.titleTextColor = .red
        alert.messageTextColor = .darkGray
        //        let action = CDAlertViewAction(title: "تایید", font: UIFont.systemFont(ofSize: getSizeScreen().height / 50), textColor: UIColor.blueColor.blue, backgroundColor: .white, handler: nil)
        let action = CDAlertViewAction(title: "تایید", font: UIFont.systemFont(ofSize: getSizeScreen().height / 50), textColor: UIColor.blueColor.blue, backgroundColor: .white) { (action) -> Bool in
            //            self.saveRequestToCoreDataDB(issue: self.probIssue!, address: self.probAdress!, Description: self.probDescription!)
            return true
        }
        alert.add(action: action)
        alert.show()
        
    }
    
}

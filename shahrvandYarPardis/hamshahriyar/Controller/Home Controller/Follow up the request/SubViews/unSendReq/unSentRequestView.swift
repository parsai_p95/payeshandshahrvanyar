//
//  unSentRequestView.swift
//  hamshahriyar
//
//  Created by apple on 11/2/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit

var SelectIndexCell: Int?
var reqSelectCell: Int?

class unSentRequestView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cellId"
    
    var problemTypeArray: [String] = []
    
    let userDefult = UserDefaults()
    
    var FllowUp: FollowUpTheRequestController?
    
    private let refreshControl = UIRefreshControl()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        
        getCoreDataUnSendReq()
        setupCollectionView()
        setupViews()
    }
    
    fileprivate func setupCollectionView() {
        
        addSubview(collectionView)
        addConstraintWithFormat(format: "H:|[v0]|", views: collectionView)
        addConstraintWithFormat(format: "V:|[v0]|", views: collectionView)
        collectionView.register(unSendReqCell.self, forCellWithReuseIdentifier: cellId)
        let topConstantCV = getSizeScreen().height / 15 + getSizeScreen().height / 17
        // getSizeScreen().height / 10
        collectionView.contentInset = UIEdgeInsets(top: topConstantCV, left: 0, bottom: 15, right: 0)
        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        refreshControl.isHidden = true
        
    }
    
    @objc private func refreshWeatherData(_ sender: Any) {
        
        getCoreDataUnSendReq()
        
    }
    
    var resendArray: [Bool] = []
    
    func getCoreDataUnSendReq() {
        
        problemTypeArray.removeAll()
        
        handelDismissBlackView()
        
        if let problem = userDefult.value(forKey: "ProblemTypeArray") as? [String] {
            problemTypeArray = problem
        }
        
        
        refreshControl.endRefreshing()
        collectionView.reloadData()
        
    }
    
    func showNumBudgeOfOfflineReq(ProblemTypeArray: [String]) {
        
        FllowUp?.showNumBudgeOfOfflineReq(ProblemTypeArray: ProblemTypeArray)
        
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupViews() {
        backgroundColor = .white
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return problemTypeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! unSendReqCell
        let subject = userDefult.value(forKey: "titleArray") as! [String] //currDateArray
        let date = userDefult.value(forKey: "currDateArray") as! [String]
        cell.subjectLabel.text = subject[indexPath.item]
        cell.dateLabel.text = "تاریخ: \n\n" + date[indexPath.item]
        let resend = userDefult.value(forKey: "sendStatusModeArray") as! [Bool]
        
        let resendStatus = resend[indexPath.item]
        if resendStatus {
            cell.statusLabel.text = "وضعیت: \n\n ارسال شده"
            cell.statusLabel.textColor = UIColor.greenColor.green
        }
        else {
            cell.statusLabel.text = "وضعیت: \n\n ارسال نشده"
            cell.statusLabel.textColor = .lightGray
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: getSizeScreen().width, height: getSizeScreen().height / 4)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    var userid: String?
    var lat: [String] = []
    var long: [String] = []
    
    var selectCell: Int?
    var fileImageStr = ""
    var filterVoiceStr = ""
    var filterVideoStr = ""
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        SelectIndexCell = indexPath.item
        selectCell = indexPath.item
        
        
        let subject = userDefult.value(forKey: "titleArray") as! [String]
        let typeProblem = userDefult.value(forKey: "ProblemTypeArray") as! [String]
        let address = userDefult.value(forKey: "addressArray") as! [String]
        let description = userDefult.value(forKey: "desArray") as! [String]
        userid = userDefult.value(forKey: "userid") as? String
        lat = userDefult.value(forKey: "latArray") as! [String]
        long = userDefult.value(forKey: "lntArray") as! [String]
        let images = userDefult.value(forKey: "imageSelectArray") as! [[Data]]
        let voice = userDefult.value(forKey: "voiceSelectArray") as! [Data]
        let video = userDefult.value(forKey: "videoSelectArray") as? [Data] ?? []
        
        let voiceData = voice[indexPath.item]
        let videoeData = video[indexPath.item]
        
        if voiceData.count == 0 {
            filterVoiceStr = "۰"
        }
        else {
            filterVoiceStr = "۱"
        }
        let selectImage = images[indexPath.item]
        
        if selectImage.count != 0 {
            fileImageStr = convertEngNumToPersianNum(num: String(selectImage.count))
        }
        else {
            fileImageStr = "۰"
        }
        
        if videoeData.count != 0 {
            filterVideoStr = "۱"
        }
        else {
            filterVideoStr = "۰"
        }
        
        
        whiteView.fillItemOfWhiteView(subject: subject[indexPath.item], typeProblem: typeProblem[indexPath.item], adress: address[indexPath.item], description: description[indexPath.item], file: [fileImageStr, filterVideoStr, filterVoiceStr])
        showSelectItemView()
        
    }
    
    func convertEngNumToPersianNum(num: String) -> String {
        //let number = NSNumber(value: Int(num)!)
        
        let format = NumberFormatter()
        format.locale = Locale(identifier: "fa_IR")
        let number =   format.number(from: num)
        let faNumber = format.string(from: number!)
        return faNumber!
        
    }
    
    let blackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return view
    }()
    
    lazy var whiteView: unsendSelectCellWhiteView = {
        let view = unsendSelectCellWhiteView()
        view.unsendView = self
        return view
    }()
    
    fileprivate func showSelectItemView() {
        
        whiteView.selectIndex = selectCell
        
        let blackViewGesture = UITapGestureRecognizer(target: self, action: #selector(handelDismissBlackView))
        let whiteViewGesture = UITapGestureRecognizer(target: self, action: #selector(handelUnDismissWhiteView))
        blackView.addGestureRecognizer(blackViewGesture)
        whiteView.addGestureRecognizer(whiteViewGesture)
        
        if let windows = UIApplication.shared.keyWindow {
            windows.addSubview(blackView)
            blackView.frame = windows.frame
            blackView.addSubview(whiteView)
            whiteView.frame.size.height = windows.frame.height / 1.4
            whiteView.frame.size.width = windows.frame.width
            whiteView.center = blackView.center
            blackView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 1
                
            }, completion: nil)
        }
        
    }
    
    @objc func handelDismissBlackView() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blackView.alpha = 0
            
        }, completion: nil)
    }
    
    @objc fileprivate func handelUnDismissWhiteView() {
        
        self.blackView.alpha = 1
        
    }
    
    
    
}

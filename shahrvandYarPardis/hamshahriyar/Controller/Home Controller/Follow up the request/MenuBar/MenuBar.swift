//
//  MenuBar.swift
//  hamshahriyar
//
//  Created by apple on 11/2/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit


class MenuBar: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cellId"
    let cellName = ["ارسال شده", "ارسال نشده"]
    
    var FollowUp: FollowUpTheRequestController?
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.lightColor.light
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        collectionView.register(MenuCell.self, forCellWithReuseIdentifier: cellId)
        
        addSubview(collectionView)
        addConstraintWithFormat(format: "H:|[v0]|", views: collectionView)
        addConstraintWithFormat(format: "V:|[v0]|", views: collectionView)
        
        let selectedIndexPath = NSIndexPath(item: 0, section: 0)
        collectionView.selectItem(at: selectedIndexPath as IndexPath, animated: false, scrollPosition: .top)
        
        setuoHorizentalBar()
    }
    
    var horizentalBarLeftAnchor: NSLayoutConstraint?
    
    func setuoHorizentalBar() {
        let horizentalBarView = UIView()
        horizentalBarView.translatesAutoresizingMaskIntoConstraints = false
        horizentalBarView.backgroundColor = UIColor.darkColor.dark
        addSubview(horizentalBarView)
        horizentalBarLeftAnchor = horizentalBarView.leftAnchor.constraint(equalTo: self.leftAnchor)
        horizentalBarLeftAnchor?.isActive = true
        horizentalBarView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        horizentalBarView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/2).isActive = true
        horizentalBarView.heightAnchor.constraint(equalToConstant: 5).isActive = true
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        FollowUp?.scrollToMenuIndex(menuIndex: indexPath.item)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 2
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MenuCell
        cell.label.text = cellName[indexPath.item]
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: frame.width / 2, height: frame.height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class MenuCell: baseCell {
    
    var label: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textAlignment = .center
        return lbl
    }()
    
    override var isHighlighted: Bool
        {
        didSet
        {
            if isHighlighted
            {
                label.textColor = .white
                //baseView.backgroundColor = UIColor(r: 247, g: 133, b: 138)
                //setupGradiantView()
            }
            else
            {
                label.textColor = .lightGray
                //  baseView.backgroundColor = .white
            }
        }
    }
    
    override var isSelected: Bool
        {
        didSet
        {
            if isSelected
            {
                label.textColor = .white
                // baseView.backgroundColor = UIColor(r: 247, g: 133, b: 138)
                //                setupGradiantView()
            }
            else
            {
                label.textColor = .lightGray
                //baseView.backgroundColor = .white
            }
        }
    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    override func setupViews() {
        super.setupViews()
        
        
        
        addSubview(label)
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: getSizeScreen().height / 60)
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.widthAnchor.constraint(equalToConstant: frame.width / 1.5).isActive = true
        addConstraint(NSLayoutConstraint(item: label, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        
    }
    
}

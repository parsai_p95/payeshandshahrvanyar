//
//  TicketCell.swift
//  hamshahriyar
//
//  Created by apple on 11/3/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit

class TicketCell: baseCell {
    
    let messageTextView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 16)
        textView.text = "Sample Messages"
        textView.textAlignment = .right
        textView.isEditable = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.backgroundColor = .clear
        return textView
    }()
    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.text = "۱ ساعت پیش"
        label.textColor = .lightGray
        label.textAlignment = .left
        //        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let textBubbleView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        //        view.backgroundColor = .clear
        return view
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "top_logo")
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    static let whiteBubbleImage = UIImage(named: "rightBubbleFill")?.resizableImage(withCapInsets: UIEdgeInsets(top: 20, left: 24, bottom: 20, right: 24)).withRenderingMode(.alwaysTemplate)
    
    static let GreenBubbleImage = UIImage(named: "leftBubbleFill")?.resizableImage(withCapInsets: UIEdgeInsets(top: 20, left: 24, bottom: 20, right: 24)).withRenderingMode(.alwaysTemplate)
    
    let bubbleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = TicketCell.whiteBubbleImage
        imageView.tintColor = .red
        return imageView
    }()
    
    var rightConstraint: NSLayoutConstraint?
    
    override func setupViews() {
        super.setupViews()
        
        
        
        let screenSize = UIScreen.main.bounds.height / 16
        
        //        let fontSize = UIScreen.main.bounds.height / 35
        //
        //        messageTextView.font = UIFont.systemFont(ofSize: fontSize)
        
        addSubview(textBubbleView)
        addSubview(messageTextView)
        
        addSubview(profileImageView)
        addConstraintWithFormat(format: "H:|-8-[v0(\(screenSize))]|", views: profileImageView)
        addConstraintWithFormat(format: "V:[v0(\(screenSize))]|", views: profileImageView)
        profileImageView.layer.cornerRadius = screenSize / 2
        
        textBubbleView.addSubview(bubbleImageView)
        addConstraintWithFormat(format: "H:|[v0]|", views: bubbleImageView)
        addConstraintWithFormat(format: "V:|[v0]|", views: bubbleImageView)
        
        addSubview(timeLabel)
        //        timeLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -30).isActive = true
        //
        ////        rightConstraint = NSLayoutConstraint(item: timeLabel, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -30)
        ////        addConstraint(rightConstraint!)
        //        timeLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: screenSize + 40).isActive = true
        //        timeLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true
        //        timeLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
}

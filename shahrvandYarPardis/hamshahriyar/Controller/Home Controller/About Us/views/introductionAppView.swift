//
//  introductionAppView.swift
//  hamshahriyar
//
//  Created by apple on 11/3/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit

class introductionAppView: UIView {
    
    let topWhiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2
        return view
    }()
    
    let logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "top_logo"))
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let nameOpLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.text = "شهرداری پردیس"
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let designAppLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.textAlignment = .center
        label.text = "سامانه هوشمند نظارت همگانی شهروندیار"
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.textAlignment = .center
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupViews() {
        
        backgroundColor = .white
        addSubview(topWhiteView)
        addSubview(designAppLabel)
        addSubview(descriptionLabel)
        
        
        addConstraintWithFormat(format: "H:|-10-[v0]-10-|", views: topWhiteView)
        addConstraintWithFormat(format: "V:|-5-[v0(\(getSizeScreen().height / 3.5))]-10-|", views: topWhiteView)
        
        topWhiteView.addSubview(logoImageView)
        topWhiteView.addSubview(nameOpLabel)
        
        logoImageView.centerXAnchor.constraint(equalTo: topWhiteView.centerXAnchor).isActive = true
        logoImageView.topAnchor.constraint(equalTo: topWhiteView.topAnchor, constant: getSizeScreen().height / 20).isActive = true
        logoImageView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        
        nameOpLabel.rightAnchor.constraint(equalTo: topWhiteView.rightAnchor).isActive = true
        nameOpLabel.leftAnchor.constraint(equalTo: topWhiteView.leftAnchor).isActive = true
        nameOpLabel.bottomAnchor.constraint(equalTo: topWhiteView.bottomAnchor).isActive = true
        nameOpLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: getSizeScreen().height / 20).isActive = true
        nameOpLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 42)
        
        designAppLabel.topAnchor.constraint(equalTo: topWhiteView.bottomAnchor).isActive = true
        designAppLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        designAppLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 12).isActive = true
        designAppLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width).isActive = true
        designAppLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 48)
        
        let s1 = "شهرداری پردیس"
        let s3 = "تلفن: " + "۰۲۱۷۶۲۴۴۳۲۱"
        let s4 = "نمابر: " + "۰۲۱۷۶۲۴۴۳۲۱"
        let s5 = "سامانه پیامک: ۱۰۰۰۱۰۱۰۱۳۷"
        let s6 = "وب سایت: www.AndishehCity.ir"
        let s7 = "ایمیل: info@AndishehCity.ir"
        
        let desText = "\(s1)\n\n\(s3)\n\n\(s4)\n\n\(s5)\n\n\(s6)\n\n\(s7)"
        descriptionLabel.text = desText
        descriptionLabel.numberOfLines = 0
        descriptionLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        descriptionLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        descriptionLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 3).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: designAppLabel.bottomAnchor).isActive = true
        descriptionLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 55)
        
    }
    
}

//
//  HomeController.swift
//  hamshahriyar
//
//  Created by apple on 11/2/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Instructions
import Pulsator

class HomeController: UIViewController, CoachMarksControllerDataSource, CoachMarksControllerDelegate {
    
    
    
    let statusBarView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        return view
    }()

    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .right
        label.text = "سامانه مدیریت شهری شهروندیار"
        label.numberOfLines = 0
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    let logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "top_logo1"))
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .white
        imageView.backgroundColor = .clear
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let userDefult = UserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNotificationObservers()
        
//        if let usid = userDefult.value(forKey: "userid") as? String {
//            print("***************")
//            print(usid)
////            userID = usid
//        }
        
        createGradientLayer()
        setupStatusBar()
        setupViews()
        setupHelpApp()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pulseAnimations()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print(123)
        pulseAnimations()
    }
    
    private func setupNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handelEnterForground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func handelEnterForground() {
        pulseAnimations()
    }
    
    var gradientLayer: CAGradientLayer!
    
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.view.bounds
        
        let topColor = UIColor.baseColor2.base
        let bottomColor = UIColor.baseColor.base
        
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor, bottomColor.cgColor]
        
        self.view.layer.addSublayer(gradientLayer)
    }
    
    let coachMarksController = CoachMarksController()
    
    fileprivate func setupHelpApp() {
        
        coachMarksController.dataSource = self
        coachMarksController.overlay.color = UIColor(white: 0, alpha: 0.7)
        pointOfInterest1.frame = CGRect(x: 20, y: getSizeScreen().height / 11, width: getSizeScreen().width / 1.5, height: getSizeScreen().height / 20 + 10)
        pointOfInterest2.frame = CGRect(x: getSizeScreen().width / 4, y: getSizeScreen().height / 4.2, width: getSizeScreen().width / 2.2 + 10, height: getSizeScreen().width / 2.2 + 10)
        pointOfInterest3.frame = CGRect(x: getSizeScreen().height / 20 - 5, y: getSizeScreen().height / 2 + getSizeScreen().width / 5, width: getSizeScreen().width / 5 + 5, height: getSizeScreen().width / 5 + 5)
        pointOfInterest4.frame = CGRect(x: getSizeScreen().height / 20 - 5, y: getSizeScreen().height / 1.2, width: getSizeScreen().width / 5 + 5, height: getSizeScreen().width / 5 + 5)
        pointOfInterest5.frame = CGRect(x: getSizeScreen().width - 1.5 * getSizeScreen().width / 5 , y: getSizeScreen().height / 2 + getSizeScreen().width / 5, width: getSizeScreen().width / 5 + 5, height: getSizeScreen().width / 5 + 5)
        pointOfInterest6.frame = CGRect(x: getSizeScreen().width - 1.5 * getSizeScreen().width / 5, y: getSizeScreen().height / 1.2, width: getSizeScreen().width / 5 + 5, height: getSizeScreen().width / 5 + 5)
        
    }
    
    func numberOfCoachMarks(for coachMarksController: CoachMarksController) -> Int {
        return 6
    }
    let pointOfInterest1 = UIView()
    let pointOfInterest2 = UIView()
    let pointOfInterest3 = UIView()
    let pointOfInterest4 = UIView()
    let pointOfInterest5 = UIView()
    let pointOfInterest6 = UIView()
    
    func coachMarksController(_ coachMarksController: CoachMarksController,
                              coachMarkAt index: Int) -> CoachMark {
        
        let array = [pointOfInterest1, pointOfInterest2, pointOfInterest3 ,pointOfInterest4, pointOfInterest5, pointOfInterest6]
        
        return coachMarksController.helper.makeCoachMark(for: array[index])
    }
    
//    let textHelpArray = ["خوش آمدید\n\n این راهنما شما را با بخش های این نرم افزار آشنا می کند","اخبار\n\n امکان دسترسی به آخرین اخبار شهرداری و سامانه","ارتقاع به ناظر\n\n درخواست تبدیل شدن به ناظر و دریافت کد ناظر برای استفاده از آن در راه های ارتباطی دیگر","پیگیری درخواست ها\n\n همه ی درخواست های شما در این بخش قابل مشاهده و پیگیری هستند", "مشکلات شهری\n\nامکان ثیت مشکل بصورت آفلاین و آنلاین"]
    let textHelpArray = ["خوش آمدید \n\n این راهنما شما را با بخش های این نرم افزار آشنا می کند","مشکلات شهری \n\n امکان ثبت مشکل بصورت آفلاین و آفلاین","اخبار \n\n امکان دسترسی به آخرین اخبار شهرداری پردیس و سامانه","مسیریابی \n\n امکان مسیریابی از موقعیت فعلی تا موقعیت انتخابی","پیگیری درخواست \n\n همه درخواست های شما در این بخش قابل مشاهده و پیگیری هستند","مکان های گردشگری \n\n امکان مشاهده تمام مکان های گردشگری و امکان مسیریابی از موقعیت فعلی کاربر تا موقیت مکان گردشگری انتخابی"]
    
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkViewsAt index: Int, madeFrom coachMark: CoachMark) -> (bodyView: CoachMarkBodyView, arrowView: CoachMarkArrowView?) {
        let coachViews = coachMarksController.helper.makeDefaultCoachViews(withArrow: true, arrowOrientation: coachMark.arrowOrientation)
        
        
        coachViews.bodyView.hintLabel.textAlignment = .center
        coachViews.bodyView.hintLabel.text = textHelpArray[index]
        if index == 5 {
            coachViews.bodyView.nextLabel.text = "در آخر"
        }
        else {
            coachViews.bodyView.nextLabel.text = "ادامه"
        }
        
        
        return (bodyView: coachViews.bodyView, arrowView: coachViews.arrowView)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.coachMarksController.start(in: .window(over: self))
        if let firstStartAppMode = userDefult.value(forKey: "startAppMode") as? Bool {
            if firstStartAppMode {
                self.coachMarksController.start(in: .window(over: self))
                userDefult.setValue(false, forKey: "startAppMode")
            }
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.coachMarksController.stop(immediately: true)
    }
    
    
    func convertEngNumToPersianNum(num: String) -> String {
        //let number = NSNumber(value: Int(num)!)
        
        let format = NumberFormatter()
        format.locale = Locale(identifier: "fa_IR")
        let number =   format.number(from: num)
        let faNumber = format.string(from: number!)
        return faNumber!
        
    }
    
    

    
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    fileprivate func setupStatusBar() {
        view.addSubview(statusBarView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: statusBarView)
        view.addConstraintWithFormat(format: "V:|[v0(\(getSizeScreen().height / 25))]", views: statusBarView)
        UIApplication.shared.statusBarStyle = .lightContent
//        view.backgroundColor = UIColor.baseColor.base
    }
    
//    let CoreDataOperation = coreDataOperation()
    
    fileprivate func setupViews() {
        
        let userDefult = UserDefaults()
        if let typee = userDefult.value(forKey: "ProblemTypeArray") {
            let ttt = typee  as! [String]
            let numItemOfCoreData = ttt.count
            if numItemOfCoreData != 0 {
                budgeNumItemSaveToCoreData = numItemOfCoreData
                budgNumberItemSaveToCoreDataLabel.isHidden = false
                showNumberOfItemsSaveToCoreData()
            }
            else {
                budgNumberItemSaveToCoreDataLabel.isHidden = true
            }
        }
        
        

        setupBaseButton()
    }
    
    let editButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.layer.masksToBounds = false
        let image = UIImage(named: "gear")?.withRenderingMode(.alwaysOriginal)
        btn.setImage(image, for: .normal)
        btn.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        btn.layer.shadowOpacity = 1
        btn.layer.shadowOffset = CGSize.zero
        btn.imageView?.clipsToBounds = true
        btn.layer.shadowRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let editLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        //        label.textColor = .lightGray
        label.textAlignment = .center
        label.text = "تنظیمات"
        
        label.backgroundColor = .clear
        //        label.isHidden = true
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    let newsButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.layer.masksToBounds = false
        let image = UIImage(named: "news")?.withRenderingMode(.alwaysOriginal)
        btn.setImage(image, for: .normal)
        btn.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        btn.layer.shadowOpacity = 1
        btn.layer.shadowOffset = CGSize.zero
        btn.imageView?.clipsToBounds = true
        btn.layer.shadowRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let newsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.text = "اخبار"
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let supportButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.layer.masksToBounds = false
        let image = UIImage(named: "follow_up")?.withRenderingMode(.alwaysOriginal)
        btn.setImage(image, for: .normal)
        btn.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        btn.layer.shadowOpacity = 1
        btn.layer.shadowOffset = CGSize.zero
        btn.imageView?.clipsToBounds = true
        btn.layer.shadowRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let supportLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.text = "پیگیری درخواست"
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let aboutUsButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .clear
        btn.tintColor = .black
        btn.setTitle("درباره ما", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    //Observer
    
    let routingButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.layer.masksToBounds = false
        let image = UIImage(named: "routing")?.withRenderingMode(.alwaysOriginal)
        btn.setImage(image, for: .normal)
        btn.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        btn.layer.shadowOpacity = 1
        btn.imageView?.clipsToBounds = true
        btn.layer.shadowOffset = CGSize.zero
        btn.layer.shadowRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let routingLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.text = "مسیریابی"
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let mapTouristButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.layer.masksToBounds = false
        let image = UIImage(named: "mapTourist")?.withRenderingMode(.alwaysOriginal)
        btn.setImage(image, for: .normal)
        btn.imageView?.clipsToBounds = true
        btn.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        btn.layer.shadowOpacity = 1
        btn.layer.shadowOffset = CGSize.zero
        btn.layer.shadowRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let mapTouristLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.text = "مکان های گردشگری"
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let SupportButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .clear
        btn.tintColor = .black
        btn.setTitle("پشتیبانی", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let budgNumberItemSaveToCoreDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.text = "۱"
        label.backgroundColor = .red
        label.layer.masksToBounds = true
        label.isHidden = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let firstLine: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let secondLine: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let CityProblemView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.baseColor.base
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        return view
    }()
    
    let allButtonView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.baseColor.base
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let cityProblemImageView: UIImageView = {
        let iv = UIImageView()
        let image = UIImage(named: "problem")?.withRenderingMode(.alwaysOriginal)
        iv.image = image
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let problemCityLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.text = "مشکلات شهری"
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let menuButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.tintColor = .white
        btn.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "menu")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        return btn
    }()
    
    let menuView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private func pulseAnimations() {
        print(1234)
        let circularPath = UIBezierPath(arcCenter: .zero, radius: getSizeScreen().width / 4.7, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        pulsatingLayer.path = circularPath.cgPath
        pulsatingLayer.strokeColor = UIColor.clear.cgColor
        pulsatingLayer.lineWidth = 10
        pulsatingLayer.fillColor = UIColor(white: 1, alpha: 0.2).cgColor
        pulsatingLayer.lineCap = CAShapeLayerLineCap.round
        let yPuls = getSizeScreen().height / 10 + getSizeScreen().height / 18 + getSizeScreen().width / 2.46
        pulsatingLayer.position = CGPoint(x: getSizeScreen().width / 2, y: yPuls)
        
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.18
        animation.duration = 0.8
        animation.autoreverses = true
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.repeatCount = Float.infinity
        pulsatingLayer.add(animation, forKey: "pulsing")
    }
    
    var pulsatingLayer: CAShapeLayer!
    
    fileprivate func setupBaseButton() {

        view.addSubview(logoImageView)
        view.addSubview(titleLabel)
        view.addSubview(firstLine)
        view.addSubview(secondLine)
        

        pulsatingLayer = CAShapeLayer()
        view.layer.addSublayer(pulsatingLayer)
        view.addSubview(CityProblemView)
        view.addSubview(menuButton)
        view.addSubview(menuView)
        CityProblemView.addSubview(cityProblemImageView)
        CityProblemView.addSubview(problemCityLabel)
        view.addSubview(allButtonView)
        allButtonView.addSubview(editLabel)
        allButtonView.addSubview(editButton)
        allButtonView.addSubview(newsButton)
        allButtonView.addSubview(newsLabel)
        allButtonView.addSubview(supportButton)
        allButtonView.addSubview(supportLabel)
        allButtonView.addSubview(budgNumberItemSaveToCoreDataLabel)
        allButtonView.addSubview(routingButton)
        allButtonView.addSubview(routingLabel)
        allButtonView.addSubview(mapTouristButton)
        allButtonView.addSubview(mapTouristLabel)
        
        let guid = view.safeAreaLayoutGuide
        let viewGesture = UITapGestureRecognizer(target: self, action: #selector(dismissMenu))
        view.addGestureRecognizer(viewGesture)
        
        newsButton.addTarget(self, action: #selector(handelNews), for: .touchUpInside)
        routingButton.addTarget(self, action: #selector(handelRouting), for: .touchUpInside)
        supportButton.addTarget(self, action: #selector(handelFollowUpRequest), for: .touchUpInside)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handelProblemCity))
        CityProblemView.addGestureRecognizer(gesture)
        
        

        
        editButton.addTarget(self, action: #selector(handelSetting), for: .touchUpInside)
        menuButton.addTarget(self, action: #selector(handelMenu), for: .touchUpInside)
        SupportButton.addTarget(self, action: #selector(handelTicket), for: .touchUpInside)
        aboutUsButton.addTarget(self, action: #selector(handelAboutUs), for: .touchUpInside)
        mapTouristButton.addTarget(self, action: #selector(handelLocationTorism), for: .touchUpInside)
        
        logoImageView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -getSizeScreen().width / 15).isActive = true
        logoImageView.topAnchor.constraint(equalTo: statusBarView.bottomAnchor, constant: getSizeScreen().width / 15).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 10).isActive = true
        logoImageView.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 10).isActive = true
        
        titleLabel.rightAnchor.constraint(equalTo: logoImageView.leftAnchor, constant: -getSizeScreen().width / 15).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: logoImageView.centerYAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        titleLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        menuButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        menuButton.topAnchor.constraint(equalTo: statusBarView.bottomAnchor, constant: 10).isActive = true
        menuButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 26).isActive = true
        menuButton.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 26).isActive = true
        
        menuView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        menuView.topAnchor.constraint(equalTo: menuButton.bottomAnchor, constant: 10).isActive = true
        menuView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 8).isActive = true
        menuView.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 8).isActive = true
        
        menuView.addSubview(aboutUsButton)
        menuView.addSubview(SupportButton)
        
        SupportButton.topAnchor.constraint(equalTo: menuView.topAnchor).isActive = true
        SupportButton.rightAnchor.constraint(equalTo: menuView.rightAnchor).isActive = true
        SupportButton.leftAnchor.constraint(equalTo: menuView.leftAnchor).isActive = true
        SupportButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 16.2).isActive = true
        SupportButton.titleLabel?.font = UIFont.systemFont(ofSize: getSizeScreen().height / 55)
        
        aboutUsButton.bottomAnchor.constraint(equalTo: menuView.bottomAnchor).isActive = true
        aboutUsButton.rightAnchor.constraint(equalTo: menuView.rightAnchor).isActive = true
        aboutUsButton.leftAnchor.constraint(equalTo: menuView.leftAnchor).isActive = true
        aboutUsButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 16.2).isActive = true
        aboutUsButton.titleLabel?.font = UIFont.systemFont(ofSize: getSizeScreen().height / 55)
        
        firstLine.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -getSizeScreen().width / 15).isActive = true
        firstLine.leftAnchor.constraint(equalTo: view.leftAnchor, constant: getSizeScreen().width / 15).isActive = true
        firstLine.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: getSizeScreen().width / 30).isActive = true
        firstLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        CityProblemView.topAnchor.constraint(equalTo: firstLine.bottomAnchor, constant: getSizeScreen().height / 18).isActive = true
        CityProblemView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        CityProblemView.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 2.2).isActive = true
        CityProblemView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2.2).isActive = true
        CityProblemView.layer.cornerRadius = getSizeScreen().width / 4.4
        
        


        
        pulseAnimations()

        cityProblemImageView.topAnchor.constraint(equalTo: CityProblemView.topAnchor, constant: getSizeScreen().width / 12).isActive = true
        cityProblemImageView.centerXAnchor.constraint(equalTo: CityProblemView.centerXAnchor).isActive = true
        cityProblemImageView.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 7).isActive = true
        cityProblemImageView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 7).isActive = true
        
        problemCityLabel.topAnchor.constraint(equalTo: cityProblemImageView.bottomAnchor, constant: 5).isActive = true
        problemCityLabel.centerXAnchor.constraint(equalTo: CityProblemView.centerXAnchor).isActive = true
        problemCityLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 7).isActive = true
        problemCityLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
        

        
        secondLine.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        secondLine.topAnchor.constraint(equalTo: CityProblemView.bottomAnchor, constant: getSizeScreen().height / 15).isActive = true
        secondLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        secondLine.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 1.5).isActive = true
        
        allButtonView.topAnchor.constraint(equalTo: secondLine.bottomAnchor).isActive = true
        allButtonView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        allButtonView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        allButtonView.bottomAnchor.constraint(equalTo: guid.bottomAnchor).isActive = true

        editButton.centerYAnchor.constraint(equalTo: allButtonView.centerYAnchor).isActive = true
        editButton.centerXAnchor.constraint(equalTo: allButtonView.centerXAnchor).isActive = true
        editButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
        editButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
        editButton.layer.cornerRadius = getSizeScreen().width / 10
        
        editLabel.topAnchor.constraint(equalTo: editButton.bottomAnchor, constant: getSizeScreen().height / 50).isActive = true
        editLabel.centerXAnchor.constraint(equalTo: allButtonView.centerXAnchor).isActive = true
        editLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        editLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 50)
        
        newsButton.topAnchor.constraint(equalTo: allButtonView.topAnchor, constant: getSizeScreen().height / 20).isActive = true
        newsButton.leftAnchor.constraint(equalTo: allButtonView.leftAnchor, constant: getSizeScreen().height / 20).isActive = true
        newsButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
        newsButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
        newsButton.layer.cornerRadius = getSizeScreen().width / 10
        
        newsLabel.topAnchor.constraint(equalTo: newsButton.bottomAnchor, constant: getSizeScreen().height / 50).isActive = true
        newsLabel.centerXAnchor.constraint(equalTo: newsButton.centerXAnchor).isActive = true
        newsLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        newsLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 50)
        
        supportButton.topAnchor.constraint(equalTo: allButtonView.topAnchor, constant: getSizeScreen().height / 20).isActive = true
        supportButton.rightAnchor.constraint(equalTo: allButtonView.rightAnchor, constant: -getSizeScreen().height / 20).isActive = true
        supportButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
        supportButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
        supportButton.layer.cornerRadius = getSizeScreen().width / 10
        
        
        supportLabel.topAnchor.constraint(equalTo: supportButton.bottomAnchor, constant: getSizeScreen().height / 50).isActive = true
        supportLabel.centerXAnchor.constraint(equalTo: supportButton.centerXAnchor).isActive = true
        supportLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
        supportLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 53)
        
        budgNumberItemSaveToCoreDataLabel.centerXAnchor.constraint(equalTo: supportButton.centerXAnchor, constant: getSizeScreen().width / 9).isActive = true
        budgNumberItemSaveToCoreDataLabel.centerYAnchor.constraint(equalTo: supportButton.centerYAnchor, constant: -getSizeScreen().width / 10).isActive = true
        budgNumberItemSaveToCoreDataLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 10).isActive = true
        budgNumberItemSaveToCoreDataLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 10).isActive = true
        budgNumberItemSaveToCoreDataLabel.layer.cornerRadius = getSizeScreen().width / 20
        budgNumberItemSaveToCoreDataLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        mapTouristButton.bottomAnchor.constraint(equalTo: allButtonView.bottomAnchor, constant: -getSizeScreen().height / 19).isActive = true
        mapTouristButton.rightAnchor.constraint(equalTo: allButtonView.rightAnchor, constant: -getSizeScreen().height / 20).isActive = true
        mapTouristButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
        mapTouristButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
        mapTouristButton.layer.cornerRadius = getSizeScreen().width / 10
        
        mapTouristLabel.topAnchor.constraint(equalTo: mapTouristButton.bottomAnchor, constant: getSizeScreen().height / 50).isActive = true
        mapTouristLabel.centerXAnchor.constraint(equalTo: mapTouristButton.centerXAnchor).isActive = true
        mapTouristLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
        mapTouristLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 53)
        
        routingButton.bottomAnchor.constraint(equalTo: allButtonView.bottomAnchor, constant: -getSizeScreen().height / 19).isActive = true
        routingButton.leftAnchor.constraint(equalTo: allButtonView.leftAnchor, constant: getSizeScreen().height / 20).isActive = true
        routingButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
        routingButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
        routingButton.layer.cornerRadius = getSizeScreen().width / 10
        
        routingLabel.topAnchor.constraint(equalTo: routingButton.bottomAnchor, constant: getSizeScreen().height / 50).isActive = true
        routingLabel.centerXAnchor.constraint(equalTo: routingButton.centerXAnchor).isActive = true
        routingLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
        routingLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 53)
        
    }
    var selectMenu = false
    @objc fileprivate func handelMenu() {
        if selectMenu {
            menuView.isHidden = true
            selectMenu = false
        }
        else {
            menuView.isHidden = false
            selectMenu = true
        }
        
    }
    
    @objc fileprivate func dismissMenu() {
        menuView.isHidden = true
        selectMenu = false
    }
    
    var budgeNumItemSaveToCoreData = 0
    
    func showNumberOfItemsSaveToCoreData() {
        
        if budgeNumItemSaveToCoreData != 0 {
            budgNumberItemSaveToCoreDataLabel.isHidden = false
        }
        else {
            budgNumberItemSaveToCoreDataLabel.isHidden = true
        }
        
        let persianNum = convertEngNumToPersianNum(num: String(budgeNumItemSaveToCoreData))
        budgNumberItemSaveToCoreDataLabel.text = persianNum
        
    }
    
    @objc fileprivate func handelProblemCity() {
        
        //        let problemCnt = cityProblemsController()
        userDefult.setValue(false, forKey: "offlineMode")
        let CPController = cityProblemsController()
        CPController.home = self
        let root = UINavigationController(rootViewController: CPController)
        self.present(root, animated: true, completion: nil)
        
        
    }
    
    @objc fileprivate func handelNews() {
        let NController = NewsController()
        let root = UINavigationController(rootViewController: NController)
        self.present(root, animated: true, completion: nil)
    }
    
    @objc fileprivate func handelFollowUpRequest() {
//        let root = UINavigationController(rootViewController: FURController)
        let FURController = FollowUpTheRequestController()
        FURController.home = self
        present(FURController, animated: true, completion: nil)
    }
    
    @objc fileprivate func handelRouting() {
        
        let upgradtoobsorve = RouterController()
        let root = UINavigationController(rootViewController: upgradtoobsorve)
        self.present(root, animated: true, completion: nil)
        
    }
    
    
    @objc fileprivate func handelTicket() {
        
        menuView.isHidden = true
        selectMenu = false
        let ticketController = TicketController()
        let root = UINavigationController(rootViewController: ticketController)
        self.present(root, animated: true, completion: nil)
        
    }
    
    @objc fileprivate func handelAboutUs() {
        menuView.isHidden = true
        selectMenu = false
        let aboutUs = aboutUsController()
        let root = UINavigationController(rootViewController: aboutUs)
        self.present(root, animated: true, completion: nil)
        
    }
    
    @objc fileprivate func handelSetting() {
        
        let setting = settingController()
        let root = UINavigationController(rootViewController: setting)
        self.present(root, animated: true, completion: nil)
        
    }
    
    @objc fileprivate func handelLocationTorism() {
        let loc = tourismLocationsController()
        let root = UINavigationController(rootViewController: loc)
        self.present(root, animated: true, completion: nil)
    }
    
    
    
}

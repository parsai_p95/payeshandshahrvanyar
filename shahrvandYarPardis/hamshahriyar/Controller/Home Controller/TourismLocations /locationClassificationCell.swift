//
//  locationClassificationCell.swift
//  hamshahriyar
//
//  Created by apple on 11/13/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit

class locationClassificationCell: baseCell {
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.image = UIImage(named: "city")?.withRenderingMode(.alwaysOriginal)
        iv.backgroundColor = UIColor(r: 201, g: 207, b: 205)
        iv.layer.masksToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    let CatLabel: UILabel = {
        let label = UILabel()
        label.text = "دسته بندی مکان ها"
        label.textColor = .black
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func setupViews() {
        
        backgroundColor = .white
        addSubview(imageView)
        addSubview(CatLabel)
        
        imageView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5).isActive = true
        imageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 11).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 11).isActive = true
        imageView.layer.cornerRadius = getSizeScreen().height / 22
        
        CatLabel.rightAnchor.constraint(equalTo: imageView.leftAnchor, constant: -5).isActive = true
        CatLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 5).isActive = true
        CatLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 5).isActive = true
        CatLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true
    }
    
}

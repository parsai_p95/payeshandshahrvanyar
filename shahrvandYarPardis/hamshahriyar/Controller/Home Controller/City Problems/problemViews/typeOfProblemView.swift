//
//  typeOfProblemView.swift
//  hamshahriyar
//
//  Created by apple on 11/2/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit
import CDAlertView
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

struct ExpandebleName {
    
    var isExpandble: Bool
    var names: [String]
}

class typeOfProblemView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if tableView == self.tblView {
            return problemTitles.count
        }
        return selectItems.count
        
    }
    
    var selectItems: [String] = []
    var selectItemsCode: [String] = []
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblView {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! typeOfbroblem2TableViewCell
            cell.Label.text = problemTitles[indexPath.row]
            cell.dropFullButton.addTarget(self, action: #selector(showSelectProblem), for: .touchUpInside)
            cell.dropFullButton.tag = indexPath.item
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: selectCellId, for: indexPath) as! typeOfbroblem2SelectTableViewCell
            cell.Label.text = selectItems[indexPath.item]
            cell.Switch.addTarget(self, action: #selector(typeOfProblemSwitchesDidChange(_:)), for: .valueChanged)
            print()
            cell.Switch.layer.setValue(Int(selectItemsCode[indexPath.item]), forKey: "typeOfProblem")
            return cell
        }
        
    }
    
    
    
    let blackProblemView = UIView()
    let whiteProblemView = UIView()
    let cancelSelectBlackViewButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("لغو", for: .normal)
        btn.tintColor = .red
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc fileprivate func showSelectProblem(sender: UIButton) {
        
        selectItems.removeAll()
        selectItemsCode.removeAll()
        selectItems = problemTitlesArray[sender.tag]
        selectItemsCode = problemCodeArray[sender.tag]
        if let windows = UIApplication.shared.keyWindow {
            windows.addSubview(blackProblemView)
            blackProblemView.backgroundColor = UIColor(white: 0, alpha: 0.75)
            blackProblemView.frame = windows.frame
            blackProblemView.addSubview(whiteProblemView)
            whiteProblemView.frame.size.height = windows.frame.height / 2
            whiteProblemView.frame.size.width = windows.frame.width - 20
            whiteProblemView.center = blackProblemView.center
            whiteProblemView.backgroundColor = .white
            whiteProblemView.layer.cornerRadius = 5
            
            setupSelectWhiteView()
            
            blackProblemView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackProblemView.alpha = 1
                
            }, completion: nil)
        }
        
    }
    
    fileprivate func setupSelectWhiteView() {
        
        whiteProblemView.addSubview(SelectTblView)
        whiteProblemView.addConstraintWithFormat(format: "H:|-10-[v0]-10-|", views: SelectTblView)
        whiteProblemView.addConstraintWithFormat(format: "V:|-10-[v0]-45-|", views: SelectTblView)
        
        whiteProblemView.addSubview(cancelSelectBlackViewButton)
        cancelSelectBlackViewButton.centerXAnchor.constraint(equalTo: whiteProblemView.centerXAnchor).isActive = true
        cancelSelectBlackViewButton.bottomAnchor.constraint(equalTo: whiteProblemView.bottomAnchor, constant: -5).isActive = true
        cancelSelectBlackViewButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        cancelSelectBlackViewButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        cancelSelectBlackViewButton.addTarget(self, action: #selector(handelCancelSelectBlackViewButton), for: .touchUpInside)
        SelectTblView.reloadData()
    }
    
    @objc fileprivate func handelCancelSelectBlackViewButton() {
//        let indexPath = IndexPath(item: 0, section: 0)
//        self.SelectTblView.scrollToRow(at: indexPath, at: .top, animated: true)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackProblemView.alpha = 0
            
        }, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return getSizeScreen().height / 10
    }
    
    lazy var tblView: UITableView = {
        let tv = UITableView(frame: .zero, style: UITableView.Style.plain)
        tv.backgroundColor = .white
        tv.dataSource = self
        tv.delegate = self
        tv.separatorStyle = .none
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    lazy var SelectTblView: UITableView = {
        let tv = UITableView(frame: .zero, style: UITableView.Style.plain)
        tv.backgroundColor = .white
        tv.dataSource = self
        tv.delegate = self
        tv.separatorStyle = .none
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    
    
    var CityProblem: cityProblemsController?
    var unsendReqView: unsendSelectCellWhiteView?
    
    var typeOfProblem: String?
    
    
    
    let loader: NVActivityIndicatorView = {
        let loader = NVActivityIndicatorView(frame: .zero)
        let loaderType = NVActivityIndicatorType.ballRotateChase
        loader.type = loaderType
        loader.color = .gray
        loader.padding = 5
        loader.translatesAutoresizingMaskIntoConstraints = false
        return loader
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupTableView()
        addSubview(loader)
        loader.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loader.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        loader.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        loader.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        
        SuggestionSwitch.isOn = false
        discontentSwitch.isOn = false
        criticismSwitch.isOn = false
        appreciationSwitch.isOn = false
        
        setupViews()
    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    let cellId = "cellId"
    let selectCellId = "selectCellId"
    
    fileprivate func setupTableView() {
        tblView.register(typeOfbroblem2TableViewCell.self, forCellReuseIdentifier: cellId)
        SelectTblView.register(typeOfbroblem2SelectTableViewCell.self, forCellReuseIdentifier: selectCellId)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.text = "تعیین نوع مشکل"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let line: UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let discontentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        return view
    }()
    
    let discontentLabel: UILabel = {
        let label = UILabel()
        label.text = "شکایت"
        label.textColor = .black
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let discontentSwitch: UISwitch = {
        
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.addTarget(self, action: #selector(typeOfProblemSwitchesDidChange(_:)), for: .valueChanged)
        
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
    let SuggestionView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let SuggestionLabel: UILabel = {
        let label = UILabel()
        label.text = "پیشنهاد"
        label.textColor = .black
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let SuggestionSwitch: UISwitch = {
        
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.addTarget(self, action: #selector(typeOfProblemSwitchesDidChange(_:)), for: .valueChanged)
        Switch.layer.setValue(2, forKey: "typeOfProblem")
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
    let criticismView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let criticismLabel: UILabel = {
        let label = UILabel()
        label.text = "انتقاد"
        label.textColor = .black
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let criticismSwitch: UISwitch = {
        
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.addTarget(self, action: #selector(typeOfProblemSwitchesDidChange(_:)), for: .valueChanged)
        Switch.layer.setValue(3, forKey: "typeOfProblem")
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
    let appreciationView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let appreciationLabel: UILabel = {
        let label = UILabel()
        label.text = "تقدیر و تشکر"
        label.textColor = .black
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let appreciationSwitch: UISwitch = {
        
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.addTarget(self, action: #selector(typeOfProblemSwitchesDidChange(_:)), for: .valueChanged)
        Switch.layer.setValue(4, forKey: "typeOfProblem")
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
    let nextLevelButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor(r: 10, g: 96, b: 238)
        btn.tintColor = .white
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 5
        btn.setTitle("مرحله بعد", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    fileprivate func setupViews() {
        addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 12).isActive = true
        titleLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 40)
        
        addSubview(line)
        line.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
        line.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        line.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        line.heightAnchor.constraint(equalToConstant: 2.5).isActive = true
        
        getTypeOfProblem()
        //*********************
        addSubview(nextLevelButton)
        nextLevelButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true
        nextLevelButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        nextLevelButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        nextLevelButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2).isActive = true
        nextLevelButton.addTarget(self, action: #selector(handelNextLevel), for: .touchUpInside)
        
        addSubview(tblView)
        tblView.topAnchor.constraint(equalTo: line.bottomAnchor, constant: getSizeScreen().height / 25).isActive = true
        tblView.bottomAnchor.constraint(equalTo: nextLevelButton.topAnchor, constant: -getSizeScreen().height / 25).isActive = true
        tblView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5).isActive = true
        tblView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 5).isActive = true
    }
    
    var problemTitles: [String] = []
    var problemCode: [String] = []
    var problemTitlesArray: [[String]] = []
    var problemCodeArray: [[String]] = []
    var isEmpty = false
    func getTypeOfProblem() {
        loader.startAnimating()
        let userInfoURL = baseURLString + "ptype.php"
        let getUrl = userInfoURL.data(using: String.Encoding.ascii, allowLossyConversion: true)
        let urlString = String(data: getUrl!, encoding: String.Encoding.ascii)
        guard let url = URL(string: urlString!) else{return}
        Alamofire.request(url, method: .get).responseJSON { (response) in
            
            if let data = response.result.value {
                let json = JSON(data).arrayValue
                
                for items in json {
                    if let cat = items["category"]["title"].string {
                        self.problemTitles.append(cat)
                    }
                    if let code = items["category"]["title"].string {
                        self.problemCode.append(code)
                    }
                    let subcategory = items["category"]["subcategory"].arrayValue
                    if subcategory.count == 0 {
                        print(123)
                    }
                    var tmpCodeArray: [String] = []
                    var tmpCatArray: [String] = []
                    for subItems in subcategory {
                        if let subCat = subItems["subject"].string, let subCode = subItems["problemcode"].string {
                            tmpCatArray.append(subCat)
                            tmpCodeArray.append(subCode)
                        }
                    }
                    self.problemTitlesArray.append(tmpCatArray)
                    self.problemCodeArray.append(tmpCodeArray)
                }
                
                self.loader.stopAnimating()
                DispatchQueue.main.async {
                    print(self.problemTitles.count)
                    self.tblView.reloadData()
                }
            }
        }
    }
    
    
    
    
    let ContainerVIew: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        
        return view
    }()
    let Label: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .right
        //        label.font = UIFont.systemFont(ofSize: getSizeScreen().height / 60)
        return label
    }()
    
    let dropButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.tintColor = .black
        btn.setImage(UIImage(named: "drop")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //
    //        let view = UIView()
    //        view.backgroundColor = .white
    //        view.addSubview(ContainerVIew)
    //        view.addConstraintWithFormat(format: "H:|-10-[v0]-10-|", views: ContainerVIew)
    //        view.addConstraintWithFormat(format: "V:|-10-[v0]-10-|", views: ContainerVIew)
    //        ContainerVIew.addSubview(Label)
    //        ContainerVIew.addSubview(dropButton)
    //
    //        dropButton.centerYAnchor.constraint(equalTo: ContainerVIew.centerYAnchor).isActive = true
    //        dropButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 12).isActive = true
    //        dropButton.leftAnchor.constraint(equalTo: ContainerVIew.leftAnchor, constant: getSizeScreen().width / 15).isActive = true
    //        dropButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 10).isActive = true
    //        dropButton.addTarget(self, action: #selector(handelExpandClose), for: .touchUpInside)
    //        dropButton.tag = section
    //
    //        Label.centerYAnchor.constraint(equalTo: ContainerVIew.centerYAnchor).isActive = true
    //        Label.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 12).isActive = true
    //        Label.rightAnchor.constraint(equalTo: ContainerVIew.rightAnchor, constant: -getSizeScreen().width / 15).isActive = true
    //        Label.leftAnchor.constraint(equalTo: dropButton.rightAnchor).isActive = true
    //        Label.font = UIFont.systemFont(ofSize: getSizeScreen().height / 50)
    //        Label.text = problemTitles[section]
    //        return view
    //    }
    
    
    
    var editReqViewMode = false
    
    @objc fileprivate func handelNextLevel() {
        
        //        if appreciationSwitch.isOn || criticismSwitch.isOn || SuggestionSwitch.isOn || discontentSwitch.isOn {
        if selectCatMode {
            print(123)
            if editReqViewMode {
                print(1234)
                editReqViewMode = false
                unsendReqView?.showNextLevelOfTypeOfProblemView()
            }
            CityProblem?.showNextLevelOfTypeOfProblemView()
        }
        else {
            
            let alert = CDAlertView(title: "", message: "موضوع مشکل را تعیین نکرده اید", type: .warning)
            let action = CDAlertViewAction(title: "تأیید")
            alert.add(action: action)
            alert.show()
        }
        
        
    }
    
    var selectCatMode = false
    
    @objc func typeOfProblemSwitchesDidChange(_ sender: UISwitch!) {
        
        let Index = (sender.layer.value(forKey: "typeOfProblem")) as! Int
        
        if sender.isOn {
            CityProblem?.problemType = "\(Index)"
            unsendReqView?.problemType = "\(Index)"
            selectCatMode = true
            handelCancelSelectBlackViewButton()
        }
        
    }
    
}

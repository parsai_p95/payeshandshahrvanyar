//
//  cityProblemsController.swift
//  hamshahriyar
//
//  Created by apple on 11/2/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
import SwiftyJSON
import CDAlertView
import Mapbox


class cityProblemsController: UIViewController, CLLocationManagerDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MKMapViewDelegate, MGLMapViewDelegate {
    
    let pinImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "pin")?.withRenderingMode(.alwaysOriginal)
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    
    var locationManager = CLLocationManager()
    let annotation =  MGLPointAnnotation()
    
    var currentLocationMode = false
    
    let mapView: MKMapView = {
        let map = MKMapView()
        map.showsUserLocation = true
        return map
    }()
    
    var home: HomeController?
    
    let downloadMapButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.layer.masksToBounds = false
        btn.tintColor = .black
        btn.setTitle("دانلود نقشه", for: .normal)
        btn.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        btn.layer.shadowOpacity = 1
        btn.layer.shadowOffset = CGSize.zero
        btn.layer.shadowRadius = 5
        btn.isHidden = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let choosePlaceButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.layer.masksToBounds = false
        btn.tintColor = .black
        btn.setTitle("انتخاب مکان", for: .normal)
        btn.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        btn.layer.shadowOpacity = 1
        btn.layer.shadowOffset = CGSize.zero
        btn.layer.shadowRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let FollowUpUnsendSelectCellWhiteView = unsendSelectCellWhiteView()
    let userDefult = UserDefaults()
    var help = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let offline = userDefult.value(forKey: "offlineMode") as? Bool {
            offlineMode = offline
        }

        if offlineMode {
            downloadMapButton.isHidden = false
            choosePlaceButton.isHidden = true
            
        }

        
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        
        view.backgroundColor = .white
        setupNavigationBar()
        
        setupMap()
        setupButtomView()
        
//        if let usid = userDefult.value(forKey: "userid") as? String {
//            if usid == "" {
//                if !offlineMode {
//                    self.dismiss(animated: true, completion: nil)
//                    userDefult.setValue(true, forKey: "UnacceptUserId")
//                    let profile = profileController()
//                    let root = UINavigationController(rootViewController: profile)
//                    self.present(root, animated: true, completion: nil)
//                }
//            }
//            else {
//                userID = usid
//            }
//        }
//        else {
//            self.dismiss(animated: true, completion: nil)
//            userDefult.setValue(true, forKey: "UnacceptUserId")
//            let profile = profileController()
//            let root = UINavigationController(rootViewController: profile)
//            self.present(root, animated: true, completion: nil)
//        }
//        else {
//            self.dismiss(animated: true, completion: nil)
//            userDefult.setValue(true, forKey: "UnacceptUserId")
//            let profile = profileController()
//            let root = UINavigationController(rootViewController: profile)
//            self.present(root, animated: true, completion: nil)
//            
//        }
//        if userID == "" {
//            let profile = profileController()
//            let root = UINavigationController(rootViewController: profile)
//            self.present(root, animated: true, completion: nil)
//        }
        
//        if let helpMode = userDefult.value(forKey: "helpMode") as? Bool {
//            help = helpMode
//        }
        //        if !help {
        //
        //            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
        //                let helpAlert = CDAlertView(title: "", message: "برای انتخاب موقعیت مشکل می بایست کمی دست خود را در موقعیت موردنظر نگه دارید", type: .alarm)
        //                let action = CDAlertViewAction(title: "فهمیدم", font:UIFont.systemFont(ofSize: 16), textColor: .white, backgroundColor: UIColor.greenColor.green) { (act) -> Bool in
        //                    self.userDefult.setValue(true, forKey: "helpMode")
        //                    return true
        //                }
        //                helpAlert.add(action: action)
        //                helpAlert.show()
        //            }
        //
        //        }
        
    }
    
    fileprivate func setupViews() {
        view.addSubview(pinImageView)
        pinImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -getSizeScreen().height / 18).isActive = true
        pinImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pinImageView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 18).isActive = true
        pinImageView.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 18).isActive = true
    }
    
    
    var uploadMode = false
    
    func updateUserInterface() {
        guard let status = Network.reachability?.status else { return }
        print("Reachability Summary")
        print("Status:", status)
        print("HostName:", Network.reachability?.hostname ?? "nil")
        print("Reachable:", Network.reachability?.isReachable ?? "nil")
        print("Wifi:", Network.reachability?.isReachableViaWiFi ?? "nil")
        
        if !(Network.reachability?.isReachable)! && !Reachabilities.isConnectedToNetwork() {
            
            if uploadMode {
                stopSending()
                showAlertFaile()
                uploadMode = false
            }
            else {
                showInernetAlert()
            }
            
        }
    }
    
    func showInernetAlert() {
        
        if uploadMode {
            stopSending()
            showAlertFaile()
        }
        else {
            
            let alert = CDAlertView(title: "", message: "دسترسی به اینترنت مقدور نیست", type: CDAlertViewType.warning)
            let action = CDAlertViewAction(title: "تأیید", font:UIFont.systemFont(ofSize: 14), textColor: .black, backgroundColor: .white) { (act) -> Bool in
                
                if !Reachabilities.isConnectedToNetwork() {
                    let alert = CDAlertView(title: "", message: "دسترسی به اینترنت مقدور نیست", type: CDAlertViewType.warning)
                    let action = CDAlertViewAction(title: "تأیید", font:UIFont.systemFont(ofSize: 14), textColor: .black, backgroundColor: .white) { (act) -> Bool in
                        
                        return true
                    }
                    alert.add(action: action)
                    alert.show()
                }
                
                return true
            }
            alert.add(action: action)
            alert.show()
            
        }
    }
    
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    fileprivate func setupButtomView() {
        
        if !offlineMode {
            setupViews()
        }
        
        view.addSubview(downloadMapButton)
        view.addConstraintWithFormat(format: "H:[v0(\(getSizeScreen().width / 3))]-\(getSizeScreen().height / 30)-|", views: downloadMapButton)
        view.addConstraintWithFormat(format: "V:[v0(\(getSizeScreen().height / 16))]-\(getSizeScreen().height / 20)-|", views: downloadMapButton)
        downloadMapButton.layer.cornerRadius = getSizeScreen().height / 32
        downloadMapButton.addTarget(self, action: #selector(startOfflinePackDownload), for: .touchUpInside)
        
        view.addSubview(choosePlaceButton)
        view.addConstraintWithFormat(format: "H:|-\(getSizeScreen().height / 30)-[v0(\(getSizeScreen().width / 3))]", views: choosePlaceButton)
        view.addConstraintWithFormat(format: "V:[v0(\(getSizeScreen().height / 16))]-\(getSizeScreen().height / 20)-|", views: choosePlaceButton)
        choosePlaceButton.layer.cornerRadius = getSizeScreen().height / 32
        choosePlaceButton.addTarget(self, action: #selector(handelChooseLocAndShowProblemView), for: .touchUpInside)
        choosePlaceButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 60)
        
    }
    
    fileprivate func ZoomLevel() -> Double {
        
        let zoom = log2(360 * Double(mapView.frame.size.width) / (mapView.region.span.longitudeDelta * 128))
        
        return roundDoubleNumber(number: zoom)
    }
    
    func roundDoubleNumber(number: Double) -> Double {
        
        return Double(round(10*number)/10)
    }
    
    
    @objc fileprivate func handelChooseLocAndShowProblemView() {
        
        handleLongPress()
        if annotation.coordinate.latitude == 0 || annotation.coordinate.longitude == 0 {
            let alert = CDAlertView(title: "", message: "موقعیت مشکل را انتخاب نکرده اید، آیا مایل به انتخاب موقعیت فعلی خود هستید؟", type: .warning)
            
            let action2 = CDAlertViewAction(title: "تأیید")
            alert.add(action: action2)
            alert.show()
        }
        else {
            
            TypeOfProblemView.isHidden = false
            SendRequsetView.isHidden = true
            showProblemView()
            //            let zoom = ZoomLevel()
            //            if zoom < 17.50 {
            //
            //                let alert = CDAlertView(title: "", message: "بیشتر زوم کنید", type: .warning)
            //                let action2 = CDAlertViewAction(title: "فهمیدم")
            //                alert.add(action: action2)
            //                alert.show()
            //
            //            }
            //            else {
            //
            //            }
            
        }
        
        
    }
    
    let blackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)
        return view
    }()
    
    let whiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        //        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var blackViewSelectedMode = true
    
    fileprivate func showProblemView() {
        
        if let windows = UIApplication.shared.keyWindow {
            windows.addSubview(blackView)
            blackView.frame = windows.frame
            
            let gesture = UITapGestureRecognizer(target: self, action: #selector(handelDismissBlackView))
            let gesture2 = UITapGestureRecognizer(target: self, action: #selector(handelUnSelectWhiteVIew))
            blackView.addGestureRecognizer(gesture)
            whiteView.addGestureRecognizer(gesture2)
            blackView.addSubview(whiteView)
            setupWhiteView()
            blackView.alpha = 0
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 1
                
            }, completion: nil)
            
        }
    }
    
    @objc fileprivate func handelUnSelectWhiteVIew() {
        dismissKeyboard()
        blackViewSelectedMode = false
    }
    
    lazy var TypeOfProblemView: typeOfProblemView = {
        let view = typeOfProblemView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.CityProblem = self
        return view
    }()
    
    lazy var SendRequsetView: sendRequestView = {
        let view = sendRequestView()
        view.CityProblem = self
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.delegate = self
        return scrollView
    }()
    
    lazy var scrollView2: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.delegate = self
        return scrollView
    }()
    
    func showNextLevelOfTypeOfProblemView() {
        scrollProblemView.isHidden = true
        TypeOfProblemView.isHidden = true
        SendRequsetView.isHidden = false
        scrollView.isHidden = false
    }
    
    func returnSendRequestView() {
        scrollProblemView.isHidden = false
        TypeOfProblemView.isHidden = false
        scrollView.isHidden = true
        SendRequsetView.isHidden = true
    }
    
    var doneButtonBottonConstant: CGFloat?
    
    func setupWhiteViewFrame(y: CGFloat) {
        if let windows = UIApplication.shared.keyWindow {
            whiteView.frame = CGRect(x: 10, y: y, width: windows.frame.width - 20, height: windows.frame.height / 1.7)
        }
    }
    
    lazy var scrollProblemView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.delegate = self
        scrollView.isHidden = false
        return scrollView
    }()
    
    var numberOfProblem1 = 0
    fileprivate func getTypeOfProblem() {
        let userInfoURL = baseURLString + "ptype.php"
        let getUrl = userInfoURL.data(using: String.Encoding.ascii, allowLossyConversion: true)
        let urlString = String(data: getUrl!, encoding: String.Encoding.ascii)
        guard let url = URL(string: urlString!) else{return}
        Alamofire.request(url, method: .get).responseJSON { (response) in
            
            if let data = response.result.value {
                self.numberOfProblem1 = JSON(data).arrayValue.count
                print(self.numberOfProblem1)
                self.scrollProblemView.addSubview(self.TypeOfProblemView)
                self.TypeOfProblemView.topAnchor.constraint(equalTo: self.scrollProblemView.topAnchor).isActive = true
                self.TypeOfProblemView.bottomAnchor.constraint(equalTo: self.scrollProblemView.bottomAnchor).isActive = true
                self.TypeOfProblemView.leftAnchor.constraint(equalTo: self.scrollProblemView.leftAnchor).isActive = true
                self.TypeOfProblemView.rightAnchor.constraint(equalTo: self.scrollProblemView.rightAnchor).isActive = true
                self.TypeOfProblemView.heightAnchor.constraint(equalToConstant: ((self.getSizeScreen().height / 2)) + 10).isActive = true
                self.TypeOfProblemView.addConstraint(NSLayoutConstraint(item: self.TypeOfProblemView, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 0, constant: ((self.view?.frame.width)!)-20))
            }
            if let err = response.result.error {
                let error = "\(err.localizedDescription)"
                if error == "The Internet connection appears to be offline." {
                    let alert = CDAlertView(title: "", message: "اینترت گوشی خود را چک کنید", type: .warning)
                    let action = CDAlertViewAction(title: "تلاش مجدد", font: UIFont.systemFont(ofSize: 12), textColor: UIColor.purpuleColor.purple, backgroundColor: .white, handler: { (action) -> Bool in
                        self.getTypeOfProblem()
                        return true
                    })
                    let action2 = CDAlertViewAction(title: "بستن")
                    alert.add(action: action)
                    alert.add(action: action2)
                    alert.show()
                }
            }
        }
    }
    
    
    fileprivate func setupWhiteView() {
        if let windows = UIApplication.shared.keyWindow {
            setupWhiteViewFrame(y: windows.frame.height / 3)
            whiteView.layer.cornerRadius = 10
            whiteView.layer.masksToBounds = true
            //            whiteView.center = blackView.center
//            whiteView.addSubview(TypeOfProblemView)
//            whiteView.addConstraintWithFormat(format: "H:|[v0]|", views: TypeOfProblemView)
//            whiteView.addConstraintWithFormat(format: "V:|[v0]|", views: TypeOfProblemView)
            
            whiteView.addSubview(scrollProblemView)
            whiteView.addConstraintWithFormat(format: "H:|[v0]|", views: scrollProblemView)
            whiteView.addConstraintWithFormat(format: "V:|[v0]|", views: scrollProblemView)
            
            whiteView.addSubview(scrollView)
            whiteView.addConstraintWithFormat(format: "H:|[v0]|", views: scrollView)
            whiteView.addConstraintWithFormat(format: "V:|[v0]|", views: scrollView)
            
            let gesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
            scrollView.addGestureRecognizer(gesture)
            
//            scrollView.addSubview(TypeOfProblemView)
//            TypeOfProblemView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
//            TypeOfProblemView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
//            TypeOfProblemView.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
//            TypeOfProblemView.rightAnchor.constraint(equalTo: scrollView.rightAnchor).isActive = true
//            TypeOfProblemView.heightAnchor.constraint(equalToConstant: getSizeScreen().height/1.2).isActive = true
//            TypeOfProblemView.addConstraint(NSLayoutConstraint(item: TypeOfProblemView, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 0, constant: ((view?.frame.width)!)-20))
            getTypeOfProblem()
            scrollView.addSubview(SendRequsetView)
            SendRequsetView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
            SendRequsetView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
            SendRequsetView.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
            SendRequsetView.rightAnchor.constraint(equalTo: scrollView.rightAnchor).isActive = true
            SendRequsetView.heightAnchor.constraint(equalToConstant: getSizeScreen().height/1.2).isActive = true
            SendRequsetView.addConstraint(NSLayoutConstraint(item: SendRequsetView, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 0, constant: ((view?.frame.width)!)-20))
            SendRequsetView.isHidden = true
            scrollView.isHidden = true
            scrollProblemView.isHidden = false
            TypeOfProblemView.isHidden = false
        }
        
        
    }
    
    @objc fileprivate func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func goBottomOfScrollView(mode: Bool) {
        
        if mode {
            UIView.animate(withDuration: 1.5, delay: 0, options: .curveEaseOut, animations: {
                
                self.scrollView.setContentOffset(CGPoint(x: 0, y: self.scrollView.contentSize.height), animated: true)
                
            }) { (completion) in
                
                
            }
            
        }
        else {
            UIView.animate(withDuration: 1.5, delay: 0, options: .curveEaseOut, animations: {
                
                self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                
            }) { (completion) in
                
                
            }
            
        }
        
    }
    
    @objc func handelDismissBlackView() {
        
        dismissKeyboard()
        
        SendRequsetView.handelDismissKeyboard()
        
        guard let annotations = MapView.annotations else { return print("Annotations Error") }
        
        if annotations.count != 0 {
            for annotation in annotations {
                MapView.removeAnnotation(annotation)
                
            }
            pinImageView.isHidden = false
        } else {
            return
        }
        
        
        SendRequsetView.budgeValue = 0
        SendRequsetView.budgeVoiceValue = 0
        SendRequsetView.imageBudgeLabel.text = ""
        SendRequsetView.voiceBudgeLabel.text = ""
        SendRequsetView.videoBudgeLabel.isHidden = true
        SendRequsetView.voiceBudgeLabel.isHidden = true
        SendRequsetView.imageBudgeLabel.isHidden = true
        SendRequsetView.addressTextField.text = ""
        SendRequsetView.issuerRequsestTextField.text = ""
        SendRequsetView.descriptionTextField.text = ""
        sendRequsetImagesArray.removeAll()
        
        TypeOfProblemView.criticismSwitch.isOn = false
        TypeOfProblemView.appreciationSwitch.isOn = false
        TypeOfProblemView.SuggestionSwitch.isOn = false
        
        blackViewSelectedMode = true
        view.endEditing(true)
        if blackViewSelectedMode {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 0
                
            }, completion: nil)
        }
        
    }
    
    func disMissBlackViewForSendRequestView() {
        
        blackViewSelectedMode = true
        view.endEditing(true)
        if blackViewSelectedMode {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 0
                
            }, completion: nil)
        }
    }
    
    var MapView: MGLMapView!
    
    var offlineMode = false
    
    
    fileprivate func setupMap() {
        
        let url = URL(string: "mapbox://styles/mapbox/streets-v11")
        MapView = MGLMapView(frame: view.bounds, styleURL: url)
        MapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        if offlineMode {
            let center = CLLocationCoordinate2D(latitude: 35.68, longitude: 51.38)
//            MapView.setCenter(CLLocationCoordinate2D(latitude: 35.68, longitude: 51.38), zoomLevel: 14, animated: false)
            MapView.setCenter(center, zoomLevel: 10.0, animated: true)
        }
        else {
            MapView.setCenter(CLLocationCoordinate2D(latitude: 35.688010, longitude: 51.382192), zoomLevel: 14.0, animated: false)
            MapView.userTrackingMode = .follow
            MapView.showsUserLocation = true
            MapView.delegate = self
        }
        
        view.addSubview(MapView)
        
        //        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(_:)))
        //        lpgr.minimumPressDuration = 1
        //        lpgr.delegate = self
        //        MapView.addGestureRecognizer(lpgr)
        
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackProgressDidChange), name: NSNotification.Name.MGLOfflinePackProgressChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackDidReceiveError), name: NSNotification.Name.MGLOfflinePackError, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackDidReceiveMaximumAllowedMapboxTiles), name: NSNotification.Name.MGLOfflinePackMaximumMapboxTilesReached, object: nil)
        
        ProgressView = UIProgressView(progressViewStyle: .default)
        ProgressView.isHidden = true
        let frame = view.bounds.size
        ProgressView.frame = CGRect(x: frame.width / 4, y: frame.height * 0.75, width: frame.width / 2, height: 10)
        view.addSubview(ProgressView)
        
    }
    var isStartLoading = true
    @nonobjc func mapView(_ mapView: MGLMapView, didUpdate userLocation: MGLUserLocation?) {
        
        if !offlineMode {
            if isStartLoading {
                isStartLoading = false
//                mapView.setCenter((userLocation?.coordinate)!, animated: true)
            }
        }
        
    }
    
    deinit {
        // Remove offline pack observers.
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func startOfflinePackDownload() {
        num = 0
        userDefult.setValue(false, forKey: "offlineMode")
        ProgressView.isHidden = false
        // Create a region that includes the current viewport and any tiles needed to view it when zoomed further in.
        // Because tile count grows exponentially with the maximum zoom level, you should be conservative with your `toZoomLevel` setting.
        let region = MGLTilePyramidOfflineRegion(styleURL: MapView.styleURL, bounds: MapView.visibleCoordinateBounds, fromZoomLevel: MapView.zoomLevel, toZoomLevel: 16)
        
        // Store some data for identification purposes alongside the downloaded resources.
        let userInfo = ["name": "My Offline Pack"]
        let context = NSKeyedArchiver.archivedData(withRootObject: userInfo)
        
        // Create and register an offline pack with the shared offline storage object.
        
        MGLOfflineStorage.shared.addPack(for: region, withContext: context) { (pack, error) in
            guard error == nil else {
                // The pack couldn’t be created for some reason.
                print("Error: \(error?.localizedDescription ?? "unknown error")")
                return
            }
            
            // Start downloading.
            pack!.resume()
        }
        
    }
    
    // MARK: - MGLOfflinePack notification handlers
    var ProgressView = UIProgressView()
    var num = 0
    @objc func offlinePackProgressDidChange(notification: NSNotification) {
        
        // Get the offline pack this notification is regarding,
        // and the associated user info for the pack; in this case, `name = My Offline Pack`
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String] {
            let progress = pack.progress
            // or notification.userInfo![MGLOfflinePackProgressUserInfoKey]!.MGLOfflinePackProgressValue
            let completedResources = progress.countOfResourcesCompleted
            let expectedResources = progress.countOfResourcesExpected
            
            // Calculate current progress percentage.
            let progressPercentage = Float(completedResources) / Float(expectedResources)
            
            // Setup the progress bar.
            
            
            ProgressView.progress = progressPercentage
            
            // If this pack has finished, print its size and resource count.
            if completedResources == expectedResources {
                let byteCount = ByteCountFormatter.string(fromByteCount: Int64(pack.progress.countOfBytesCompleted), countStyle: ByteCountFormatter.CountStyle.memory)
                print("Offline pack “\(userInfo["name"] ?? "unknown")” completed: \(byteCount), \(completedResources) resources")
                ProgressView.isHidden = true
                num += 1
                if num == 1 {
                    userDefult.setValue(false, forKey: "offlineMode")
                    let alert = CDAlertView(title: "", message: "دانلود با موفقیت انجام شد", type: .success)
                    let action = CDAlertViewAction(title: "تأیید")
                    alert.add(action: action)
                    alert.show()
                    
                }
                
                
            } else {
                // Otherwise, print download/verification progress.
                print("Offline pack “\(userInfo["name"] ?? "unknown")” has \(completedResources) of \(expectedResources) resources — \(progressPercentage * 100)%.")
                
            }
        }
    }
    
    @objc func offlinePackDidReceiveError(notification: NSNotification) {
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String],
            let error = notification.userInfo?[MGLOfflinePackUserInfoKey.error] as? NSError {
            print("Offline pack “\(userInfo["name"] ?? "unknown")” received error: \(error.localizedFailureReason ?? "unknown error")")
        }
    }
    
    @objc func offlinePackDidReceiveMaximumAllowedMapboxTiles(notification: NSNotification) {
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String],
            let maximumCount = (notification.userInfo?[MGLOfflinePackUserInfoKey.maximumCount] as AnyObject).uint64Value {
            print("Offline pack “\(userInfo["name"] ?? "unknown")” reached limit of \(maximumCount) tiles.")
        }
    }
    
    
    @nonobjc func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        return nil
    }
    
    // Allow callout view to appear when an annotation is tapped.
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//
//        let center = CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude)
//        Currentcoordinate = center
//
//        MapView.userTrackingMode = .follow
//        //       annotation.coordinate = CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude)
//        print( locations[0].coordinate.latitude)
//        annotation.coordinate = CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude)
//        MapView.setCenter(center, animated: true)
//        MapView.addAnnotation(annotation)
//        //         let span = MKCoordinateSpan(latitudeDelta: 0.007, longitudeDelta: 0.007)
//        //         let region = MKCoordinateRegion(center: center, span: span)
//        //         self.mapView.setRegion(region, animated: true)
//
//    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    var coordinate: CLLocationCoordinate2D?
    var Currentcoordinate: CLLocationCoordinate2D?
    
    @objc func handleLongPress() {
        currentLocationMode = false
        let centerPointX = view.center.x
        let centerPointY = view.center.y - getSizeScreen().height / 10
        let centerPoint = CGPoint(x: centerPointX, y: centerPointY)
        coordinate =  MapView.convert(centerPoint, toCoordinateFrom: MapView)
        //            coordinate = mapView.convert(gestureRecognizer?.location(in: mapView) ?? CGPoint.zero, toCoordinateFrom: mapView)
        annotation.coordinate = CLLocationCoordinate2D(latitude: (coordinate?.latitude)!, longitude: (coordinate?.longitude)!)
        MapView.addAnnotation(annotation)
        MapView.setCenter(coordinate!, animated: true)
        pinImageView.isHidden = true
    }
    
    fileprivate func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.lightColor.light
        navigationController?.navigationBar.tintColor = .white
        
        if !offlineMode {
            let image = UIImage(named: "back")
            let cancel = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handelDismiss))
            navigationItem.setLeftBarButton(cancel, animated: true)
            
            let image1 = UIImage(named: "locationFill")?.withRenderingMode(.alwaysTemplate)
            let location = UIBarButtonItem(image: image1, style: .plain, target: self, action: #selector(showLocationUser))
            navigationItem.setRightBarButton(location, animated: true)
        }
        
        
        
    }
    
    @objc fileprivate func showLocationUser() {
        
        setupMap()
        setupButtomView()
    }
    
    let imagePickerController = UIImagePickerController()
    let cameraPickerController = UIImagePickerController()
    var isVideo = false
    
    func showImagePicker() {
        imagePickerController.delegate = self
        imagePickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePickerController.mediaTypes = ["public.image", "public.movie"]
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    func openCamera() {
        cameraPickerController.delegate = self
        cameraPickerController.sourceType = UIImagePickerController.SourceType.camera
        self.present(cameraPickerController, animated: true, completion: nil)
    }
    
    var sendRequsetImagesArray: [Data] = []
    var sendRequsetImagesFeedbackArray: [Data] = []
    var videoUrl: URL?
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        
        if picker == self.imagePickerController {
            
            if isVideo {
                videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? URL
                SendRequsetView.videoBudgeLabel.isHidden = false
            }
            else {
                let myImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
                let compressedImage = myImage?.jpeg(.lowest)
                sendRequsetImagesArray.append(compressedImage!)
                sendRequsetImagesFeedbackArray.append(compressedImage!)
                let numImage = sendRequsetImagesArray.count
                SendRequsetView.budgeValue = numImage
                SendRequsetView.addBudgeToImageBudgeLabel()
            }
        }
        else if picker == self.cameraPickerController {
            
            if isVideo {
                videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? URL
                SendRequsetView.videoBudgeLabel.isHidden = false
            }
            else {
                let myImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
                let compressedImage = myImage?.jpeg(.lowest)
                sendRequsetImagesArray.append(compressedImage!)
                sendRequsetImagesFeedbackArray.append(compressedImage!)
                let numImage = sendRequsetImagesArray.count
                SendRequsetView.budgeValue = numImage
                SendRequsetView.addBudgeToImageBudgeLabel()
            }
        }
        
        self.dismiss(animated: true, completion: nil)
        isVideo = false
        showProblemView()
    }
    
    @objc func showCurrentLocation() {
        locationManager.stopUpdatingLocation()
        showLoc()
    }
    
    @objc func handelDismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    func showLoc() {
        if CLLocationManager.locationServicesEnabled() {
            
            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined  {
                
                locationManager.requestWhenInUseAuthorization()
                
            }
            
            locationManager.desiredAccuracy = 1.0
            
            locationManager.startUpdatingLocation()
            currentLocationMode = true
        }
        
    }
    
    
    var problemType = ""
    private var uploadRequest: Request?
    private var uploadFailed = true
    var urlVoice: URL!
    
    private var probIssue: String?
    private var probAdress: String?
    private var probDescription: String?
    private var userID: String?
    
    func sendProblem(issue: String, address: String, Description: String) {
        probIssue = issue
        probAdress = address
        probDescription = Description
        uploadMode = true
        self.updateUserInterface()
        
        let lat = String(annotation.coordinate.latitude)
        let long = String(annotation.coordinate.longitude)
        let userInfoURL = baseURLString + "subrequest.php"
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        if let ussid = UserDefaults.standard.value(forKey: "userid") as? String {
            userID = ussid
        }
        print(userID!)
        let parameter = ["ProblemType":problemType, "title": issue, "userid": userID!, "addrss": address, "des": Description, "lat": lat, "lnt": long]
        
        //        let getUrl = userInfoURL.data(using: String.Encoding.ascii, allowLossyConversion: true)
        //        let urlString = String(data: getUrl!, encoding: String.Encoding.ascii)
        guard let url = URL(string: userInfoURL) else{return}
        var counter = 0
        showProgressView()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            
            
            var numimgaeloop = 1
            for im in self.sendRequsetImagesArray {
                multipartFormData.append(im, withName: "img\(numimgaeloop)", fileName: "image.jpg", mimeType: "image/jpg")
                numimgaeloop+=1
            }
            
            do {
                if let uVoice = self.urlVoice {
                    let voiceData = try Data(contentsOf: uVoice)
                    multipartFormData.append(voiceData, withName: "audio", fileName: "1.mp3", mimeType: "audio/mp3")
                }
                
                
            }
            catch {
                print("error of Voice")
            }
            
            do {
                if let uVideo = self.videoUrl {
                    let voiceData = try Data(contentsOf: uVideo)
                    multipartFormData.append(voiceData, withName: "video", fileName: "1.mp4", mimeType: "video/mp4")
                }
                
                
            }
            catch {
                print("error of Voice")
            }
            
            for (key, value) in parameter {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            
            
            switch result {
             
                
                
            case .success(let upload, _, _):
                
                self.uploadRequest = upload
                self.dismissKeyboard()
                self.handelDismissBlackView()
                
                
                upload.uploadProgress(closure: { (progress) in
                    
                    let numProgress = progress.fractionCompleted
                    let roundProgress = self.roundDoubleNumber(number: numProgress)
                    self.progressView.progress = Float(roundProgress)
                    let progressTextLabel = String(roundProgress * 100)  + "%"
                    self.progressLabel.text = progressTextLabel
                    
                    if progressTextLabel == "100.0%" {
                        counter += 1
                        if counter == 1 {
                            self.dismissBackProgressView()
                            self.progressView.progress = 0.0
                            self.progressLabel.text = ""
                            self.showAlertSecsess()
                            
                        }
                        
                        
                    }
                })
                
                
            case .failure(let encodingError):
                print(encodingError.localizedDescription)
                self.showAlertFaile()
            }
            
            
            
        }
        
    }
    
    
    
    var ProblemTypeArray: [String] = []
    var addressArray: [String] = []
    var titleArray: [String] = []
    var desArray: [String] = []
    var latArray: [String] = []
    var lntArray: [String] = []
    var imageSelectArray: [[Data]] = []
    var currDateArray: [String] = []
    var voiceSelectArray: [Data] = []
    var videoSelectArray: [Data] = []
    var sendStatusModeArray: [Bool] = []
    
    let persianCalender: GDCalendar = {
        let pc = GDCalendar()
        pc.translatesAutoresizingMaskIntoConstraints = false
        pc.backgroundColor = .white
        pc.headerBackgroundColor = UIColor(r: 44, g: 202, b: 175)
        pc.itemHighlightColor = UIColor(r: 44, g: 202, b: 175)
        pc.itemHighlightTextColor = .white
        pc.itemsFont = UIFont.systemFont(ofSize: 9)
        pc.headersFont = UIFont.boldSystemFont(ofSize: 9)
        return pc
    }()
    
    func getCurrentDate() -> String {
        
        var Day = ""
        var Month = ""
        var Year = ""
        var FullDate = ""
        
        let day = String(persianCalender.currentDate.componentsOfDate.day)
        if let number = Int(day.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
            let perNum = convertEngNumToPersianNum(num: String(number))
            Day = perNum
        }
        let year = String(persianCalender.currentDate.componentsOfDate.year)
        if let number = Int(year.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
            let perNum = convertEngNumToPersianNum(num: String(number))
            Year = perNum
        }
        
        let month = String(persianCalender.currentDate.componentsOfDate.month)
        if let number = Int(month.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
            let perNum = convertEngNumToPersianNum(num: String(number))
            Month = perNum
        }
        
        FullDate = Year + "/" + Month + "/" + Day
        return FullDate
        
    }
    
    
    var voiceData: Data?
    func saveRequestToCoreDataDB(issue: String, address: String, Description: String) {
        dismissKeyboard()
        handelDismissBlackView()
        
        voiceSelectArray.removeAll()
        
        let userDefult = UserDefaults()
        let lat = String(annotation.coordinate.latitude)
        let long = String(annotation.coordinate.longitude)
        let currDate = getCurrentDate()
        
        if let pta = userDefult.value(forKey: "ProblemTypeArray") {
            ProblemTypeArray = pta as! [String]
        }
        if let ar = userDefult.value(forKey: "addressArray") {
            addressArray = ar as! [String]
        }
        if let pta = userDefult.value(forKey: "titleArray") {
            titleArray = pta as! [String]
        }
        if let da = userDefult.value(forKey: "desArray") {
            desArray = da as! [String]
        }
        if let la = userDefult.value(forKey: "latArray") {
            latArray = la as! [String]
        }
        if let lna = userDefult.value(forKey: "lntArray") {
            lntArray = lna as! [String]
        }
        if let pta = userDefult.value(forKey: "imageSelectArray") {
            imageSelectArray = pta as! [[Data]]
        }
        if let pta = userDefult.value(forKey: "currDateArray") {
            currDateArray = pta as! [String]
        }
        if let pta = userDefult.value(forKey: "voiceSelectArray") {
            voiceSelectArray = pta as! [Data]
        }
        if let pta = userDefult.value(forKey: "sendStatusModeArray") {
            sendStatusModeArray = pta as! [Bool]
        }
        if let video = userDefult.value(forKey: "videoSelectArray") as? [Data] {
            videoSelectArray = video
        }
        
        ProblemTypeArray.append(problemType)
        addressArray.append(address)
        titleArray.append(issue)
        desArray.append(Description)
        latArray.append(lat)
        lntArray.append(long)
        imageSelectArray.append(sendRequsetImagesFeedbackArray)
        currDateArray.append(currDate)
        sendStatusModeArray.append(false)
        
        do {
            if let URLVoive = self.urlVoice {
                let voiceData = try Data(contentsOf: URLVoive)
                voiceSelectArray.append(voiceData)
            }
            
        }
        catch {
            print("error of Voice")
        }
        
        
        if voiceData == nil {
            voiceSelectArray.append(Data())
        }
        
        do {
            if let URLvideo = self.videoUrl {
                let voiceData = try Data(contentsOf: URLvideo)
                videoSelectArray.append(voiceData)
            }
            
        }
        catch {
            videoSelectArray.append(Data())
        }
        
        if videoUrl == nil {
            videoSelectArray.append(Data())
        }
        
        userDefult.setValue(ProblemTypeArray, forKey: "ProblemTypeArray")
        userDefult.setValue(addressArray, forKey: "addressArray")
        userDefult.setValue(titleArray, forKey: "titleArray")
        userDefult.setValue(desArray, forKey: "desArray")
        userDefult.setValue(latArray, forKey: "latArray")
        userDefult.setValue(lntArray, forKey: "lntArray")
        userDefult.setValue(imageSelectArray, forKey: "imageSelectArray")
        userDefult.setValue(currDateArray, forKey: "currDateArray")
        userDefult.setValue(voiceSelectArray, forKey: "voiceSelectArray")
        userDefult.setValue(sendStatusModeArray, forKey: "sendStatusModeArray")
        userDefult.setValue(videoSelectArray, forKey: "videoSelectArray")
        
        
        let numberID = ProblemTypeArray.count
        home?.budgeNumItemSaveToCoreData = numberID
        home?.showNumberOfItemsSaveToCoreData()
        let alert = CDAlertView(title: "درخواست بصورت آفلاین ذخیره شد", message: "از بخش درخواست ها میتوانید این درخواست را مجددا ارسال کنید", type: CDAlertViewType.success)
        alert.titleFont = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        alert.titleTextColor = UIColor.greenColor.green
        alert.messageFont = UIFont.systemFont(ofSize: getSizeScreen().height / 52)
        alert.messageTextColor = .gray
        let action = CDAlertViewAction(title: "تأیید", font: UIFont.systemFont(ofSize: getSizeScreen().height / 55), textColor: UIColor.blueColor.blue, backgroundColor: .white, handler: nil)
        alert.add(action: action)
        alert.show()
        
        
        
        
    }
    
    
    
    fileprivate func showAlertSecsess() {
        
        let alert = CDAlertView(title: "با موفقیت انجام شد", message: "شما مینوانید از بخش پیگیری درخواست وضعیت درخواست خود را دنیال کنید", type: CDAlertViewType.success)
        alert.titleTextColor = UIColor(r: 25, g: 204, b: 12)
        alert.messageTextColor = .darkGray
        let action = CDAlertViewAction(title: "تایید", font: UIFont.systemFont(ofSize: getSizeScreen().height / 50), textColor: UIColor.blueColor.blue, backgroundColor: .white, handler: nil)
        alert.add(action: action)
        alert.show()
        
    }
    
    fileprivate func showAlertFaile() {
        dismissBackProgressView()
        let alert = CDAlertView(title: "مشکل در ارسال اطلاعات", message: "درخواست شما ارسال نشد، برای ارسال مجدد به بخش پیگیری درخواست مراجعه کنید", type: CDAlertViewType.error)
        alert.titleTextColor = .red
        alert.messageTextColor = .darkGray
        //        let action = CDAlertViewAction(title: "تایید", font: UIFont.systemFont(ofSize: getSizeScreen().height / 50), textColor: UIColor.blueColor.blue, backgroundColor: .white, handler: nil)
        let action = CDAlertViewAction(title: "تایید", font: UIFont.systemFont(ofSize: getSizeScreen().height / 50), textColor: UIColor.blueColor.blue, backgroundColor: .white) { (action) -> Bool in
            self.saveRequestToCoreDataDB(issue: self.probIssue!, address: self.probAdress!, Description: self.probDescription!)
            return true
        }
        alert.add(action: action)
        alert.show()
        
    }
    
    let progressWhiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    let progressBlackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.6)
        return view
    }()
    
    let titleProgressLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "درحال فشرده سازی و ارسال اطلاعات..."
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let progressLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .gray
        lbl.text = "۱۵٪"
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    var progressView: UIProgressView = {
        let progress = UIProgressView(progressViewStyle: .bar)
        progress.progress = 0.5
        progress.trackTintColor = .white
        progress.progressTintColor = UIColor.blueColor.blue
        progress.translatesAutoresizingMaskIntoConstraints = false
        return progress
    }()
    
    
    let cancelProgressAndUploadProblemButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.tintColor = .red
        btn.setTitle("لفو", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    func showProgressView() {
        
        if let windows = UIApplication.shared.keyWindow {
            
            windows.addSubview(progressBlackView)
            progressBlackView.frame = windows.frame
            progressBlackView.addSubview(progressWhiteView)
            setupProgressWhiteView()
            progressBlackView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.progressBlackView.alpha = 1
                
            }, completion: nil)
        }
        
    }
    
    fileprivate func setupProgressWhiteView() {
        if let windows = UIApplication.shared.keyWindow {
            
            progressWhiteView.frame.size.height = windows.frame.height / 4 - 10
            progressWhiteView.frame.size.width = windows.frame.width - 50
            progressWhiteView.layer.cornerRadius = 10
            progressWhiteView.layer.masksToBounds = true
            progressWhiteView.center = blackView.center
            
            progressWhiteView.addSubview(titleProgressLabel)
            titleProgressLabel.topAnchor.constraint(equalTo: progressWhiteView.topAnchor, constant: 15).isActive = true
            titleProgressLabel.rightAnchor.constraint(equalTo: progressWhiteView.rightAnchor).isActive = true
            titleProgressLabel.leftAnchor.constraint(equalTo: progressWhiteView.leftAnchor).isActive = true
            titleProgressLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
            titleProgressLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
            
            progressWhiteView.addSubview(progressLabel)
            progressLabel.topAnchor.constraint(equalTo: titleProgressLabel.bottomAnchor, constant: 15).isActive = true
            progressLabel.rightAnchor.constraint(equalTo: progressWhiteView.rightAnchor).isActive = true
            progressLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
            progressLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
            progressLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 52)
            
            progressWhiteView.addSubview(progressView)
            progressView.rightAnchor.constraint(equalTo: progressLabel.leftAnchor).isActive = true
            progressView.leftAnchor.constraint(equalTo: progressWhiteView.leftAnchor, constant: 10).isActive = true
            progressView.centerYAnchor.constraint(equalTo: progressLabel.centerYAnchor).isActive = true
            
            progressWhiteView.addSubview(cancelProgressAndUploadProblemButton)
            cancelProgressAndUploadProblemButton.bottomAnchor.constraint(equalTo: progressWhiteView.bottomAnchor, constant: -20).isActive = true
            cancelProgressAndUploadProblemButton.centerXAnchor.constraint(equalTo: progressWhiteView.centerXAnchor).isActive = true
            cancelProgressAndUploadProblemButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
            cancelProgressAndUploadProblemButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
            cancelProgressAndUploadProblemButton.addTarget(self, action: #selector(stopSending), for: .touchUpInside)
            
        }
    }
    
    @objc fileprivate func stopSending() {
        dismissBackProgressView()
        uploadRequest?.cancel()
        uploadRequest = nil
    }
    
    func convertEngNumToPersianNum(num: String) -> String {
        //let number = NSNumber(value: Int(num)!)
        
        let format = NumberFormatter()
        format.locale = Locale(identifier: "fa_IR")
        let number =   format.number(from: num)
        let faNumber = format.string(from: number!)
        return faNumber!
        
    }
    
    @objc fileprivate func dismissBackProgressView() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.progressBlackView.alpha = 0
            
        }, completion: nil)
    }
    
}

//
//  DescriptionController.swift
//  hamshahriyar
//
//  Created by apple on 10/22/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit

private let cellId = "cellId"

class DescriptionController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    let descriptionTitleText = ["نظارت بر عملکرد شهرداری پردیس", "دسترسی به آخرین اخبار", "بدون نیاز به اینترنت"]
    let descriptionimage = ["cone1", "apple_news_icon", "coins"]
    let descriptionText = ["در نرم افزار شهروند یار امکان ارسال مشکلات شهری به شهرداری پردیس فراهم شده است.", "در نرم افزار شهروند یار امکان مشاهده مکان های گردشگری هر شهر وجود دارد", "در نرم افزار شهروندیار یار امکان استفاده از نقشه به صورت آفلاین هم فراهم است."]
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .white
        return cv
    }()
    
    let bottomView: UIView = {
        let view = UIView()
        return view
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
    }
    
    fileprivate func setupCollectionView() {
        
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
        
        collectionView.register(descriptionCell.self, forCellWithReuseIdentifier: cellId)
        view.addSubview(collectionView)
        collectionView.frame = view.frame
        collectionView.isPagingEnabled = true
        
        setupViews()
    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    let firstCircleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 69, g: 90, b: 100)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        return view
    }()
    let secondCircleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 180, g: 185, b: 185)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        return view
    }()
    let thirdCircleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 180, g: 185, b: 185)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        return view
    }()
    
    let passButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.tintColor = UIColor(r: 69, g: 90, b: 100)
        btn.backgroundColor = .white
        btn.setTitle("رد کردن", for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let loginButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.tintColor = UIColor(r: 69, g: 90, b: 100)
        btn.backgroundColor = .white
        btn.setTitle("ورود", for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isHidden = true
        return btn
    }()
    
    fileprivate func setupViews() {
        view.addSubview(bottomView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: bottomView)
        view.addConstraintWithFormat(format: "V:[v0(\(getSizeScreen().height / 10))]", views: bottomView)
        let guide = view.safeAreaLayoutGuide
        bottomView.bottomAnchor.constraint(equalTo: guide.bottomAnchor).isActive = true
        
        passButton.addTarget(self, action: #selector(exitDesctiptionView), for: .touchUpInside)
        loginButton.addTarget(self, action: #selector(exitDesctiptionView), for: .touchUpInside)
        
        bottomView.addSubview(firstCircleView)
        bottomView.addSubview(secondCircleView)
        bottomView.addSubview(thirdCircleView)
        bottomView.addSubview(passButton)
        bottomView.addSubview(loginButton)
        
        secondCircleView.centerXAnchor.constraint(equalTo: bottomView.centerXAnchor).isActive = true
        secondCircleView.centerYAnchor.constraint(equalTo: bottomView.centerYAnchor).isActive = true
        secondCircleView.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 40).isActive = true
        secondCircleView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 40).isActive = true
        secondCircleView.layer.cornerRadius = getSizeScreen().width / 80
        
        firstCircleView.rightAnchor.constraint(equalTo: secondCircleView.leftAnchor, constant: -7).isActive = true
        firstCircleView.centerYAnchor.constraint(equalTo: bottomView.centerYAnchor).isActive = true
        firstCircleView.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 40).isActive = true
        firstCircleView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 40).isActive = true
        firstCircleView.layer.cornerRadius = getSizeScreen().width / 80
        
        thirdCircleView.leftAnchor.constraint(equalTo: secondCircleView.rightAnchor, constant: 7).isActive = true
        thirdCircleView.centerYAnchor.constraint(equalTo: bottomView.centerYAnchor).isActive = true
        thirdCircleView.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 40).isActive = true
        thirdCircleView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 40).isActive = true
        thirdCircleView.layer.cornerRadius = getSizeScreen().width / 80
        
        passButton.leftAnchor.constraint(equalTo: bottomView.leftAnchor).isActive = true
        passButton.centerYAnchor.constraint(equalTo: bottomView.centerYAnchor).isActive = true
        passButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 14).isActive = true
        passButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        
        loginButton.rightAnchor.constraint(equalTo: bottomView.rightAnchor).isActive = true
        loginButton.centerYAnchor.constraint(equalTo: bottomView.centerYAnchor).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 14).isActive = true
        loginButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
    }
    
    @objc fileprivate func exitDesctiptionView() {
        
        let messageController = sendMessageController()
        self.present(messageController, animated: true, completion: nil)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! descriptionCell
        cell.titleLabel.text = descriptionTitleText[indexPath.item]
        cell.imageView.image = UIImage(named: descriptionimage[indexPath.item])
        cell.descriptionLabel.text = descriptionText[indexPath.item]
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.safeAreaLayoutGuide.layoutFrame.width, height: view.safeAreaLayoutGuide.layoutFrame.height )
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = targetContentOffset.pointee.x / view.frame.width
        
        if index == 1 {
            
            secondCircleView.backgroundColor = UIColor(r: 69, g: 90, b: 100)
            firstCircleView.backgroundColor = UIColor(r: 180, g: 185, b: 185)
            thirdCircleView.backgroundColor = UIColor(r: 180, g: 185, b: 185)
            passButton.isHidden = false
            loginButton.isHidden = true
            
        }
        else if index == 2 {
            
            secondCircleView.backgroundColor = UIColor(r: 180, g: 185, b: 185)
            firstCircleView.backgroundColor = UIColor(r: 180, g: 185, b: 185)
            thirdCircleView.backgroundColor = UIColor(r: 69, g: 90, b: 100)
            passButton.isHidden = true
            loginButton.isHidden = false
            
        }
        else {
            
            secondCircleView.backgroundColor = UIColor(r: 180, g: 185, b: 185)
            firstCircleView.backgroundColor = UIColor(r: 69, g: 90, b: 100)
            thirdCircleView.backgroundColor = UIColor(r: 180, g: 185, b: 185)
            passButton.isHidden = false
            loginButton.isHidden = true
        }
        
        
    }
    
}

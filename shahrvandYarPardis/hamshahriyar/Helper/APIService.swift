//
//  APIService.swift
//  hamshahriyar
//
//  Created by apple on 10/22/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


let shareInterface = APIService()

let baseURLString = "http://46.100.56.72:8587/app/"



class APIService: NSObject {
    
    
    func convertPersianNumToEngNum(num: String) -> String {
        //let number = NSNumber(value: Int(num)!)
        
        let format = NumberFormatter()
        format.locale = Locale(identifier: "EN")
        let number =   format.number(from: num)
        let faNumber = format.string(from: number!)
        return faNumber!
        
    }
    
    
    
    func SendSMS(mobile: String) {
        
        let sendSmsUrlString = baseURLString + "enternum.php?mobile=\(mobile)"
        guard let url = URL(string: sendSmsUrlString) else {return}
        Alamofire.request(url, method: .get).responseJSON { (response) in
            
            if let data = response.result.value {
                let json = JSON(data)
                print(json)
            }
            
            guard response.result.isSuccess else {
                let err = String(describing: response.result.error)
                print("Error while fetching remote rooms: \(err)")
                return
            }
            
            guard let value = response.result.value as? [String: Any],
                let rows = value["rows"] as? [[String: Any]] else {
                    print("Malformed data received from fetchAllRooms service")
                    
                    return
            }
            
            print(rows)
        }
        
    }
    
    
    
    
}

//
//  baseCell.swift
//  hamshahriyar
//
//  Created by apple on 10/22/1397 AP.
//  Copyright © 1397 apple. All rights reserved.
//

import UIKit


class baseCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    func setupViews()
    {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

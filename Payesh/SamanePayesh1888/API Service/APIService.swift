//
//  APIService.swift
//  SamanePayesh1888
//
//  Created by Parsai on 9/12/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


let shareInterface = APIService()

let baseURLString = "http://212.33.202.139:8587/app/"



class APIService: NSObject {
    
    
    func convertPersianNumToEngNum(num: String) -> String {
        //let number = NSNumber(value: Int(num)!)
        
        let format = NumberFormatter()
        format.locale = Locale(identifier: "EN")
        let number =   format.number(from: num)
        let faNumber = format.string(from: number!)
        return faNumber!
        
    }
    

    
    func SendSMS(mobile: String) {
 
        let sendSmsUrlString = baseURLString + "enternum.php?mobile=\(mobile)"
        guard let url = URL(string: sendSmsUrlString) else {return}
        Alamofire.request(url, method: .post).responseJSON { (response) in

            guard response.result.isSuccess else {
                let err = String(describing: response.result.error)
                      print("Error while fetching remote rooms: \(err)")
                return
            }
            
            guard let value = response.result.value as? [String: Any],
                let rows = value["rows"] as? [[String: Any]] else {
                    print("Malformed data received from fetchAllRooms service")
                    
                    return
            }
            
            print(rows)
        }
        
    }
    
    

    
}


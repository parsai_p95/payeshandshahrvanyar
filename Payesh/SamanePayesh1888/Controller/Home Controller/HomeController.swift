//
//  HomeController.swift
//  SamanePayesh1888
//
//  Created by Parsai on 9/18/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Instructions

class HomeController: UIViewController, CoachMarksControllerDataSource, CoachMarksControllerDelegate {
    
    
    
    let statusBarView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        return view
    }()
    
    let loader: NVActivityIndicatorView = {
        let loader = NVActivityIndicatorView(frame: .zero)
        let loaderType = NVActivityIndicatorType.ballRotateChase
        loader.type = loaderType
        loader.color = .white
        loader.padding = 5
        loader.translatesAutoresizingMaskIntoConstraints = false
        return loader
    }()
    
    let baseWhiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let versionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "سامانه پایش"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let versionView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 240, g: 40, b: 73)
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let homeImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "city"))
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "top_logo"))
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    let userDefult = UserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        if let appVersion = userDefult.value(forKey: "appVersion") {
//            versionLabel.text = (appVersion as! String)
//        }
        
        view.backgroundColor = .white
        setupStatusBar()
        setupViews()
//        getVersionOfApp()
        setupHelpApp()
        
        if let IsUpdateToObserve = UserDefaults.standard.value(forKey: "isUpdateToObserve") as? Bool {
            if IsUpdateToObserve {
                changeUpgradeToObserveOps()
            }
        }
    }
    
    let coachMarksController = CoachMarksController()
    
    fileprivate func setupHelpApp() {
        
        coachMarksController.dataSource = self
        coachMarksController.overlay.color = UIColor(white: 0, alpha: 0.7)
        pointOfInterest.frame = CGRect(x: getSizeScreen().width / 3 - 13, y: getSizeScreen().height / 2 - getSizeScreen().height / 12, width: getSizeScreen().width / 2.4, height: getSizeScreen().height / 11.5)
        pointOfInterest2.frame = CGRect(x: getSizeScreen().height / 20  , y: getSizeScreen().height / 2 + getSizeScreen().height / 12 , width: getSizeScreen().width / 5.7, height: getSizeScreen().width / 5.7)
        pointOfInterest5.frame = CGRect(x: getSizeScreen().width - getSizeScreen().height / 20 - getSizeScreen().width / 6 , y: getSizeScreen().height / 2 + getSizeScreen().height / 12 , width: getSizeScreen().width / 5.7, height: getSizeScreen().width / 5.7)
        pointOfInterest3.frame = CGRect(x: getSizeScreen().width - getSizeScreen().height / 20 - getSizeScreen().width / 6 , y: getSizeScreen().height - getSizeScreen().height / 7.5 - getSizeScreen().width / 6 , width: getSizeScreen().width / 5.7, height: getSizeScreen().width / 5.7)
        pointOfInterest4.frame = CGRect(x: getSizeScreen().height / 20  , y: getSizeScreen().height - getSizeScreen().height / 7.5 - getSizeScreen().width / 6 , width: getSizeScreen().width / 5.7, height: getSizeScreen().width / 5.7)
        
    }
    
    func numberOfCoachMarks(for coachMarksController: CoachMarksController) -> Int {
        return 5
    }
    let pointOfInterest = UIView()
    let pointOfInterest2 = UIView()
    let pointOfInterest3 = UIView()
    let pointOfInterest4 = UIView()
    let pointOfInterest5 = UIView()
    
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkAt index: Int) -> CoachMark {
        
        let array = [pointOfInterest, pointOfInterest2, pointOfInterest3 ,pointOfInterest4, pointOfInterest5]
        
        return coachMarksController.helper.makeCoachMark(for: array[index])
    }
    
    let textHelpArray = ["خوش آمدید\n\n این راهنما شما را با بخش های این نرم افزار آشنا می کند","اخبار\n\n امکان دسترسی به آخرین اخبار شهرداری و سامانه","ارتقاع به ناظر\n\n درخواست تبدیل شدن به ناظر و دریافت کد ناظر برای استفاده از آن در راه های ارتباطی دیگر","پیگیری درخواست ها\n\n همه ی درخواست های شما در این بخش قابل مشاهده و پیگیری هستند", "مشکلات شهری\n\nامکان ثبت مشکل بصورت آفلاین و آنلاین"]
    
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkViewsAt index: Int, madeFrom coachMark: CoachMark) -> (bodyView: CoachMarkBodyView, arrowView: CoachMarkArrowView?) {
        let coachViews = coachMarksController.helper.makeDefaultCoachViews(withArrow: true, arrowOrientation: coachMark.arrowOrientation)
        

        coachViews.bodyView.hintLabel.textAlignment = .center
        coachViews.bodyView.hintLabel.text = textHelpArray[index]
        if index == 4 {
            coachViews.bodyView.nextLabel.text = "در آخر"
        }
        else {
            coachViews.bodyView.nextLabel.text = "ادامه"
        }
        
        
        return (bodyView: coachViews.bodyView, arrowView: coachViews.arrowView)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let firstStartAppMode = userDefult.value(forKey: "startAppMode") as? Bool {
            if firstStartAppMode {
                self.coachMarksController.start(in: .window(over: self))
                userDefult.setValue(false, forKey: "startAppMode")
            }
        }
 
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.coachMarksController.stop(immediately: true)
    }

    
    func convertEngNumToPersianNum(num: String) -> String {
        //let number = NSNumber(value: Int(num)!)
        
        let format = NumberFormatter()
        format.locale = Locale(identifier: "fa_IR")
        let number =   format.number(from: num)
        let faNumber = format.string(from: number!)
        return faNumber!
        
    }
    
    
    private func getVersionOfApp() {

        let urlString =  baseURLString + "show_version.php"
        guard let url = URL(string: urlString) else {return}
        
        
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            
            if let data = data {
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    
                    if let JSON = json as? NSDictionary {
                        if let  version = JSON["version"] as? String {
                            
                            let versions = self.convertEngNumToPersianNum(num: version)
                            DispatchQueue.main.sync {
                                self.versionLabel.text = "سامانه پایش" + "  " + versions
                                
                                self.userDefult.setValue(self.versionLabel.text, forKey: "appVersion")
                                
                            }
                            
                        }
                    }
                }
                catch {
                }
                
            }
            
            }.resume()
    }
    
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    fileprivate func setupStatusBar() {
        view.addSubview(statusBarView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: statusBarView)
        view.addConstraintWithFormat(format: "V:|[v0(\(getSizeScreen().height / 25))]", views: statusBarView)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    let CoreDataOperation = coreDataOperation()

    fileprivate func setupViews() {

        let userDefult = UserDefaults()
        if let typee = userDefult.value(forKey: "ProblemTypeArray") {
            let ttt = typee  as! [String]
            let numItemOfCoreData = ttt.count
            if numItemOfCoreData != 0 {
                budgeNumItemSaveToCoreData = numItemOfCoreData
                budgNumberItemSaveToCoreDataLabel.isHidden = false
                showNumberOfItemsSaveToCoreData()
            }
            else {
                budgNumberItemSaveToCoreDataLabel.isHidden = true
            }
        }

        
        view.addSubview(homeImageView)
        homeImageView.topAnchor.constraint(equalTo: statusBarView.bottomAnchor).isActive = true
        homeImageView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        homeImageView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        homeImageView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 2).isActive = true
        
        view.addSubview(logoImageView)
        logoImageView.topAnchor.constraint(equalTo: statusBarView.bottomAnchor, constant: getSizeScreen().height / 35).isActive = true
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImageView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        
        view.addSubview(baseWhiteView)
        baseWhiteView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        baseWhiteView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        baseWhiteView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        baseWhiteView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 1.8).isActive = true
        baseWhiteView.layer.cornerRadius = getSizeScreen().height / 13.5
        
        baseWhiteView.addSubview(versionView)
        versionView.topAnchor.constraint(equalTo: baseWhiteView.topAnchor, constant: -getSizeScreen().height / 36).isActive = true
        versionView.centerXAnchor.constraint(equalTo: baseWhiteView.centerXAnchor).isActive = true
        versionView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2.3).isActive = true
        versionView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 12).isActive = true
        versionView.layer.cornerRadius = getSizeScreen().height / 24
        
        versionView.addSubview(loader)
        loader.centerXAnchor.constraint(equalTo: versionView.centerXAnchor).isActive = true
        loader.centerYAnchor.constraint(equalTo: versionView.centerYAnchor).isActive = true
        loader.heightAnchor.constraint(equalToConstant: 30).isActive = true
        loader.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        versionView.addSubview(versionLabel)
        versionLabel.centerXAnchor.constraint(equalTo: versionView.centerXAnchor).isActive = true
        versionLabel.centerYAnchor.constraint(equalTo: versionView.centerYAnchor).isActive = true
        versionLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        versionLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        
        setupBaseButton()
    }
    
    let editButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.layer.masksToBounds = false
        let image = UIImage(named: "gear")?.withRenderingMode(.alwaysOriginal)
        btn.setImage(image, for: .normal)
        btn.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        btn.layer.shadowOpacity = 1
        btn.layer.shadowOffset = CGSize.zero
        btn.layer.shadowRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let editLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
//        label.textColor = .lightGray
        label.textAlignment = .center
        label.text = "تنظیمات"
        
        label.backgroundColor = .clear
//        label.isHidden = true
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let problemCityButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.layer.masksToBounds = false
        let image = UIImage(named: "problem")?.withRenderingMode(.alwaysOriginal)
        btn.setImage(image, for: .normal)
        btn.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        btn.layer.shadowOpacity = 1
        btn.layer.shadowOffset = CGSize.zero
        btn.layer.shadowRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let problemCityLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.textAlignment = .center
        label.text = "مشکلات شهری"
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let newsButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.layer.masksToBounds = false
        let image = UIImage(named: "news")?.withRenderingMode(.alwaysOriginal)
        btn.setImage(image, for: .normal)
        btn.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        btn.layer.shadowOpacity = 1
        btn.layer.shadowOffset = CGSize.zero
        btn.layer.shadowRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let newsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.textAlignment = .center
        label.text = "اخبار و اطلاعیه"
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let supportButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.layer.masksToBounds = false
        let image = UIImage(named: "follow_up")?.withRenderingMode(.alwaysOriginal)
        btn.setImage(image, for: .normal)
        btn.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        btn.layer.shadowOpacity = 1
        btn.layer.shadowOffset = CGSize.zero
        btn.layer.shadowRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let supportLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.textAlignment = .center
        label.text = "پیگیری درخواست"
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let aboutUsButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .clear
        btn.tintColor = UIColor(r: 240, g: 40, b: 73)
        btn.setTitle("درباره ما", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    //Observer
    
    let observerButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.layer.masksToBounds = false
        let image = UIImage(named: "observer")?.withRenderingMode(.alwaysOriginal)
        btn.setImage(image, for: .normal)
        btn.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        btn.layer.shadowOpacity = 1
        btn.layer.shadowOffset = CGSize.zero
        btn.layer.shadowRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let observerLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.textAlignment = .center
        label.text = "ارتقا به ناظر"
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let SupportButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .clear
        btn.tintColor = UIColor(r: 240, g: 40, b: 73)
        btn.setTitle("پشتیبانی", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let budgNumberItemSaveToCoreDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.text = "۱"
        label.backgroundColor = .red
        label.layer.masksToBounds = true
        label.isHidden = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate func setupBaseButton() {
        
        baseWhiteView.addSubview(editButton)
        baseWhiteView.addSubview(problemCityLabel)
        baseWhiteView.addSubview(problemCityButton)
        baseWhiteView.addSubview(editLabel)
        baseWhiteView.addSubview(newsButton)
        baseWhiteView.addSubview(newsLabel)
        baseWhiteView.addSubview(SupportButton)
        baseWhiteView.addSubview(aboutUsButton)
        baseWhiteView.addSubview(supportButton)
        baseWhiteView.addSubview(supportLabel)
        baseWhiteView.addSubview(observerLabel)
        baseWhiteView.addSubview(observerButton)
        baseWhiteView.addSubview(budgNumberItemSaveToCoreDataLabel)
        
        problemCityButton.addTarget(self, action: #selector(handelProblemCity), for: .touchUpInside)
        newsButton.addTarget(self, action: #selector(handelNews), for: .touchUpInside)
        supportButton.addTarget(self, action: #selector(handelFollowUpRequest), for: .touchUpInside)
        observerButton.addTarget(self, action: #selector(upgradeToObserve), for: .touchUpInside)
        SupportButton.addTarget(self, action: #selector(handelTicket), for: .touchUpInside)
        aboutUsButton.addTarget(self, action: #selector(handelAboutUs), for: .touchUpInside)
        editButton.addTarget(self, action: #selector(handelSetting), for: .touchUpInside)
        
        editButton.centerXAnchor.constraint(equalTo: baseWhiteView.centerXAnchor).isActive = true
        editButton.centerYAnchor.constraint(equalTo: baseWhiteView.centerYAnchor).isActive = true
        editButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 6).isActive = true
        editButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 6).isActive = true
        editButton.layer.cornerRadius = getSizeScreen().width / 12
        
        editLabel.topAnchor.constraint(equalTo: editButton.bottomAnchor, constant: getSizeScreen().height / 50).isActive  = true
        editLabel.centerXAnchor.constraint(equalTo: baseWhiteView.centerXAnchor).isActive = true
        editLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        editLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4.5).isActive = true
        editLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 50)
        
        problemCityButton.topAnchor.constraint(equalTo: baseWhiteView.topAnchor, constant: getSizeScreen().height / 7.5).isActive = true
        problemCityButton.rightAnchor.constraint(equalTo: baseWhiteView.rightAnchor, constant: -getSizeScreen().height / 20).isActive = true
        problemCityButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 6).isActive = true
        problemCityButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 6).isActive = true
        problemCityButton.layer.cornerRadius = getSizeScreen().width / 12
        
        problemCityLabel.topAnchor.constraint(equalTo: problemCityButton.bottomAnchor, constant: getSizeScreen().height / 50).isActive  = true
        problemCityLabel.centerXAnchor.constraint(equalTo: problemCityButton.centerXAnchor).isActive = true
        problemCityLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 30).isActive = true
        problemCityLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
        problemCityLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 50)
        
        newsButton.topAnchor.constraint(equalTo: baseWhiteView.topAnchor, constant: getSizeScreen().height / 7.5).isActive = true
        newsButton.leftAnchor.constraint(equalTo: baseWhiteView.leftAnchor, constant: getSizeScreen().height / 20).isActive = true
        newsButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 6).isActive = true
        newsButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 6).isActive = true
        newsButton.layer.cornerRadius = getSizeScreen().width / 12
        
        newsLabel.topAnchor.constraint(equalTo: newsButton.bottomAnchor, constant: getSizeScreen().height / 50).isActive  = true
        newsLabel.centerXAnchor.constraint(equalTo: newsButton.centerXAnchor).isActive = true
        newsLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 30).isActive = true
        newsLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
        newsLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 50)
        
        aboutUsButton.bottomAnchor.constraint(equalTo: baseWhiteView.bottomAnchor, constant: -getSizeScreen().height / 55).isActive = true
        aboutUsButton.rightAnchor.constraint(equalTo: baseWhiteView.rightAnchor, constant: -getSizeScreen().height / 20).isActive = true
        
        SupportButton.bottomAnchor.constraint(equalTo: baseWhiteView.bottomAnchor, constant: -getSizeScreen().height / 55).isActive = true
        SupportButton.leftAnchor.constraint(equalTo: baseWhiteView.leftAnchor, constant: getSizeScreen().height / 20).isActive = true
        
        supportButton.bottomAnchor.constraint(equalTo: baseWhiteView.bottomAnchor, constant: -getSizeScreen().height / 7.5).isActive = true
        supportButton.leftAnchor.constraint(equalTo: baseWhiteView.leftAnchor, constant: getSizeScreen().height / 20).isActive = true
        supportButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 6).isActive = true
        supportButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 6).isActive = true
        supportButton.layer.cornerRadius = getSizeScreen().width / 12
        
        budgNumberItemSaveToCoreDataLabel.centerXAnchor.constraint(equalTo: supportButton.centerXAnchor, constant: getSizeScreen().width / 9).isActive = true
        budgNumberItemSaveToCoreDataLabel.centerYAnchor.constraint(equalTo: supportButton.centerYAnchor, constant: -getSizeScreen().width / 10).isActive = true
        budgNumberItemSaveToCoreDataLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 10).isActive = true
        budgNumberItemSaveToCoreDataLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 10).isActive = true
        budgNumberItemSaveToCoreDataLabel.layer.cornerRadius = getSizeScreen().width / 20
        budgNumberItemSaveToCoreDataLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        supportLabel.topAnchor.constraint(equalTo: supportButton.bottomAnchor, constant: getSizeScreen().height / 50).isActive  = true
        supportLabel.centerXAnchor.constraint(equalTo: supportButton.centerXAnchor).isActive = true
        supportLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 30).isActive = true
        supportLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
        supportLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 50)
        
        observerButton.bottomAnchor.constraint(equalTo: baseWhiteView.bottomAnchor, constant: -getSizeScreen().height / 7.5).isActive = true
        observerButton.rightAnchor.constraint(equalTo: baseWhiteView.rightAnchor, constant: -getSizeScreen().height / 20).isActive = true
        observerButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 6).isActive = true
        observerButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 6).isActive = true
        observerButton.layer.cornerRadius = getSizeScreen().width / 12
        
        observerLabel.topAnchor.constraint(equalTo: observerButton.bottomAnchor, constant: getSizeScreen().height / 50).isActive  = true
        observerLabel.centerXAnchor.constraint(equalTo: observerButton.centerXAnchor).isActive = true
        observerLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 30).isActive = true
        observerLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
        observerLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 50)
    }
    
    
    
    var budgeNumItemSaveToCoreData = 0
    
    func showNumberOfItemsSaveToCoreData() {
        
        if budgeNumItemSaveToCoreData != 0 {
            budgNumberItemSaveToCoreDataLabel.isHidden = false
        }
        else {
            budgNumberItemSaveToCoreDataLabel.isHidden = true
        }
        
        let persianNum = convertEngNumToPersianNum(num: String(budgeNumItemSaveToCoreData))
        budgNumberItemSaveToCoreDataLabel.text = persianNum
        
    }
    
    @objc fileprivate func handelProblemCity() {
        
//        let problemCnt = cityProblemsController()
        userDefult.setValue(false, forKey: "offlineMode")
        let CPController = cityProblemsController()
        CPController.home = self
        let root = UINavigationController(rootViewController: CPController)
        self.present(root, animated: true, completion: nil)
        
        
    }
    
    @objc fileprivate func handelNews() {
        let NController = NewsController()
        let root = UINavigationController(rootViewController: NController)
        self.present(root, animated: true, completion: nil)
    }
    
    @objc fileprivate func handelFollowUpRequest() {
//        let root = UINavigationController(rootViewController: FURController)
        let FURController = FollowUpTheRequestController()
        FURController.home = self
        present(FURController, animated: true, completion: nil)
    }
    
    @objc fileprivate func upgradeToObserve() {
        
        let upgradtoobsorve = upgradeToObserverController()
        upgradtoobsorve.homeVC = self
        let root = UINavigationController(rootViewController: upgradtoobsorve)
        self.present(root, animated: true, completion: nil)
    }
    
    
    
    func changeUpgradeToObserveOps() {
        
        observerButton.isEnabled = false
        observerLabel.text = "درحال بررسی"
        observerLabel.textColor = .lightGray
        UserDefaults.standard.setValue(true, forKey: "isUpdateToObserve")
        
    }
    
    @objc fileprivate func handelTicket() {
        
        let ticketController = TicketController()
        let root = UINavigationController(rootViewController: ticketController)
        self.present(root, animated: true, completion: nil)
        
    }
    
    @objc fileprivate func handelAboutUs() {
        
        let aboutUs = aboutUsController()
        let root = UINavigationController(rootViewController: aboutUs)
        self.present(root, animated: true, completion: nil)
        
    }
    
    @objc fileprivate func handelSetting() {
        
        let setting = settingController()
        let root = UINavigationController(rootViewController: setting)
        self.present(root, animated: true, completion: nil)
        
    }
    


}

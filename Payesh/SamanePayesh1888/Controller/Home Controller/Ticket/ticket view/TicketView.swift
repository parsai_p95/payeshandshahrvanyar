//
//  TicketView.swift
//  Zistagram
//
//  Created by Parsai on 9/4/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Instructions
import CDAlertView

class TicketView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, CoachMarksControllerDataSource, CoachMarksControllerDelegate {
    
    

    

    struct ticketMessage {
        
        var text: String?
        var date: String?
        var isSender: Bool?
        
    }
    
    var ticketCnt: TicketController?
    
    var height: CGFloat?
    var width: CGFloat?
    
    let cellId = "cellId"
    
    lazy var CV: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor(r: 217, g: 220, b: 221)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    var messages: [ticketMessage] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let screenSize = UIScreen.main.bounds
        width = screenSize.width
        height = screenSize.height
        
        backgroundColor = UIColor(r: 217, g: 220, b: 221)
        setupCollcetionView()
        setupViews()
        setupHelpApp()
    }
    
    let coachMarksController = CoachMarksController()
    let pointOfInterest = UIView()
    let pointOfInterest2 = UIView()
    let pointOfInterest3 = UIView()
    
    let textHelpArray = ["ابتدا باید عنوان نظر خود را در این قسمت وارد کنید، سپس دکمه را سمت راست را لمس کنید.", " در این قسمت توضیحات مربوط به عنوان درج شده در مرحله قبل را وارد کنید، سپس دکمه ارسال را لمس کنید.", "برای مشاهده جواب پیام خود، پاسخ هر پیام در زیر آن قرار دارد"]
    
    fileprivate func setupHelpApp() {
        
        coachMarksController.dataSource = self
        coachMarksController.overlay.color = UIColor(white: 0, alpha: 0.7)
        pointOfInterest.frame = CGRect(x: 0, y: height! - height! / 10, width: width!, height: height! / 12 + 10)
        pointOfInterest2.frame = CGRect(x: 0, y: height! - height! / 10, width: width!, height: height! / 12 + 10)
        pointOfInterest3.frame = CGRect(x: width! / 2, y: height! - height! / 5, width: 0, height: 0)
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkAt index: Int) -> CoachMark {
        
        let array = [pointOfInterest, pointOfInterest2, pointOfInterest3]
        
        return coachMarksController.helper.makeCoachMark(for: array[index])
    }
    
    func numberOfCoachMarks(for coachMarksController: CoachMarksController) -> Int {
        
        return 3
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkViewsAt index: Int, madeFrom coachMark: CoachMark) -> (bodyView: CoachMarkBodyView, arrowView: CoachMarkArrowView?) {
        let coachViews = coachMarksController.helper.makeDefaultCoachViews(withArrow: true, arrowOrientation: coachMark.arrowOrientation)
        
        
        coachViews.bodyView.hintLabel.textAlignment = .center
        coachViews.bodyView.hintLabel.text = textHelpArray[index]
        if index == 2 {
            coachViews.bodyView.nextLabel.text = "در آخر"
        }
        else {
            coachViews.bodyView.nextLabel.text = "ادامه"
        }
        
        
        return (bodyView: coachViews.bodyView, arrowView: coachViews.arrowView)
    }
    
    
    
    var bottomCVConstraint: NSLayoutConstraint?
    
    fileprivate func setupCollcetionView() {
        
        CV.register(TicketCell.self, forCellWithReuseIdentifier: cellId)
        addSubview(CV)
        addConstraintWithFormat(format: "H:|[v0]|", views: CV)
        addConstraintWithFormat(format: "V:|[v0]", views: CV)
        
        bottomCVConstraint = NSLayoutConstraint(item: CV, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        addConstraint(bottomCVConstraint!)
        
        CV.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 20, right: 0)
        CV.scrollIndicatorInsets = UIEdgeInsets(top: 10, left: 0, bottom: 20, right: 0)
        
//        CV.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: height! / 10, right: 0)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        handelSendMessage()
        return true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let messageInputContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    lazy var inputTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "عنوان ..."
        textField.delegate = self
        textField.textAlignment = .right
        
        return textField
    }()
    
    let sendButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "next")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .white
        button.backgroundColor = UIColor(r: 87, g: 155, b: 159)
        button.layer.masksToBounds = true
        return button
    }()
    
    var BottomButtonViewConstraint: NSLayoutConstraint?
    var bottomConstant: CGFloat = 0
    
    fileprivate func setupViews() {
        
        
        
        let bottomView = UIView()
        bottomView.backgroundColor = .white
        addSubview(bottomView)
        addConstraintWithFormat(format: "H:|[v0]|", views: bottomView)
        addConstraintWithFormat(format: "V:[v0(50)]|", views: bottomView)
        
        addSubview(messageInputContainerView)
        addConstraintWithFormat(format: "H:|[v0]|", views: messageInputContainerView)
        addConstraintWithFormat(format: "V:[v0(\(height! / 12))]", views: messageInputContainerView)
 
        let guide = safeAreaLayoutGuide
        
        BottomButtonViewConstraint = NSLayoutConstraint(item: messageInputContainerView, attribute: .bottom, relatedBy: .equal, toItem: guide, attribute: .bottom, multiplier: 1, constant: 0)
        addConstraint(BottomButtonViewConstraint!)
        
        
        
        setupInputComponents()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handelKeyBoardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handelKeyBoardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        getTickets()
    }
    

    
    var bottomPadding: CGFloat?
    
    @objc func handelKeyBoardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let keyBoardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
            let keyboardRectangle = keyBoardFrame.cgRectValue
            
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            
            bottomPadding = 0
            
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                bottomPadding = window?.safeAreaInsets.bottom
            }
            
            BottomButtonViewConstraint?.constant = isKeyboardShowing ? -keyboardRectangle.height + bottomPadding!: 0
            bottomCVConstraint?.constant = isKeyboardShowing ? -keyboardRectangle.height + bottomPadding!: 0
            
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                
                self.layoutIfNeeded()
                
            }) { (completion) in
                
                if isKeyboardShowing {
                    let indexPath = NSIndexPath(item: 0, section: 0)
                    self.CV.scrollToItem(at: indexPath as IndexPath, at: .top, animated: true)
                }
                
            }
        }
    }
    
    fileprivate func setupInputComponents() {
        
        let topBorderView = UIView()
        topBorderView.backgroundColor = .lightGray

        messageInputContainerView.addSubview(inputTextField)
        messageInputContainerView.addSubview(sendButton)
        messageInputContainerView.addSubview(topBorderView)
        inputTextField.translatesAutoresizingMaskIntoConstraints = false
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        
        sendButton.centerYAnchor.constraint(equalTo: messageInputContainerView.centerYAnchor).isActive = true
        sendButton.rightAnchor.constraint(equalTo: messageInputContainerView.rightAnchor, constant: -10).isActive = true
        sendButton.heightAnchor.constraint(equalToConstant: height! / 16).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: height! / 16).isActive = true
        
        inputTextField.centerYAnchor.constraint(equalTo: messageInputContainerView.centerYAnchor).isActive = true
        inputTextField.rightAnchor.constraint(equalTo: sendButton.leftAnchor, constant: -10).isActive = true
        inputTextField.leftAnchor.constraint(equalTo: messageInputContainerView.leftAnchor, constant: 10).isActive = true
        inputTextField.heightAnchor.constraint(equalToConstant: height! / 16).isActive = true
        
        sendButton.addTarget(self, action: #selector(handelSendMessage), for: .touchUpInside)
        sendButton.layer.cornerRadius = height! / 32
        
        messageInputContainerView.addConstraintWithFormat(format: "H:|[v0]|", views: topBorderView)
        messageInputContainerView.addConstraintWithFormat(format: "V:|[v0(1)]", views: topBorderView)
        
    }
    
    var title = ""
    var Description = ""
    var nextStep = false
    
    @objc func handelSendMessage() {
        
        if let userid = userDefult.value(forKey: "userid") as? String {
            UserId = userid
        }
        
        if inputTextField.text != "" && !nextStep {
            
            nextStep = true
            title = inputTextField.text!
            print(title)
            inputTextField.placeholder = "توضیحات ..."
            sendButton.setImage(UIImage(named: "sendMessageFill")?.withRenderingMode(.alwaysTemplate), for: .normal)
            inputTextField.text = ""

        }
        else if inputTextField.text != "" && nextStep{
            nextStep = false
            Description = inputTextField.text!
            print(Description)
            inputTextField.text = ""
            inputTextField.placeholder = "عنوان ..."
            sendButton.setImage(UIImage(named: "next")?.withRenderingMode(.alwaysTemplate), for: .normal)
            inputTextField.endEditing(true)
            sendTicket()
        }
        
    }
    let userDefult = UserDefaults()
    var UserId = ""
    func sendTicket() {
        
        if let userid = userDefult.value(forKey: "userid") as? String {
            UserId = userid
        }
        
        let userInfoURL = "http://212.33.202.139:8587/app/ticket.php?userid=\(UserId)&subject=\(title)&msg=\(Description)"
        let getUrl = userInfoURL.data(using: String.Encoding.ascii, allowLossyConversion: true)
        let urlString = String(data: getUrl!, encoding: String.Encoding.ascii)
        guard let url = URL(string: urlString!) else{return}
        Alamofire.request(url, method: .get).responseJSON { (response) in
            
            if let data = response.data {
                let json = JSON(data)
                let array = json["array"].int
                if array == 200 || array == 215 {
                    self.getTickets()

                }
            }
        }
        
    }
    
    func getTickets() {
        
        if let userid = userDefult.value(forKey: "userid") as? String {
            UserId = userid
        }
        
        let userInfoURL = "http://212.33.202.139:8587/app/loadticket.php?userid=\(UserId)"
        let getUrl = userInfoURL.data(using: String.Encoding.ascii, allowLossyConversion: true)
        let urlString = String(data: getUrl!, encoding: String.Encoding.ascii)
        guard let url = URL(string: urlString!) else{return}
        
        Alamofire.request(url, method: .get).responseJSON { (response) in
            
            if let data = response.data {
                let json = JSON(data)
                let array = json["array"]
                self.messages.removeAll()
                for key in array {
                    let userInfoJson = key.1
                    let msg = userInfoJson["msg"].string
                    let subject = userInfoJson["subject"].string
                    let answer = userInfoJson["resp"].string
                    
//                    let formattedString = NSMutableAttributedString()
//                    formattedString.bold(subject!).normal("\n\n").normal(msg!)

                    let message = "عنوان:    \n" + subject! + "\n\n" + "توضیحات:    \n" + msg!
                    
                    
                    if answer != "" {
                        let myMessage = ticketMessage(text: message, date: "", isSender: true)
                        self.messages.append(myMessage)
                        let answerMessage = ticketMessage(text: answer, date: "", isSender: false)
                        self.messages.append(answerMessage)
                    }
                    else {
                        let myMessage = ticketMessage(text: message, date: "", isSender: true)
                        self.messages.append(myMessage)
                        let answerMessage = ticketMessage(text: "هیچ پاسخی ارسال نشده است", date: "", isSender: false)
                        self.messages.append(answerMessage)
                    }
                    
                }
                
                DispatchQueue.main.async {
                    
                    
                    self.CV.reloadData()
                }
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TicketCell
        cell.messageTextView.text = messages[indexPath.item].text
        cell.timeLabel.text = messages[indexPath.item].date
        cell.rightConstraint?.constant = 0
        
        if let messageText = messages[indexPath.item].text {
            
            let messageSender = messages[indexPath.item].isSender ??  false
            
            let size = CGSize(width: width! / 2.5, height: 1000)
            let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: option, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18)], context: nil)
            
            if !messageSender  {
                
                cell.messageTextView.frame = CGRect(x: height! / 18 + 28, y: 0, width: estimatedFrame.width + width! / 3.5, height: estimatedFrame.height + 30)
                cell.textBubbleView.frame = CGRect(x: height! / 18 + 25, y: -6, width: estimatedFrame.width + width! / 3.5 + 21, height: estimatedFrame.height + 36)
                cell.timeLabel.frame = CGRect(x: width! / 4 - 5 , y: estimatedFrame.height + 5 , width: 100, height: 20)
                cell.profileImageView.isHidden = false
                cell.bubbleImageView.image = TicketCell.GreenBubbleImage
                cell.bubbleImageView.tintColor = .white
                cell.messageTextView.textColor = .black
                cell.timeLabel.textColor = .lightGray

            }
            else {
                
                cell.messageTextView.frame = CGRect(x: frame.width - estimatedFrame.width - 50 - 16 - 16, y: 0, width: estimatedFrame.width + width! / 6.5, height: estimatedFrame.height + 30)
                cell.textBubbleView.frame = CGRect(x: frame.width - estimatedFrame.width - 50 - 16 - 26, y: -4, width: estimatedFrame.width + width! / 6.5 + 26, height: estimatedFrame.height + 36)
                cell.timeLabel.frame = CGRect(x: frame.width - estimatedFrame.width - 75 , y: estimatedFrame.height + 5 , width: 100, height: 20)
                cell.profileImageView.isHidden = true
                cell.bubbleImageView.image = TicketCell.whiteBubbleImage
                cell.bubbleImageView.tintColor = UIColor(r: 87, g: 155, b: 159)
                cell.messageTextView.textColor = .white
                cell.timeLabel.textColor = .white
                cell.timeLabel.isHidden = true
                
                

            }

        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if let messageText = messages[indexPath.item].text {
            
            let size = CGSize(width: width! / 2.5, height: 1000)
            let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: option, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18)], context: nil)
            return CGSize(width: frame.width, height: estimatedFrame.height + 30)
            
        }
        
        return CGSize(width: frame.width, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 0, bottom: height! / 12 + 10, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        inputTextField.endEditing(true)
    }
    
    

}

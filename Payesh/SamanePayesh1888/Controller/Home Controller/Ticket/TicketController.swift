//
//  TicketController.swift
//  SamanePayesh1888
//
//  Created by apple on 10/9/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit
import CDAlertView

class TicketController: UIViewController {
    
    let titleNavLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 25))
        label.textColor = .white
        label.textAlignment = .center
        label.text = "سامانه پایش "
        return label
    }()
    
    lazy var ticketView: TicketView = {
        let view = TicketView()
        view.ticketCnt = self
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()
        
        view.addSubview(ticketView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: ticketView)
        view.addConstraintWithFormat(format: "V:|[v0]|", views: ticketView)
        
        if let usid = userDefult.value(forKey: "userid") as? String {
            if usid == "" {
                self.dismiss(animated: true, completion: nil)
                userDefult.setValue(true, forKey: "UnacceptUserId")
                let profile = profileController()
                let root = UINavigationController(rootViewController: profile)
                self.present(root, animated: true, completion: nil)
            }
            else {
                ticketView.getTickets()
            }
        }
        else {
            self.dismiss(animated: true, completion: nil)
            userDefult.setValue(true, forKey: "UnacceptUserId")
            let profile = profileController()
            let root = UINavigationController(rootViewController: profile)
            self.present(root, animated: true, completion: nil)
        }
        
        
        
    }
    let userDefult = UserDefaults()
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let firstStartAppMode = userDefult.value(forKey: "startHelpTicketMode") as? Bool {
            if firstStartAppMode {
                self.ticketView.coachMarksController.start(in: .window(over: self))
                userDefult.setValue(false, forKey: "startHelpTicketMode")
            }
        }
        else {
            self.ticketView.coachMarksController.start(in: .window(over: self))
            userDefult.setValue(false, forKey: "startHelpTicketMode")
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.ticketView.coachMarksController.stop()

    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    fileprivate func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.lightColor.light
        navigationController?.navigationBar.tintColor = .white
        
        let image = UIImage(named: "back")
        let cancel = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handelDismiss))
        navigationItem.setLeftBarButton(cancel, animated: true)
        
        navigationItem.titleView = titleNavLabel
        titleNavLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 50)
    }
    
    @objc func handelDismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    

}

//
//  File.swift
//  SamanePayesh1888
//
//  Created by Parsai on 9/27/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit
import  CoreData




class coreDataOperation {
    
    var myData: [String] = []
    var myImageData: [Data] = []
    
    var id: Int16 = 0
    
    let userDefult = UserDefaults()
    
    let persianCalender: GDCalendar = {
        let pc = GDCalendar()
        pc.translatesAutoresizingMaskIntoConstraints = false
        pc.backgroundColor = .white
        pc.headerBackgroundColor = UIColor(r: 44, g: 202, b: 175)
        pc.itemHighlightColor = UIColor(r: 44, g: 202, b: 175)
        pc.itemHighlightTextColor = .white
        pc.itemsFont = UIFont.systemFont(ofSize: 9)
        pc.headersFont = UIFont.boldSystemFont(ofSize: 9)
        return pc
    }()
    
    
    
    func saveDataToDB(userid: String, ProblemType: String, title: String, addrss: String, des: String, lat: String, lnt: String, images: [Data]) -> Bool {
        
        if let reqId = UserDefaults.standard.value(forKey: "reqId")  {
            id = Int16(reqId as! Int)
        }
        
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate?.persistentContainer.viewContext
        let postEntity = NSEntityDescription.entity(forEntityName: "UnsendReuset", in: managedContext!)!
        
        let user = NSManagedObject(entity: postEntity, insertInto: managedContext)
        let date = getCurrentDate()
        
        user.setValue(date, forKey: "date")
        user.setValue(id, forKey: "id")
        user.setValue(userid, forKey: "userid")
        user.setValue(ProblemType, forKey: "ProblemType")
        user.setValue(title, forKey: "title")
        user.setValue(addrss, forKey: "addrss")
        user.setValue(des, forKey: "des")
        user.setValue(lat, forKey: "lat")
        user.setValue(lnt, forKey: "lnt")
        print(images.count)
//        user.setValue(images[0], forKey: "img1")
//        user.setValue(images[1], forKey: "img2")
//        user.setValue(images[2], forKey: "img3")
        
        
        do {
            try managedContext?.save()
            id += 1
            UserDefaults.standard.set(id, forKey: "reqId")
            
            return true
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
            return false
        }
        
    }
    
    func getCurrentDate() -> String {
        
        var Day = ""
        var Month = ""
        var Year = ""
        var FullDate = ""
        
        let day = String(persianCalender.currentDate.componentsOfDate.day)
        if let number = Int(day.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
            let perNum = convertEngNumToPersianNum(num: String(number))
            Day = perNum
        }
        let year = String(persianCalender.currentDate.componentsOfDate.year)
        if let number = Int(year.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
            let perNum = convertEngNumToPersianNum(num: String(number))
            Year = perNum
        }
        
        let month = String(persianCalender.currentDate.componentsOfDate.month)
        if let number = Int(month.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
            let perNum = convertEngNumToPersianNum(num: String(number))
            Month = perNum
        }
        
        FullDate = Year + "/" + Month + "/" + Day
        return FullDate
        
    }
    
    func fetchCoreData(fieldDB: String) -> [String] {
        
        myData.removeAll()
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//
//        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UnsendReuset")
//        request.returnsObjectsAsFaults = false
//        do {
//            let result = try context.fetch(request)
////            if result != nil {
////                for data in result as! [NSManagedObject] {
////
////                    if fieldDB == "id" {
////                        let tmp = data.value(forKey: fieldDB) as! Int16
////                        let tempID = String(tmp)
////                        myData.append(tempID)
////                    }
////                    else {
////                        let tmp = data.value(forKey: fieldDB) as! String
////                        myData.append(tmp)
////                    }
////
////
////                }
////            }
//
//        } catch {
//
//            print("Failed")
//        }
        return myData
        
    }
    
    func fetchImageCoreData(numberImage: Int) -> [Data] {
        
        myImageData.removeAll()
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//
//        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UnsendReuset")
//
//        request.returnsObjectsAsFaults = false
////        do {
//////            let result = try context.fetch(request)
//////            if result != nil {
//////                for data in result as! [NSManagedObject] {
//////                    if let tmp = data.value(forKey: "img\(numberImage)")  {
//////                        myImageData.append(tmp as! Data)
//////                    }
//////
//////                }
//////            }
////
////        } catch {
////
////            print("Failed")
//        }
        
        return myImageData
        
    }
    
    func deleteOneItem(id: String) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UnsendReuset")
        fetchRequest.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let test = try context.fetch(fetchRequest)
            let objectToDelete = test[0] as! NSManagedObject
            context.delete(objectToDelete)
            
            do {
                try context.save()
                
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
            
            
        } catch {
            // Error Handling
            // ...
        }
        
        
    }
    
    func deleteAllRecors() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UnsendReuset")
        
        // Configure Fetch Request
        fetchRequest.includesPropertyValues = false
        
        do {
            let items = try context.fetch(fetchRequest) as! [NSManagedObject]
            
            for item in items {
                context.delete(item)
            }
            
            // Save Changes
            try context.save()
            
        } catch {
            // Error Handling
            // ...
        }
        
    }
    
    func convertEngNumToPersianNum(num: String) -> String {
        //let number = NSNumber(value: Int(num)!)
        
        let format = NumberFormatter()
        format.locale = Locale(identifier: "fa_IR")
        let number =   format.number(from: num)
        let faNumber = format.string(from: number!)
        return faNumber!
        
    }
    
    
}

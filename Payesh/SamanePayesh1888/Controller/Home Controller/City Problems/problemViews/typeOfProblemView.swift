//
//  typeOfProblemView.swift
//  SamanePayesh1888
//
//  Created by Parsai on 9/23/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit
import CDAlertView
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class typeOfProblemView: UIView {
    
    var CityProblem: cityProblemsController?
    var unsendReqView: unsendSelectCellWhiteView?
    
    var typeOfProblem: String?
    
    let loader: NVActivityIndicatorView = {
        let loader = NVActivityIndicatorView(frame: .zero)
        let loaderType = NVActivityIndicatorType.ballRotateChase
        loader.type = loaderType
        loader.color = .gray
        loader.padding = 5
        loader.translatesAutoresizingMaskIntoConstraints = false
        return loader
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(loader)
        loader.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loader.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        loader.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        loader.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        
        SuggestionSwitch.isOn = false
        discontentSwitch.isOn = false
        criticismSwitch.isOn = false
        appreciationSwitch.isOn = false
        setupViews()
    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.text = "تعیین نوع مشکل"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let line: UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let discontentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        return view
    }()
    
    let discontentLabel: UILabel = {
        let label = UILabel()
        label.text = "شکایت"
        label.textColor = .black
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let discontentSwitch: UISwitch = {
        
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.addTarget(self, action: #selector(typeOfProblemSwitchesDidChange(_:)), for: .valueChanged)
        
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
    let SuggestionView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let SuggestionLabel: UILabel = {
        let label = UILabel()
        label.text = "پیشنهاد"
        label.textColor = .black
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let SuggestionSwitch: UISwitch = {
        
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.addTarget(self, action: #selector(typeOfProblemSwitchesDidChange(_:)), for: .valueChanged)
        Switch.layer.setValue(2, forKey: "typeOfProblem")
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
    let criticismView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let criticismLabel: UILabel = {
        let label = UILabel()
        label.text = "انتقاد"
        label.textColor = .black
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let criticismSwitch: UISwitch = {
        
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.addTarget(self, action: #selector(typeOfProblemSwitchesDidChange(_:)), for: .valueChanged)
        Switch.layer.setValue(3, forKey: "typeOfProblem")
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
    let appreciationView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let appreciationLabel: UILabel = {
        let label = UILabel()
        label.text = "تقدیر و تشکر"
        label.textColor = .black
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let appreciationSwitch: UISwitch = {
        
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.addTarget(self, action: #selector(typeOfProblemSwitchesDidChange(_:)), for: .valueChanged)
        Switch.layer.setValue(4, forKey: "typeOfProblem")
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
    let nextLevelButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor(r: 10, g: 96, b: 238)
        btn.tintColor = .white
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 5
        btn.setTitle("مرحله بعد", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    fileprivate func setupViews() {
        addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 12).isActive = true
        titleLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 40)
        
        addSubview(line)
        line.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
        line.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        line.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        line.heightAnchor.constraint(equalToConstant: 2.5).isActive = true
        
//
//        //*********************
//        addSubview(SuggestionView)
//        SuggestionView.topAnchor.constraint(equalTo: discontentView.bottomAnchor, constant: getSizeScreen().height / 40).isActive = true
//        SuggestionView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().height / 40).isActive = true
//        SuggestionView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: getSizeScreen().height / 40).isActive = true
//        SuggestionView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 12).isActive = true
//
//        SuggestionView.addSubview(SuggestionLabel)
//        SuggestionView.addSubview(SuggestionSwitch)
//
//        SuggestionLabel.leftAnchor.constraint(equalTo: SuggestionView.leftAnchor, constant: getSizeScreen().width / 10).isActive = true
//        SuggestionLabel.centerYAnchor.constraint(equalTo: SuggestionView.centerYAnchor).isActive = true
//        SuggestionLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
//        SuggestionLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 45)
//
//        SuggestionSwitch.rightAnchor.constraint(equalTo: SuggestionView.rightAnchor, constant: -getSizeScreen().width / 10).isActive = true
//        SuggestionSwitch.centerYAnchor.constraint(equalTo: SuggestionView.centerYAnchor).isActive = true
//
//        //*********************
//        addSubview(criticismView)
//        criticismView.topAnchor.constraint(equalTo: SuggestionView.bottomAnchor, constant: getSizeScreen().height / 40).isActive = true
//        criticismView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().height / 40).isActive = true
//        criticismView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: getSizeScreen().height / 40).isActive = true
//        criticismView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 12).isActive = true
//
//        criticismView.addSubview(criticismLabel)
//        criticismView.addSubview(criticismSwitch)
//
//        criticismLabel.leftAnchor.constraint(equalTo: criticismView.leftAnchor, constant: getSizeScreen().width / 10).isActive = true
//        criticismLabel.centerYAnchor.constraint(equalTo: criticismView.centerYAnchor).isActive = true
//        criticismLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
//        criticismLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 45)
//
//        criticismSwitch.rightAnchor.constraint(equalTo: criticismView.rightAnchor, constant: -getSizeScreen().width / 10).isActive = true
//        criticismSwitch.centerYAnchor.constraint(equalTo: criticismView.centerYAnchor).isActive = true
//
//        //*********************
//        addSubview(appreciationView)
//        appreciationView.topAnchor.constraint(equalTo: criticismView.bottomAnchor, constant: getSizeScreen().height / 40).isActive = true
//        appreciationView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().height / 40).isActive = true
//        appreciationView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: getSizeScreen().height / 40).isActive = true
//        appreciationView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 12).isActive = true
//
//        appreciationView.addSubview(appreciationLabel)
//        appreciationView.addSubview(appreciationSwitch)
//
//        appreciationLabel.leftAnchor.constraint(equalTo: appreciationView.leftAnchor, constant: getSizeScreen().width / 10).isActive = true
//        appreciationLabel.centerYAnchor.constraint(equalTo: appreciationView.centerYAnchor).isActive = true
//        appreciationLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
//        appreciationLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 45)
//
//        appreciationSwitch.rightAnchor.constraint(equalTo: appreciationView.rightAnchor, constant: -getSizeScreen().width / 10).isActive = true
//        appreciationSwitch.centerYAnchor.constraint(equalTo: appreciationView.centerYAnchor).isActive = true
        getTypeOfProblem()
        //*********************
        addSubview(nextLevelButton)
        nextLevelButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true
        nextLevelButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        nextLevelButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        nextLevelButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2).isActive = true
        nextLevelButton.addTarget(self, action: #selector(handelNextLevel), for: .touchUpInside)
    }
    
    fileprivate func getTypeOfProblem() {
        loader.startAnimating()
        let userInfoURL = baseURLString + "ptype.php"
        let getUrl = userInfoURL.data(using: String.Encoding.ascii, allowLossyConversion: true)
        let urlString = String(data: getUrl!, encoding: String.Encoding.ascii)
        guard let url = URL(string: urlString!) else{return}
        var count = 1
        Alamofire.request(url, method: .get).responseJSON { (response) in
            
            if let data = response.result.value {
                let json = JSON(data).arrayValue
                for items in json {

                    if let cat = items["category"]["title"].string {
                        self.discontentLabel.text = cat
                        if let code = items["category"]["problemcode"].string {
                            self.discontentSwitch.layer.setValue(Int(code), forKey: "typeOfProblem")
                            self.setupTypeOfProblemsViews(count: CGFloat(count), cat: cat, code: code)
                        }
                    }

                    count += 1
                }
                self.loader.stopAnimating()
                
            }
            
            if let err = response.result.error {
                let error = "\(err.localizedDescription)"
                if error == "The Internet connection appears to be offline." {
                    let alert = CDAlertView(title: "", message: "اینترت گوشی خود را چک کنید", type: .warning)
                    let action = CDAlertViewAction(title: "تلاش مجدد", font: UIFont.systemFont(ofSize: 12), textColor: UIColor.purpuleColor.purple, backgroundColor: .white, handler: { (action) -> Bool in
                        self.getTypeOfProblem()
                        return true
                    })
                    alert.add(action: action)
                    alert.show()
                }
            }
        }
    }
    
    fileprivate func setupTypeOfProblemsViews(count: CGFloat, cat: String, code: String) {
        
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = cat
        
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.addTarget(self, action: #selector(typeOfProblemSwitchesDidChange(_:)), for: .valueChanged)
        Switch.layer.setValue(Int(code), forKey: "typeOfProblem")
        Switch.isOn = false
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(view)
        view.frame = CGRect(x: 10, y: count * (self.getSizeScreen().height / 12 + 10), width: self.getSizeScreen().width - self.getSizeScreen().height / 20, height: self.getSizeScreen().height / 12)
        
        view.addSubview(label)
        view.addSubview(Switch)
        
        label.leftAnchor.constraint(equalTo: view.leftAnchor, constant: self.getSizeScreen().width / 10).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        label.widthAnchor.constraint(equalToConstant: self.getSizeScreen().width / 3).isActive = true
        label.font = UIFont.boldSystemFont(ofSize: self.getSizeScreen().height / 45)
        
        Switch.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -self.getSizeScreen().width / 10).isActive = true
        Switch.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    var editReqViewMode = false
    
    @objc fileprivate func handelNextLevel() {
        
//        if appreciationSwitch.isOn || criticismSwitch.isOn || SuggestionSwitch.isOn || discontentSwitch.isOn {
        if selectCatMode {
            print(123)
            if editReqViewMode {
                print(1234)
                editReqViewMode = false
                unsendReqView?.showNextLevelOfTypeOfProblemView()
            }
            CityProblem?.showNextLevelOfTypeOfProblemView()
        }
        else {
            
            let alert = CDAlertView(title: "", message: "موضوع مشکل را تعیین نکرده اید", type: .warning)
            let action = CDAlertViewAction(title: "فهمیدم")
            alert.add(action: action)
            alert.show()
        }
        
        
    }
    
    var selectCatMode = false
    
    @objc func typeOfProblemSwitchesDidChange(_ sender: UISwitch!) {
        
        let Index = (sender.layer.value(forKey: "typeOfProblem")) as! Int
        
        if sender.isOn && Index == 1 {
            print(Index)
            appreciationSwitch.isOn = false
            criticismSwitch.isOn = false
            SuggestionSwitch.isOn = false
            CityProblem?.problemType = "\(Index)"
            unsendReqView?.problemType = "\(Index)"
            selectCatMode = true
        }
        else if sender.isOn && Index == 2 {
            print(Index)
            appreciationSwitch.isOn = false
            criticismSwitch.isOn = false
            discontentSwitch.isOn = false
            CityProblem?.problemType = "\(Index)"
            unsendReqView?.problemType = "\(Index)"
            selectCatMode = true
        }
        else if sender.isOn && Index == 3 {
            print(Index)
            appreciationSwitch.isOn = false
            discontentSwitch.isOn = false
            SuggestionSwitch.isOn = false
            CityProblem?.problemType = "\(Index)"
            unsendReqView?.problemType = "\(Index)"
            selectCatMode = true
        }
        else if sender.isOn && Index == 4 {
            print(Index)
            selectCatMode = true
            discontentSwitch.isOn = false
            criticismSwitch.isOn = false
            SuggestionSwitch.isOn = false
            CityProblem?.problemType = "\(Index)"
            unsendReqView?.problemType = "\(Index)"
        }
        
    }
    
}

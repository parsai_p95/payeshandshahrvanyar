//
//  typeOfbroblem2TableViewCell.swift
//  SamanePayesh1888
//
//  Created by apple on 12/18/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit

class typeOfbroblem2TableViewCell: UITableViewCell {
    
    let ContainerVIew: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        return view
    }()
    
//    let Switch: UISwitch = {
//        let Switch = UISwitch()
//        Switch.setOn(false, animated: true)
////        Switch.addTarget(self, action: #selector(typeOfProblemSwitchesDidChange(_:)), for: .valueChanged)
//        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
//        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
//        Switch.translatesAutoresizingMaskIntoConstraints = false
//        return Switch
//    }()
    
//    let ImageView: UIImageView = {
//        let iv = UIImageView(image: UIImage(named: "drop")?.withRenderingMode(.alwaysTemplate))
//        iv.translatesAutoresizingMaskIntoConstraints = false
//        iv.contentMode = .scaleAspectFill
//        iv.tintColor = .black
//        return iv
//    }()
    
    let dropButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.tintColor = .black
        btn.setImage(UIImage(named: "drop")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let dropFullButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .clear
        return btn
    }()
    
    let Label: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .center
//        label.font = UIFont.systemFont(ofSize: getSizeScreen().height / 60)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    fileprivate func setupViews() {
        
        addSubview(ContainerVIew)
        addConstraintWithFormat(format: "H:|-10-[v0]-10-|", views: ContainerVIew)
        addConstraintWithFormat(format: "V:|-10-[v0]-10-|", views: ContainerVIew)
        
        ContainerVIew.addSubview(Label)
        ContainerVIew.addSubview(dropButton)
        ContainerVIew.addSubview(dropFullButton)
        
        ContainerVIew.addConstraintWithFormat(format: "H:|[v0]|", views: dropFullButton)
        ContainerVIew.addConstraintWithFormat(format: "V:|[v0]|", views: dropFullButton)
        
        dropButton.centerYAnchor.constraint(equalTo: ContainerVIew.centerYAnchor).isActive = true
        dropButton.leftAnchor.constraint(equalTo: ContainerVIew.leftAnchor, constant: getSizeScreen().width / 10).isActive = true
        dropButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 25).isActive = true
        dropButton.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 25).isActive = true
        
        Label.centerYAnchor.constraint(equalTo: ContainerVIew.centerYAnchor).isActive = true
        Label.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 12).isActive = true
        Label.rightAnchor.constraint(equalTo: ContainerVIew.rightAnchor, constant: -getSizeScreen().width / 15).isActive = true
        Label.leftAnchor.constraint(equalTo: dropButton.rightAnchor, constant: getSizeScreen().width / 15).isActive = true
        Label.font = UIFont.systemFont(ofSize: getSizeScreen().height / 65)
    }

}


class typeOfbroblem2SelectTableViewCell: UITableViewCell {
    
    let ContainerVIew: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        return view
    }()
    
    let Switch: UISwitch = {
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
    let Label: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    fileprivate func setupViews() {
        
        addSubview(ContainerVIew)
        addConstraintWithFormat(format: "H:|-10-[v0]-10-|", views: ContainerVIew)
        addConstraintWithFormat(format: "V:|-10-[v0]-10-|", views: ContainerVIew)
        
        ContainerVIew.addSubview(Label)
        ContainerVIew.addSubview(Switch)
        
        Switch.centerYAnchor.constraint(equalTo: ContainerVIew.centerYAnchor).isActive = true
        Switch.rightAnchor.constraint(equalTo: ContainerVIew.rightAnchor, constant: -getSizeScreen().width / 10).isActive = true

        
        Label.centerYAnchor.constraint(equalTo: ContainerVIew.centerYAnchor).isActive = true
        Label.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 12).isActive = true
        Label.rightAnchor.constraint(equalTo: Switch.leftAnchor, constant: -getSizeScreen().width / 15).isActive = true
        Label.leftAnchor.constraint(equalTo: ContainerVIew.leftAnchor).isActive = true
        Label.font = UIFont.systemFont(ofSize: getSizeScreen().height / 65)
        
    }
}

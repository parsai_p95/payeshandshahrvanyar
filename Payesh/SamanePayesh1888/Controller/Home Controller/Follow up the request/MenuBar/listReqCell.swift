//
//  listReqCell.swift
//  SamanePayesh1888
//
//  Created by Parsai on 9/27/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit

class listReqCell: baseCell {
    
    
    
    let baseView: UIView = {
        let view = UIView()
        return view
    }()
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(baseView)
        backgroundColor = .white
        addConstraintWithFormat(format: "H:|[v0]|", views: baseView)
        addConstraintWithFormat(format: "V:|[v0]|", views: baseView)
        
    }
    
}

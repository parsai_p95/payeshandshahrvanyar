//
//  selectSenReqView.swift
//  SamanePayesh1888
//
//  Created by apple on 10/8/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CDAlertView

class requestSelectItem: NSObject {
    
    var trackingCode: [String] = []
    var trackingOrgnization: [String] = []
    var dateOfgetToOrgnization: [String] = []
    var answer: [String] = []
    
}

class selectSenReqView: UIView {
    
    var requsrtView: sentRequestView?
    
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.textAlignment = .right
        lbl.text = "جزئیات درخواست"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let trackingCodeLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .darkGray
        lbl.textAlignment = .right
        lbl.text = "کد پیگیری : "
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let fullTrackingCodeLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .gray
        lbl.textAlignment = .right
        lbl.text = "۵۰۰"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let trackingOrgnizationLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .darkGray
        lbl.textAlignment = .right
        lbl.text = "واحد پیگیری : "
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let fullTrackingOrgnizationLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .gray
        lbl.textAlignment = .right
        lbl.text = "بازرسی"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let dateOfgetToOrgnizationLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .darkGray
        lbl.textAlignment = .right
        lbl.text = "تاریخ تحویل به واحد فعلی : "
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let fullDateOfgetToOrgnizationLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .gray
        lbl.textAlignment = .right
        lbl.text = "۱۳۹۶/۰۷/۲۶"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let answerLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .darkGray
        lbl.textAlignment = .right
        lbl.text = "پاسخ مسئول : "
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let answerTextView: UITextView = {
        let tv = UITextView()
        tv.textColor = .gray
        tv.textAlignment = .right
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.isEditable = false
        return tv
    }()
    
    let cancelButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.tintColor = UIColor.blueColor.blue
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("فهمیدم", for: .normal)
        return btn
    }()
    
    let editReqButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.tintColor = UIColor.blueColor.blue
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("تغییر درخواست", for: .normal)
        return btn
    }()
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    func convertPersianNumToEngNum(num: String) -> String {
        //let number = NSNumber(value: Int(num)!)
        
        let format = NumberFormatter()
        format.locale = Locale(identifier: "EN")
        let number =   format.number(from: num)
        let faNumber = format.string(from: number!)
        return faNumber!
        
    }
    
    let userDefult = UserDefaults()
    var index = 0

    override init(frame: CGRect) {
        super.init(frame: frame)

        let gesture = UITapGestureRecognizer(target: self, action: #selector(handelDismissKeyboard))
        blackView.addGestureRecognizer(gesture)
        
        
        backgroundColor = .white
        setupViews()
    }
    
    
    @objc fileprivate func handelDismissKeyboard() {
        blackView.endEditing(true)
        handelBlackViewDismiss()
    }
    
    func getSelectIndex() {
        index = selectCellRequsetView
    }
    
    var title = ""
    var Description = ""
    var ProblemId = ""
    var userid = ""
    var owner = ""
    
    func showDetailRequest(trackingCode: [String], trackingOrgnize: [String], date: [String], answer: [String], issue: [String], Des: [String]) {
        
        fullTrackingCodeLabel.text = ""
        fullTrackingOrgnizationLabel.text = ""
        fullDateOfgetToOrgnizationLabel.text = ""
        answerTextView.text = ""
        
        fullTrackingCodeLabel.text = convertPersianNumToEngNum(num: trackingCode[index])
        fullTrackingOrgnizationLabel.text = trackingOrgnize[index]
        fullDateOfgetToOrgnizationLabel.text = filterDateAndChangeEngToPersian(dateString: date[index]) //date[index]
        title = issue[index]
        Description = Des[index]
        ProblemId = trackingCode[index]
        owner = trackingOrgnize[index]
        
        DispatchQueue.main.async {
            
            self.issuerRequsestTextField.text = self.filterString(str: self.Description, part: 0)
            self.descriptionTextField.text = self.filterString(str: self.Description, part: 1)
            
        }
        
        if answer[index] == "" {
            answerTextView.text = "هیچ پیامی دریافت نشده است"
        }
        else {
            answerTextView.text = answer[index]
        }
        
        
    }
    
    func filterString(str: String, part: Int) -> String {
        
        let delimiter = "-"
        var token = str.components(separatedBy: delimiter)
        let zeroToken = token[part]
        
        return zeroToken
    }
    
    func filterDateAndChangeEngToPersian(dateString: String) -> String {
        var i = 0
        let delimiter = " "
        var token = dateString.components(separatedBy: delimiter)
        let zeroToken = token[0]
        let okChar = Set("1234567890")
        let orgDate = zeroToken.filter {okChar.contains($0) }
        let perOrgDate =  convertEngNumToPersianNum(num: orgDate)
        var temp = ""
        for num in perOrgDate {
            i += 1
            temp = temp + String(num)
            if i == 4 {
                temp = temp + "/"
                
            }
            if i == 6 {
                temp = temp + "/"
                
            }
        }
        
        return temp
    }
    
    func filterDateAndChangePersianToEng(dateString: String) -> String {
        var i = 0
        let delimiter = " "
        var token = dateString.components(separatedBy: delimiter)
        let zeroToken = token[0]
        let okChar = Set("۱۲۳۴۵۶۷۸۹۰")
        let orgDate = zeroToken.filter {okChar.contains($0) }
        let perOrgDate =  convertPersianNumToEngNum(num: orgDate)
        var temp = ""
        for num in perOrgDate {
            i += 1
            temp = temp + String(num)
            if i == 4 {
                temp = temp + "/"
                
            }
            if i == 5 {
                temp = temp + "/"
                
            }
        }
        
        return temp
    }
    
    func convertEngNumToPersianNum(num: String) -> String {
        //let number = NSNumber(value: Int(num)!)
        
        let format = NumberFormatter()
        format.locale = Locale(identifier: "fa_IR")
        if let number =  format.number(from: num) {
            let faNumber = format.string(from: number)
            return faNumber!
        }
        else {
            return " "
        }
        
    }
    
    
    fileprivate func setupViews() {
        addSubview(titleLabel)
        addSubview(trackingCodeLabel)
        addSubview(fullTrackingCodeLabel)
        addSubview(trackingOrgnizationLabel)
        addSubview(fullTrackingOrgnizationLabel)
        addSubview(dateOfgetToOrgnizationLabel)
        addSubview(fullDateOfgetToOrgnizationLabel)
        addSubview(answerLabel)
        addSubview(answerTextView)
        addSubview(cancelButton)
        addSubview(editReqButton)
        
        titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 15).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: getSizeScreen().height / 40).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 17).isActive = true
        titleLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 45)
        
        trackingCodeLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 10).isActive = true
        trackingCodeLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
        trackingCodeLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 17).isActive = true
        trackingCodeLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        trackingCodeLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        fullTrackingCodeLabel.rightAnchor.constraint(equalTo: trackingCodeLabel.leftAnchor, constant: -getSizeScreen().width / 20).isActive = true
        fullTrackingCodeLabel.centerYAnchor.constraint(equalTo: trackingCodeLabel.centerYAnchor).isActive = true
        fullTrackingCodeLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 17).isActive = true
        fullTrackingCodeLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        fullTrackingCodeLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        trackingOrgnizationLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 10).isActive = true
        trackingOrgnizationLabel.topAnchor.constraint(equalTo: trackingCodeLabel.bottomAnchor).isActive = true
        trackingOrgnizationLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 17).isActive = true
        trackingOrgnizationLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        trackingOrgnizationLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        fullTrackingOrgnizationLabel.rightAnchor.constraint(equalTo: trackingOrgnizationLabel.leftAnchor, constant: -getSizeScreen().width / 20).isActive = true
        fullTrackingOrgnizationLabel.centerYAnchor.constraint(equalTo: trackingOrgnizationLabel.centerYAnchor).isActive = true
        fullTrackingOrgnizationLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 17).isActive = true
        fullTrackingOrgnizationLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        fullTrackingOrgnizationLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        dateOfgetToOrgnizationLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 10).isActive = true
        dateOfgetToOrgnizationLabel.topAnchor.constraint(equalTo: trackingOrgnizationLabel.bottomAnchor).isActive = true
        dateOfgetToOrgnizationLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 17).isActive = true
        dateOfgetToOrgnizationLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2).isActive = true
        dateOfgetToOrgnizationLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        fullDateOfgetToOrgnizationLabel.rightAnchor.constraint(equalTo: dateOfgetToOrgnizationLabel.leftAnchor, constant: -getSizeScreen().width / 20).isActive = true
        fullDateOfgetToOrgnizationLabel.centerYAnchor.constraint(equalTo: dateOfgetToOrgnizationLabel.centerYAnchor).isActive = true
        fullDateOfgetToOrgnizationLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 17).isActive = true
        fullDateOfgetToOrgnizationLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 4).isActive = true
        fullDateOfgetToOrgnizationLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        answerLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 10).isActive = true
        answerLabel.topAnchor.constraint(equalTo: dateOfgetToOrgnizationLabel.bottomAnchor).isActive = true
        answerLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        answerLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3.5).isActive = true
        answerLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        answerTextView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 8).isActive = true
        answerTextView.topAnchor.constraint(equalTo: answerLabel.bottomAnchor).isActive = true
        answerTextView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 8).isActive = true
        answerTextView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: getSizeScreen().width / 12).isActive = true
        answerTextView.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        
        cancelButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -getSizeScreen().width / 20).isActive = true
        cancelButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -getSizeScreen().width / 20).isActive = true
        cancelButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        cancelButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
        cancelButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        cancelButton.addTarget(self, action: #selector(handelDismiss), for: .touchUpInside)
        
        editReqButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: getSizeScreen().width / 20).isActive = true
        editReqButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -getSizeScreen().width / 20).isActive = true
        editReqButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        editReqButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
        editReqButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 50)
        editReqButton.addTarget(self, action: #selector(handelEditRequest), for: .touchUpInside)
        
        
        
    }
    
    @objc fileprivate func handelDismiss() {
        requsrtView?.handelDismissBlackView()
    }
    
    let blackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.65)
        return view
    }()
    
    let whiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        return view
    }()
    
    let titleIssueLabel: UILabel = {
        let label = UILabel()
        label.text = "عنوان درخواست"
        label.textAlignment = .center
        label.textColor = UIColor.blueColor.blue
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "افزودن توضیحات"
        label.textAlignment = .center
        label.textColor = UIColor.blueColor.blue
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let issuerRequsestTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.textColor = .black
        tf.textAlignment = .right
        tf.placeholder = "عنوان درخواست"
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.subjectColor = UIColor.blueColor.blue
        tf.underLineColor = UIColor.blueColor.blue
        return tf
    }()
    
    let descriptionTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.textColor = .black
        tf.textAlignment = .right
        tf.placeholder = "عنوان درخواست"
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.subjectColor = UIColor.blueColor.blue
        tf.underLineColor = UIColor.blueColor.blue
        return tf
    }()
    
    let saveEditReqButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor.blueColor.blue
        btn.tintColor = UIColor.white
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("تغییر درخواست", for: .normal)
        return btn
    }()
    
    @objc func handelBlackViewDismiss() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blackView.alpha = 0
            
        }, completion: nil)
    }
    
    @objc func handelWhiteViewUnDismiss() {
        self.blackView.alpha = 1
    }

    
    @objc fileprivate func handelEditRequest() {
        
        if let windows = UIApplication.shared.keyWindow {
            windows.addSubview(blackView)
            blackView.frame = windows.frame
            blackView.addSubview(whiteView)
            let g1 = UITapGestureRecognizer(target: self, action: #selector(handelDismissKeyboard))
            let g2 = UITapGestureRecognizer(target: self, action: #selector(handelWhiteViewUnDismiss))
            blackView.addGestureRecognizer(g1)
            whiteView.addGestureRecognizer(g2)

            blackView.addConstraintWithFormat(format: "H:|-\(getSizeScreen().width / 5)-[v0]-\(getSizeScreen().width / 5)-|", views: whiteView)
            blackView.addConstraintWithFormat(format: "V:|-\(getSizeScreen().height / 4)-[v0]-\(getSizeScreen().height / 2.3)-|", views: whiteView)

            
            whiteView.addSubview(issuerRequsestTextField)
            whiteView.addSubview(descriptionTextField)
            whiteView.addSubview(saveEditReqButton)
            whiteView.addSubview(titleIssueLabel)
            whiteView.addSubview(descriptionLabel)
            

            
            issuerRequsestTextField.topAnchor.constraint(equalTo: whiteView.topAnchor, constant: getSizeScreen().height / 20).isActive = true
            issuerRequsestTextField.rightAnchor.constraint(equalTo: whiteView.rightAnchor, constant: -10).isActive = true
            issuerRequsestTextField.leftAnchor.constraint(equalTo: whiteView.leftAnchor, constant: 10).isActive = true
            issuerRequsestTextField.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 25).isActive = true
            issuerRequsestTextField.becomeFirstResponder()
            
            titleIssueLabel.topAnchor.constraint(equalTo: whiteView.topAnchor, constant: 5).isActive = true
            titleIssueLabel.bottomAnchor.constraint(equalTo: issuerRequsestTextField.topAnchor).isActive = true
            titleIssueLabel.rightAnchor.constraint(equalTo: whiteView.rightAnchor, constant: -10).isActive = true
            titleIssueLabel.leftAnchor.constraint(equalTo: whiteView.leftAnchor, constant: 10).isActive = true
            
            (descriptionTextField).topAnchor.constraint(equalTo: issuerRequsestTextField.bottomAnchor, constant: getSizeScreen().height / 20).isActive = true
            (descriptionTextField).rightAnchor.constraint(equalTo: whiteView.rightAnchor, constant: -10).isActive = true
            (descriptionTextField).leftAnchor.constraint(equalTo: whiteView.leftAnchor, constant: 10).isActive = true
            (descriptionTextField).heightAnchor.constraint(equalToConstant: getSizeScreen().height / 25).isActive = true
            
            descriptionLabel.topAnchor.constraint(equalTo: issuerRequsestTextField.bottomAnchor).isActive = true
            descriptionLabel.bottomAnchor.constraint(equalTo: descriptionTextField.topAnchor).isActive = true
            descriptionLabel.rightAnchor.constraint(equalTo: whiteView.rightAnchor, constant: -10).isActive = true
            descriptionLabel.leftAnchor.constraint(equalTo: whiteView.leftAnchor, constant: 10).isActive = true
            
            saveEditReqButton.bottomAnchor.constraint(equalTo: whiteView.bottomAnchor, constant: -getSizeScreen().height / 25).isActive = true
            saveEditReqButton.rightAnchor.constraint(equalTo: whiteView.rightAnchor, constant: -10).isActive = true
            saveEditReqButton.leftAnchor.constraint(equalTo: whiteView.leftAnchor, constant: 10).isActive = true
            saveEditReqButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 25).isActive = true
            saveEditReqButton.addTarget(self, action: #selector(updateRequset), for: .touchUpInside)
            
            blackView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 1
                
            }, completion: nil)
        }
        
    }
    
    var newTitle = ""
    var newDes = ""
    
    @objc fileprivate func updateRequset() {
        print(123)
        if let userId = UserDefaults.standard.value(forKey: "userid") as? String {
            userid = userId
        }
        
        newTitle = issuerRequsestTextField.text!
        newDes = descriptionTextField.text!
        
        let userInfoURL = baseURLString + "editreq.php?problemid=\(ProblemId)&title=\(newTitle)&owner=\(owner)&des=\(newDes)&userid=\(userid)"
//        let getUrl = userInfoURL.data(using: String.Encoding.ascii, allowLossyConversion: true)
//        let urlString = String(data: getUrl!, encoding: String.Encoding.ascii)
        let encode = userInfoURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let url = URL(string: encode!) else{return}
        Alamofire.request(url, method: .get).responseJSON { (response) in

            if let data = response.result.value {
                if let json = JSON(data).int {
                    if json == 227 || json == 228 || json == 229 {
                        let alert = CDAlertView(title: "", message: "تغییر درخواست با موفقیت انجام شد", type: CDAlertViewType.success)
                        let action = CDAlertViewAction(title: "فهمیدم")
                        alert.add(action: action)
                        alert.show()
                        self.handelDismissKeyboard()
                    }
                    else {
                        let alert = CDAlertView(title: "", message: "تغییر درخواست با موفقیت انجام نشد، دوباره امتحان کنید", type: CDAlertViewType.warning)
                        let action = CDAlertViewAction(title: "فهمیدم")
                        alert.add(action: action)
                        alert.show()
                        self.handelDismissKeyboard()
                    }
                }
            }


        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

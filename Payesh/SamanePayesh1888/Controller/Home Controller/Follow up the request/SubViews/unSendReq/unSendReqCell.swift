//
//  unSendReqCell.swift
//  SamanePayesh1888
//
//  Created by Parsai on 9/30/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit

class unSendReqCell: baseCell {
    
    let baseView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        return view
    }()
    
    let answerAdminView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    let subjectLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.text = "شکایت-درخواست از اپلیکیشن-Asdad asdad"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let line: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.blueColor.blue
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let statusLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .lightGray
        lbl.textAlignment = .right
        lbl.text = "وضعیت: \n\n ارسال نشده"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let dateLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .lightGray
        lbl.textAlignment = .right
        lbl.text = "تاریخ: \n\n ۲۶/۰۹/۱۳۹۷"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        return lbl
    }()
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    override func setupViews() {
        super.setupViews()
        
        let distance = getSizeScreen().height / 40
        
        addSubview(baseView)
        addConstraintWithFormat(format: "H:|-\(distance)-[v0]-\(distance)-|", views: baseView)
        addConstraintWithFormat(format: "V:|-\(distance - 10)-[v0]-\(distance)-|", views: baseView)
        
        baseView.addSubview(answerAdminView)
        baseView.addConstraintWithFormat(format: "H:[v0(\(getSizeScreen().width / 20))]|", views: answerAdminView)
        baseView.addConstraintWithFormat(format: "V:|[v0]|", views: answerAdminView)
        
        baseView.addSubview(subjectLabel)
        subjectLabel.rightAnchor.constraint(equalTo: answerAdminView.leftAnchor).isActive = true
        subjectLabel.leftAnchor.constraint(equalTo: baseView.leftAnchor).isActive = true
        subjectLabel.topAnchor.constraint(equalTo: baseView.topAnchor, constant: 10).isActive = true
        subjectLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 17).isActive = true
        subjectLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 45)
        
        baseView.addSubview(line)
        line.rightAnchor.constraint(equalTo: answerAdminView.leftAnchor).isActive = true
        line.leftAnchor.constraint(equalTo: baseView.leftAnchor).isActive = true
        line.topAnchor.constraint(equalTo: subjectLabel.bottomAnchor, constant: 5).isActive = true
        line.heightAnchor.constraint(equalToConstant:1.5).isActive = true
        
        baseView.addSubview(statusLabel)
        statusLabel.rightAnchor.constraint(equalTo: answerAdminView.leftAnchor, constant: -10).isActive = true
        statusLabel.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 10).isActive = true
        statusLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        statusLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 47)
        
        baseView.addSubview(dateLabel)
        dateLabel.rightAnchor.constraint(equalTo: statusLabel.leftAnchor, constant: -10).isActive = true
        dateLabel.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 10).isActive = true
        dateLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        dateLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 47)
        
    }
    
}

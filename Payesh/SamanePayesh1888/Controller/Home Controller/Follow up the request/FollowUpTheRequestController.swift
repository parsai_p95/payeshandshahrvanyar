//
//  FollowUpTheRequestController.swift
//  SamanePayesh1888
//
//  Created by Parsai on 9/23/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit

class FollowUpTheRequestController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let cellId = "cellID"
    
    lazy var menuBar: MenuBar = {
        
        let mb = MenuBar()
        mb.FollowUp = self
        mb.translatesAutoresizingMaskIntoConstraints = false
        return mb
        
    }()
    
    var home: HomeController?
    
    func setupCollectionView()
    {
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
//            flowLayout.shouldInvalidateLayout(forBoundsChange: CGRect(x: 50, y: 0, width: view.frame.width, height: view.frame.height))
        }
        collectionView.register(listReqCell.self, forCellWithReuseIdentifier: cellId)
        view.addSubview(collectionView)
        collectionView.frame = view.frame

        collectionView.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
        collectionView.scrollIndicatorInsets = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
        collectionView.isPagingEnabled = true
    }
    
    let imagePickerController = UIImagePickerController()
    
    func showImagePicker() {
        imagePickerController.delegate = self
        imagePickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    var sendRequsetImagesArray: [Data] = []
    var sendRequsetImagesFeedbackArray: [Data] = []
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        
        if picker == self.imagePickerController {
            
            let myImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            let compressedImage = myImage?.jpeg(.lowest)
            sendRequsetImagesArray.append(compressedImage!)
            sendRequsetImagesFeedbackArray.append(compressedImage!)
            let numImage = sendRequsetImagesArray.count
//            SendRequsetView.budgeValue = numImage
            UnSendReqView.whiteView.SendRequsetView.budgeValue = numImage
            UnSendReqView.whiteView.SendRequsetView.addBudgeToImageBudgeLabel()
        }
        else {
            print("else")
        }
        
        self.dismiss(animated: true, completion: nil)
        UnSendReqView.whiteView.showProblemView()
    }
    
    let navView = UIView()
    
    private func setupMenuBar()
    {
        navigationController?.hidesBarsOnSwipe = true
        
        let redView = UIView()
        redView.backgroundColor = UIColor.lightColor.light
        view.addSubview(redView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: redView)
        view.addConstraintWithFormat(format: "V:[v0(50)]", views: redView)
        
        
        navView.backgroundColor = UIColor.lightColor.light
        
        view.addSubview(navView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: navView)
        view.addConstraintWithFormat(format: "V:[v0(\(getSizeScreen().height / 15))]", views: navView)
        let guide = view.safeAreaLayoutGuide
        navView.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
        
        view.addSubview(menuBar)
        menuBar.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        menuBar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        menuBar.topAnchor.constraint(equalTo: navView.bottomAnchor).isActive = true
        menuBar.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 17).isActive = true
    }
    
    let backButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .clear
        btn.tintColor = .white
        btn.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "back")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        return btn
    }()
    
    let cancelFilterButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .clear
        btn.tintColor = .white
        btn.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.isHidden = true
        return btn
    }()
    
    let calenderButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .clear
        btn.tintColor = .white
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(UIImage(named: "calender")?.withRenderingMode(.alwaysTemplate), for: .normal)
        return btn
    }()
    
    fileprivate func setupNavigationBar() {
        
        backButton.addTarget(self, action: #selector(handelDismiss), for: .touchUpInside)
        calenderButton.addTarget(self, action: #selector(handelCalenderTap), for: .touchUpInside)
        cancelFilterButton.addTarget(self, action: #selector(handelCanelCalender), for: .touchUpInside)
        
        navView.addSubview(backButton)
        navView.addSubview(calenderButton)
        navView.addSubview(titleNavLabel)
        navView.addSubview(cancelFilterButton)
        
        backButton.centerYAnchor.constraint(equalTo: navView.centerYAnchor).isActive = true
        backButton.leftAnchor.constraint(equalTo: navView.leftAnchor, constant: getSizeScreen().width / 20).isActive = true
        
        cancelFilterButton.centerYAnchor.constraint(equalTo: navView.centerYAnchor).isActive = true
        cancelFilterButton.leftAnchor.constraint(equalTo: navView.leftAnchor, constant: getSizeScreen().width / 20).isActive = true
        
        calenderButton.centerYAnchor.constraint(equalTo: navView.centerYAnchor).isActive = true
        calenderButton.rightAnchor.constraint(equalTo: navView.rightAnchor, constant: -getSizeScreen().width / 18).isActive = true
        
        titleNavLabel.centerYAnchor.constraint(equalTo: navView.centerYAnchor).isActive = true
        titleNavLabel.centerXAnchor.constraint(equalTo: navView.centerXAnchor).isActive = true
        titleNavLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 18).isActive = true
        titleNavLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2).isActive = true
        titleNavLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 55)
    }

    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
//        cv.backgroundColor = UIColor(white: 0, alpha: 0.1)
        cv.backgroundColor = .white
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let titleNavLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.textAlignment = .center
        label.text = "پیگیری درخواست ها"
        return label
    }()
    
    let blackView = UIView()
    let greenView = UIView()
    let whiteView = UIView()
    
    var nameMonth: String?
    var numMonth: String?
    var numDay: String?
    var numYear: String?
    var fullCal: String?
    
    let persianCalender: GDCalendar = {
        let pc = GDCalendar()
        pc.translatesAutoresizingMaskIntoConstraints = false
        pc.backgroundColor = .white
        pc.headerBackgroundColor = UIColor(r: 44, g: 202, b: 175)
        pc.itemHighlightColor = UIColor(r: 44, g: 202, b: 175)
        pc.itemHighlightTextColor = .white
        pc.itemsFont = UIFont.systemFont(ofSize: 9)
        pc.headersFont = UIFont.boldSystemFont(ofSize: 9)
        return pc
    }()
    var height: CGFloat?
    
    let acceptButton: UIButton = {
        let btn = UIButton(type: .system)
        return btn
    }()
    
    let cancelButton: UIButton = {
        let btn = UIButton(type: .system)
        return btn
    }()
    
    let monthLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 20)
        return lbl
    }()
    let dayNumLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 40)
        return lbl
    }()
    let yearNumLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.textColor = UIColor(white: 1.5, alpha: 0.5)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 15)
        return lbl
    }()

    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        setupNavigationBar()
        setupCollectionView()
        setupMenuBar()
    }


    
    @objc fileprivate func handelDismiss() {
        
        dismiss(animated: true, completion: nil)
        
    }
    

    
    fileprivate func getFilterDate(date: String) -> String {
        let delimiter = "/"
        var editedDate = ""
        
        var token = date.components(separatedBy: delimiter)
        let zeroToken = token[0]
        let firstToken = token[1]
        let secondToken = token[2]
        
        if secondToken.count == 1 {
            
            editedDate = zeroToken + "/" + firstToken + "/" + secondToken
        }
        else {
            editedDate = date
        }
        
        return editedDate
    }
    
    @objc func handelAccept() {
        
        
        calenderButton.isHidden = true
        backButton.isHidden = true
        cancelFilterButton.isHidden = false
        
        if !selectCalenderCellMode {
            titleNavLabel.text = getCurrentPersianDate()
            SendReqView.dateSelect = getCurrentPersianDate()
            selectCalenderCellMode = false
            
            let filterCheckDate = getFilterDate(date: getCurrentPersianDate())
            SendReqView.getFilterDateToShowRequest(dateFilter: filterCheckDate)
        }
        else {
            titleNavLabel.text = fullCal
            SendReqView.dateSelect = fullCal!
            selectCalenderCellMode = false
            let filterCheckDate = getFilterDate(date: fullCal!)
            SendReqView.getFilterDateToShowRequest(dateFilter: filterCheckDate)
        }

        handleDismiss()
    }
    
    @objc func handelCanelCalender() {
        

        
        calenderButton.isHidden = false
        backButton.isHidden = false
        cancelFilterButton.isHidden = true
        
        SendReqView.loader.stopAnimating()
        
        titleNavLabel.text = "پیگیری درخواست ها"
        
        SendReqView.backToNormal()
        
    }
    
    @objc func handelCancel() {
        
        handleDismiss()
    }
    
    @objc func handleDismiss() {
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow
            {
                self.greenView.frame = CGRect(x: 0, y: window.frame.height, width: self.greenView.frame.width, height: self.greenView.frame.height)
            }
            
        }, completion: nil)
    }
    
    func getCurrentPersianDate() -> String {
        var days = ""
        var monthes = ""
        var years = ""
        
        let day = String(persianCalender.currentDate.componentsOfDate.day)
        if let number = Int(day.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
            let perNum = convertEngNumToPersianNum(num: String(number))
            
            days = perNum
            
        }
        
        let month = String(persianCalender.currentDate.componentsOfDate.month)
        if let number = Int(month.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
            let perNum = convertEngNumToPersianNum(num: String(number))
            
            monthes = perNum
            
        }
        
        
        let year = String(persianCalender.currentDate.componentsOfDate.year)
        if let number = Int(year.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
            let perNum = convertEngNumToPersianNum(num: String(number))
            
            years = perNum
            
        }
        
        let str = (years + "/" + monthes + "/" + days)
        return str
    }
    
    var selectCalenderCellMode = false
    
    @objc func handelCalenderTap() {
        
        acceptButton.setImage(#imageLiteral(resourceName: "done").withRenderingMode(.alwaysTemplate), for: .normal)
        acceptButton.backgroundColor = UIColor(r: 44, g: 202, b: 175)
        acceptButton.tintColor = .white
        acceptButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        acceptButton.translatesAutoresizingMaskIntoConstraints = false
        acceptButton.addTarget(self, action: #selector(handelAccept), for: .touchUpInside)
        
        cancelButton.setImage(#imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysTemplate), for: .normal)
        cancelButton.backgroundColor = UIColor(r: 44, g: 202, b: 175)
        cancelButton.tintColor = .white
        cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.addTarget(self, action: #selector(handelCancel), for: .touchUpInside)
        
        if let window = UIApplication.shared.keyWindow
        {
            whiteView.backgroundColor = .white
            whiteView.translatesAutoresizingMaskIntoConstraints = false
            height = window.frame.height/1.5 + 50
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            greenView.backgroundColor = UIColor(r: 44, g: 202, b: 175)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
            window.addSubview(blackView)
            window.addSubview(greenView)
            greenView.layer.cornerRadius = 5
            greenView.layer.masksToBounds = false
            
            
            
            persianCalender.dateSelectHandler = { [weak self] date in
                
                self?.selectCalenderCellMode = true
                // action when a date is selected
                self?.monthLabel.text = date.monthName
                self?.nameMonth = date.monthName
                
                let selectmonth = String(date.componentsOfDate.month)
                if let number = Int(selectmonth.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
                    let perNum = self?.convertEngNumToPersianNum(num: String(number))
                    
                    self?.numMonth = perNum
                    
                }
                
                let selectDay = String(date.componentsOfDate.day)
                if let number = Int(selectDay.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
                    let perNum = self?.convertEngNumToPersianNum(num: String(number))
                    
                    self?.dayNumLabel.text = perNum
                    self?.numDay = perNum
                    
                }
                
                let selectYear = String(date.componentsOfDate.year)
                if let number = Int(selectYear.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
                    let perNum = self?.convertEngNumToPersianNum(num: String(number))
                    
                    self?.yearNumLabel.text = perNum
                    self?.numYear = perNum
                    
                }
                
                if let years = self?.numYear
                {
                    if let monthes = self?.numMonth
                    {
                        if let days = self?.numDay
                        {
                            let cal = (years + "/" + monthes + "/" + days)
                            self?.fullCal = cal
                            
                        }
                    }
                }
                
            }
            
            //let y = window.frame.height - height
            greenView.frame = CGRect(x: 0, y: window.frame.height, width: (window.frame.width/1.3), height: height!)
            
            
            blackView.frame = window.frame
            blackView.alpha = 0
            
            
            
            
            greenView.addSubview(whiteView)
            whiteView.leftAnchor.constraint(equalTo: greenView.leftAnchor).isActive = true
            whiteView.rightAnchor.constraint(equalTo: greenView.rightAnchor).isActive = true
            whiteView.bottomAnchor.constraint(equalTo: greenView.bottomAnchor).isActive = true
            let calenderHeight: CGFloat = (greenView.frame.height/3) * 2 + 25
            whiteView.heightAnchor.constraint(equalToConstant: calenderHeight).isActive = true
            whiteView.layer.cornerRadius = 5
            whiteView.layer.masksToBounds = true
            
            greenView.addSubview(monthLabel)
            greenView.addSubview(dayNumLabel)
            greenView.addSubview(yearNumLabel)
            
            monthLabel.centerXAnchor.constraint(equalTo: greenView.centerXAnchor).isActive = true
            monthLabel.topAnchor.constraint(equalTo: greenView.topAnchor, constant: 5).isActive = true
            monthLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
            monthLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
            monthLabel.text = persianCalender.currentDate.monthName
            
            dayNumLabel.centerXAnchor.constraint(equalTo: greenView.centerXAnchor).isActive = true
            dayNumLabel.topAnchor.constraint(equalTo: monthLabel.bottomAnchor, constant: 5).isActive = true
            dayNumLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true
            dayNumLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
            
            
            
            
            let day = String(persianCalender.currentDate.componentsOfDate.day)
            if let number = Int(day.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
                let perNum = convertEngNumToPersianNum(num: String(number))
                
                self.dayNumLabel.text = perNum
                
            }
            
            
            
            whiteView.addSubview(persianCalender)
            
            
            
            persianCalender.leftAnchor.constraint(equalTo: whiteView.leftAnchor, constant: 20).isActive = true
            persianCalender.rightAnchor.constraint(equalTo: whiteView.rightAnchor, constant: -20).isActive = true
            persianCalender.bottomAnchor.constraint(equalTo: whiteView.bottomAnchor, constant: -(window.frame.width/5)).isActive = true
            persianCalender.topAnchor.constraint(equalTo: whiteView.topAnchor).isActive = true
            
            yearNumLabel.centerXAnchor.constraint(equalTo: greenView.centerXAnchor).isActive = true
            yearNumLabel.topAnchor.constraint(equalTo: dayNumLabel.bottomAnchor).isActive = true
            yearNumLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
            yearNumLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
            let year = String(persianCalender.currentDate.componentsOfDate.year)
            if let number = Int(year.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
                let perNum = convertEngNumToPersianNum(num: String(number))
                
                self.yearNumLabel.text = perNum
                
            }
            
            let operationView = UIView()
            operationView.backgroundColor = .white
            operationView.translatesAutoresizingMaskIntoConstraints = false
            whiteView.addSubview(operationView)
            operationView.leftAnchor.constraint(equalTo: whiteView.leftAnchor).isActive = true
            operationView.rightAnchor.constraint(equalTo: whiteView.rightAnchor).isActive = true
            operationView.topAnchor.constraint(equalTo: persianCalender.bottomAnchor).isActive = true
            operationView.bottomAnchor.constraint(equalTo: whiteView.bottomAnchor).isActive = true
            
            operationView.addSubview(acceptButton)
            acceptButton.rightAnchor.constraint(equalTo: operationView.rightAnchor, constant: -20).isActive = true
            acceptButton.centerYAnchor.constraint(equalTo: operationView.centerYAnchor).isActive = true
            acceptButton.heightAnchor.constraint(equalToConstant: window.frame.width/10).isActive = true
            acceptButton.widthAnchor.constraint(equalToConstant: (window.frame.width/10)*2).isActive = true
            
            acceptButton.layer.cornerRadius = window.frame.width/20
            acceptButton.layer.masksToBounds = false
            
            operationView.addSubview(cancelButton)
            cancelButton.leftAnchor.constraint(equalTo: operationView.leftAnchor, constant: 20).isActive = true
            cancelButton.centerYAnchor.constraint(equalTo: operationView.centerYAnchor).isActive = true
            cancelButton.heightAnchor.constraint(equalToConstant: window.frame.width/10).isActive = true
            cancelButton.widthAnchor.constraint(equalToConstant: (window.frame.width/10)*2).isActive = true
            
            cancelButton.layer.cornerRadius = window.frame.width/20
            cancelButton.layer.masksToBounds = false
            
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 1
                //self.persianCalender.frame = CGRect(x: 0, y: y, width: 250, height: self.persianCalender.frame.height)
                self.greenView.center = CGPoint(x: window.frame.size.width/2, y: window.frame.size.height/2)
                
            }, completion: nil)
            
            
            
        }
        
    }
    
    func convertEngNumToPersianNum(num: String) -> String {
        //let number = NSNumber(value: Int(num)!)
        let format = NumberFormatter()
        format.locale = Locale(identifier: "fa_IR")
        let number =   format.number(from: num)
        let faNumber = format.string(from: number!)
        return faNumber!
        
    }
    
    func scrollToMenuIndex(menuIndex: Int)
    {
        let indexPath = NSIndexPath(item: menuIndex, section: 0)
        collectionView.scrollToItem(at: indexPath as IndexPath, at: .right, animated: true)
        
        if menuIndex == 1 {
            calenderButton.isHidden = true
        }
        else {
            calenderButton.isHidden = false
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        menuBar.horizentalBarLeftAnchor?.constant = scrollView.contentOffset.x / 2
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let index = targetContentOffset.pointee.x / view.frame.width
        
        let indexpath = NSIndexPath(item: Int(index), section: 0)
        menuBar.collectionView.selectItem(at: indexpath as IndexPath, animated: true, scrollPosition: .right)
        
        if index == 1 {
            calenderButton.isHidden = true
        }
        else {
            calenderButton.isHidden = false
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    lazy var SendReqView: sentRequestView = {
        let view = sentRequestView()
        view.FllowUp = self
        return view
    }()
    lazy var UnSendReqView: unSentRequestView = {
        let view = unSentRequestView()
        view.FllowUp = self
        return view
    }()
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! listReqCell
        if indexPath.item == 0 {
            
            cell.baseView.addSubview(SendReqView)
            cell.baseView.addConstraintWithFormat(format: "H:|[v0]|", views: SendReqView)
            cell.baseView.addConstraintWithFormat(format: "V:|[v0]|", views: SendReqView)
            
        }
        if indexPath.item == 1 {
            cell.baseView.addSubview(UnSendReqView)
            cell.baseView.addConstraintWithFormat(format: "H:|[v0]|", views: UnSendReqView)
            cell.baseView.addConstraintWithFormat(format: "V:|[v0]|", views: UnSendReqView)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldUpdateFocusIn context: UICollectionViewFocusUpdateContext) -> Bool {
        return true
    }
    
    func showNumBudgeOfOfflineReq(ProblemTypeArray: [String]) {
        
        let numberID = ProblemTypeArray.count
        home?.budgeNumItemSaveToCoreData = numberID
        home?.showNumberOfItemsSaveToCoreData()
        
    }

}

//
//  aboutUsController.swift
//  SamanePayesh1888
//
//  Created by apple on 10/10/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit

class aboutUsController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cellID"
    
    lazy var menuBar: aboutUsMenuBar = {
        
        let mb = aboutUsMenuBar()
        mb.aboutUs = self
//        mb.translatesAutoresizingMaskIntoConstraints = false
        return mb
        
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        //        cv.backgroundColor = UIColor(white: 0, alpha: 0.1)
        cv.backgroundColor = .white
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupCollectionView()
        setupNavigationBar()
        setupMenuBar()
    }
    
    fileprivate func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.lightColor.light
        navigationController?.navigationBar.tintColor = .white
        
        let image = UIImage(named: "back")
        let cancel = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handelDismiss))
        navigationItem.setLeftBarButton(cancel, animated: true)
        
    }
    
    @objc func handelDismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    func setupCollectionView()
    {
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0

        }
        collectionView.register(aboutUsCell.self, forCellWithReuseIdentifier: cellId)
        view.addSubview(collectionView)
        collectionView.frame = view.frame
        
        collectionView.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
        collectionView.scrollIndicatorInsets = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
        collectionView.isPagingEnabled = true
    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    private func setupMenuBar()
    {
        
        
        let redView = UIView()
        redView.backgroundColor = UIColor.lightColor.light
        view.addSubview(redView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: redView)
        view.addConstraintWithFormat(format: "V:[v0(50)]", views: redView)

        view.addSubview(menuBar)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: menuBar)
        view.addConstraintWithFormat(format: "V:[v0(50)]", views: menuBar)
        
        let guide = view.safeAreaLayoutGuide
        menuBar.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! aboutUsCell
        if indexPath.item == 0 {
            
            let introducteView = introductionAppView()
            cell.baseView.addSubview(introducteView)
            cell.baseView.addConstraintWithFormat(format: "H:|[v0]|", views: introducteView)
            cell.baseView.addConstraintWithFormat(format: "V:|[v0]|", views: introducteView)
        }
        else {
            let AboutUsSubView = aboutUsSubView()
            cell.baseView.addSubview(AboutUsSubView)
            cell.baseView.addConstraintWithFormat(format: "H:|[v0]|", views: AboutUsSubView)
            cell.baseView.addConstraintWithFormat(format: "V:|[v0]|", views: AboutUsSubView)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldUpdateFocusIn context: UICollectionViewFocusUpdateContext) -> Bool {
        return true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        menuBar.horizentalBarLeftAnchor?.constant = scrollView.contentOffset.x / 2
    }
    
    func scrollToMenuIndex(menuIndex: Int)
    {
        let indexPath = NSIndexPath(item: menuIndex, section: 0)
        collectionView.scrollToItem(at: indexPath as IndexPath, at: .right, animated: true)
        
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let index = targetContentOffset.pointee.x / view.frame.width
        
        let indexpath = NSIndexPath(item: Int(index), section: 0)
        menuBar.collectionView.selectItem(at: indexpath as IndexPath, animated: true, scrollPosition: .right)

        
    }
    

}

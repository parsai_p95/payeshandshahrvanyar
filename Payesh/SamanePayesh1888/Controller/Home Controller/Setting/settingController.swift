//
//  settingController.swift
//  SamanePayesh1888
//
//  Created by apple on 10/10/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit

class settingController: UIViewController {
    
    let settingMapView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    
    let settingMapLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "          تنظیمات نقشه"
        lbl.textColor = .white
        lbl.textAlignment = .right
        lbl.backgroundColor = UIColor(r: 87, g: 155, b: 159)
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        return lbl
    }()
    
    let settingDesMapLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "زمانی که به اینترنت دسترسی ندارید درصورت دانلود نقشه، نقشه خودکار بحالت آفلاین میرود، فقط کافی ست زوم کنید"
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let manageDownloadView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    
    let manageDownloadLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "          مدیریت دانلود"
        lbl.textColor = .white
        lbl.textAlignment = .right
        lbl.backgroundColor = UIColor(r: 87, g: 155, b: 159)
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        return lbl
    }()
    
    let offlineMapLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "نقشه آفلاین"
        lbl.textColor = .black
        lbl.textAlignment = .right
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let downloadMapButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.tintColor = UIColor.blueColor.blue
        btn.setTitle("دانلود نقشه", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let settingNewsView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    
    let settingNewsLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "          تنظیمات اخبار"
        lbl.textColor = .white
        lbl.textAlignment = .right
        lbl.backgroundColor = UIColor(r: 87, g: 155, b: 159)
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        return lbl
    }()
    
    let remindImportantNewsLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "اطلاع رسانی اخبار مهم : "
        lbl.textColor = .black
        lbl.textAlignment = .right
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let remindImportantNewsSwitch: UISwitch = {
        
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.addTarget(self, action: #selector(typeOfProblemSwitchesDidChange(_:)), for: .valueChanged)
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
    @objc func typeOfProblemSwitchesDidChange(_ sender: UISwitch!) {
        if sender.isOn {
            userDefult.setValue(true, forKey: "isSendNotification")
        }
        else {
            userDefult.setValue(false, forKey: "isSendNotification")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor(r: 217, g: 220, b: 221)
        setupNavigationBar()
        setupViews()
        
        if let isSendNotification = userDefult.value(forKey: "isSendNotification") as? Bool {
            if isSendNotification {
                remindImportantNewsSwitch.isOn = true
            }
            else {
                remindImportantNewsSwitch.isOn = false
            }
        }
        else {
            remindImportantNewsSwitch.isOn = false
        }
    }
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    fileprivate func setupViews() {
        view.addSubview(settingMapView)
        settingMapView.topAnchor.constraint(equalTo: view.topAnchor, constant: (getSizeScreen().height / 25)).isActive = true
        settingMapView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: (getSizeScreen().height / 70)).isActive = true
        settingMapView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -(getSizeScreen().height / 70)).isActive = true
        settingMapView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 5).isActive = true
        
        settingMapView.addSubview(settingMapLabel)
        settingMapView.addConstraintWithFormat(format: "H:|[v0]|", views: settingMapLabel)
        settingMapView.addConstraintWithFormat(format: "V:|[v0(\(getSizeScreen().height / 20))]", views: settingMapLabel)
        
        settingMapView.addSubview(settingDesMapLabel)
        settingDesMapLabel.topAnchor.constraint(equalTo: settingMapLabel.bottomAnchor).isActive = true
        settingDesMapLabel.rightAnchor.constraint(equalTo: settingMapView.rightAnchor, constant: -getSizeScreen().height / 20).isActive = true
        settingDesMapLabel.leftAnchor.constraint(equalTo: settingMapView.leftAnchor, constant: getSizeScreen().height / 20).isActive = true
        settingDesMapLabel.bottomAnchor.constraint(equalTo: settingMapView.bottomAnchor).isActive = true
        settingDesMapLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 50)
        
        view.addSubview(manageDownloadView)
        manageDownloadView.topAnchor.constraint(equalTo: settingMapView.bottomAnchor, constant: (getSizeScreen().height / 25)).isActive = true
        manageDownloadView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: (getSizeScreen().height / 70)).isActive = true
        manageDownloadView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -(getSizeScreen().height / 70)).isActive = true
        manageDownloadView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 5).isActive = true
        
        manageDownloadView.addSubview(manageDownloadLabel)
        manageDownloadView.addConstraintWithFormat(format: "H:|[v0]|", views: manageDownloadLabel)
        manageDownloadView.addConstraintWithFormat(format: "V:|[v0(\(getSizeScreen().height / 20))]", views: manageDownloadLabel)
        
        manageDownloadView.addSubview(offlineMapLabel)
        offlineMapLabel.topAnchor.constraint(equalTo: manageDownloadLabel.bottomAnchor).isActive = true
        offlineMapLabel.rightAnchor.constraint(equalTo: manageDownloadView.rightAnchor, constant: -getSizeScreen().height / 20).isActive = true
        offlineMapLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        offlineMapLabel.bottomAnchor.constraint(equalTo: manageDownloadView.bottomAnchor).isActive = true
        offlineMapLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 50)
        
        manageDownloadView.addSubview(downloadMapButton)
        downloadMapButton.leftAnchor.constraint(equalTo: manageDownloadView.leftAnchor, constant: getSizeScreen().width / 20).isActive = true
        downloadMapButton.centerYAnchor.constraint(equalTo: offlineMapLabel.centerYAnchor).isActive = true
        downloadMapButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 3).isActive = true
        downloadMapButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 10).isActive = true
        downloadMapButton.addTarget(self, action: #selector(handelDownloadMap), for: .touchUpInside)
        downloadMapButton.titleLabel?.font = UIFont.systemFont(ofSize: getSizeScreen().height / 50)
        
        view.addSubview(settingNewsView)
        settingNewsView.topAnchor.constraint(equalTo: manageDownloadView.bottomAnchor, constant: (getSizeScreen().height / 25)).isActive = true
        settingNewsView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: (getSizeScreen().height / 70)).isActive = true
        settingNewsView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -(getSizeScreen().height / 70)).isActive = true
        settingNewsView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 5).isActive = true
        
        settingNewsView.addSubview(settingNewsLabel)
        settingNewsView.addConstraintWithFormat(format: "H:|[v0]|", views: settingNewsLabel)
        settingNewsView.addConstraintWithFormat(format: "V:|[v0(\(getSizeScreen().height / 20))]", views: settingNewsLabel)
        
        settingNewsView.addSubview(remindImportantNewsLabel)
        remindImportantNewsLabel.topAnchor.constraint(equalTo: settingNewsLabel.bottomAnchor).isActive = true
        remindImportantNewsLabel.rightAnchor.constraint(equalTo: settingNewsView.rightAnchor, constant: -getSizeScreen().height / 20).isActive = true
        remindImportantNewsLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2.3).isActive = true
        remindImportantNewsLabel.bottomAnchor.constraint(equalTo: settingNewsView.bottomAnchor).isActive = true
        remindImportantNewsLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 55)
        
        settingNewsView.addSubview(remindImportantNewsSwitch)
        remindImportantNewsSwitch.leftAnchor.constraint(equalTo: settingNewsView.leftAnchor, constant: getSizeScreen().width / 20).isActive = true
        remindImportantNewsSwitch.centerYAnchor.constraint(equalTo: remindImportantNewsLabel.centerYAnchor).isActive = true

    }
    let userDefult = UserDefaults()
    
    @objc fileprivate func handelDownloadMap() {
        
        userDefult.setValue(true, forKey: "offlineMode")
        
        let cityProblem = cityProblemsController()
        self.navigationController?.pushViewController(cityProblem, animated: true)
        
    }
    
    fileprivate func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.lightColor.light
        navigationController?.navigationBar.tintColor = .white
        
        let image = UIImage(named: "back")
        let cancel = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handelDismiss))
        navigationItem.setLeftBarButton(cancel, animated: true)
        
        let navLabel = UILabel()
        navLabel.textColor = .white
        navLabel.text = ""
        navigationItem.titleView = navLabel
        navigationItem.title = "تنظیمات"
        
    }
    
    @objc func handelDismiss() {
        dismiss(animated: true, completion: nil)
    }

}

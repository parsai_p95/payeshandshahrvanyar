//
//  selectedShowNewsView.swift
//  SamanePayesh1888
//
//  Created by Parsai on 9/23/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit
import ImageSlideshow

class selectedShowNewsView: UIView {
    
    struct newsImage {
        var img1: String?
        var img2: String?
        var img3: String?
        var img4: String?
        var img5: String?
        var img6: String?
    }
    
    var NewsImage: [newsImage] = []

    var newsCollectionView: NewsController?

    var bodyArray: [String] = []
    var subjectArray: [String] = []
    var catArray: [String] = []
    var imageArray: [String] = []
    var images: [String] = []

    
    let topLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 24, g: 177, b: 255)
        return view
    }()
    
    let titleLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
    
    let subjectLabel: UILabel = {
        let label = UILabel()
        label.text = "تست"
        label.textColor = .gray
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let bodyTextView: UITextView = {
        let textView = UITextView()
        textView.textColor = .gray
        textView.isEditable = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.textAlignment = .center
        return textView
    }()
    
    let imageSlider: ImageSlideshow = {
        let imageSlider = ImageSlideshow()
        imageSlider.translatesAutoresizingMaskIntoConstraints = false
        return imageSlider
    }()
    
    let importantImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 10
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "important")?.withRenderingMode(.alwaysTemplate)
        imageView.backgroundColor = .white
        imageView.tintColor = .red
        imageView.clipsToBounds = true
        imageView.isHidden = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    

    var indexSelect = 0
    var numberOfImage = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupViews() {
        setupSlideMenu()
    }
    

    
    fileprivate func setupSlideMenu() {
        
        addSubview(topLine)
        addConstraintWithFormat(format: "H:|[v0]|", views: topLine)
        addConstraintWithFormat(format: "V:|-\(getSizeScreen().height / 17)-[v0(1.5)]", views: topLine)

        addSubview(imageSlider)
        imageSlider.topAnchor.constraint(equalTo: topLine.bottomAnchor, constant: 10).isActive = true
        imageSlider.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        imageSlider.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        imageSlider.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 3).isActive = true
        imageSlider.contentScaleMode = .scaleAspectFill
        imageSlider.backgroundColor = .lightGray
        imageSlider.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
        imageSlider.slideshowInterval = 2
        
        addSubview(subjectLabel)
        subjectLabel.topAnchor.constraint(equalTo: imageSlider.bottomAnchor, constant: 10).isActive = true
        subjectLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5).isActive = true
        subjectLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 5).isActive = true
        subjectLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        subjectLabel.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 45)
        
        addSubview(importantImageView)
        importantImageView.bottomAnchor.constraint(equalTo: subjectLabel.topAnchor).isActive = true
        importantImageView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -30).isActive = true
        importantImageView.heightAnchor.constraint(equalToConstant: 36).isActive = true
        importantImageView.widthAnchor.constraint(equalToConstant: 36).isActive = true
        importantImageView.layer.cornerRadius = 18
        importantImageView.layer.masksToBounds = true
        
        addSubview(titleLine)
        titleLine.topAnchor.constraint(equalTo: subjectLabel.bottomAnchor, constant: 10).isActive = true
        titleLine.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        titleLine.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        titleLine.heightAnchor.constraint(equalToConstant: 1.5).isActive = true
        
        addSubview(bodyTextView)
        bodyTextView.topAnchor.constraint(equalTo: titleLine.bottomAnchor, constant: 10).isActive = true
        bodyTextView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        bodyTextView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        bodyTextView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
        bodyTextView.font = UIFont.boldSystemFont(ofSize: getSizeScreen().height / 45)
    }
    
    func addImageToSliderImage(imageArray: [String]) {
        
        var imageSource: [AlamofireSource] = []
        
        for image in imageArray {
            if let images = AlamofireSource(urlString: image) {
                imageSource.append(images)
            }
        }
        imageSlider.setImageInputs(imageSource)
    }
    
     func getOneNews(item: Int) {
        
        bodyArray.removeAll()
        subjectArray.removeAll()
        catArray.removeAll()
        images.removeAll()
        
        var image1 = ""
        var image2 = ""
        var image3 = ""
        var image4 = ""
        var image5 = ""
        var image6 = ""
        numberOfImage = 0
        
        let urlString = "http://212.33.202.139:8587/app/news.php"
        guard let url = URL(string: urlString) else {return}
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, erroe) in
            
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    if let Json = json as? NSDictionary {
                        if let array = Json["array"] as? [NSDictionary] {
                            for (key) in array {
                                if let body = key["body"] as? String {
                                    self.bodyArray.append(body)
                                    if let sunject = key["subject"] as? String  {
                                        self.subjectArray.append(sunject)
                                        if let cat = key["cat"] as? String {
                                            self.catArray.append(cat)
                                            
                                            
                                        }
                                    }
                                }
                                
                                if let img1 = key["img1"] {
                                    image1 = (img1 as? String)!
                                    
                                }
                                if let img2 = key["img2"] {
                                    image2 = (img2 as? String)!
                                    
                                }
                                if let img3 = key["img3"] {
                                    image3 = (img3 as? String)!
                                    
                                }
                                if let img4 = key["img4"] {
                                    image4 = (img4 as? String)!
                                    
                                }
                                if let img5 = key["img5"] {
                                    image5 = (img5 as? String)!
                                    
                                }
                                if let img6 = key["img6"] {
                                    image6 = (img6 as? String)!
                                    
                                }
                                
                                let currentImage = newsImage(img1: image1, img2: image2, img3: image3, img4: image4, img5: image5, img6: image6)
                                self.NewsImage.append(currentImage)
                               
                            }
                            
                            DispatchQueue.main.async {
                                self.images.removeAll()
                                let selectCurrentImage = self.NewsImage[item]
                                let titleNews = self.subjectArray[item]
                                self.subjectLabel.text = titleNews
                                let bodyNews = self.bodyArray[item]
                                self.bodyTextView.text = bodyNews
                                let category = self.catArray[item]
                                if category == "important" {
                                    self.importantImageView.isHidden = false
                                }
                                if let ima1 = selectCurrentImage.img1 {
                                    if ima1 != "" {
                                        self.images.append(baseURLString + ima1)
                                    }
                                }
                                if let ima2 = selectCurrentImage.img2 {
                                    if ima2 != "" {
                                        self.images.append(baseURLString + ima2)
                                    }
                                    
                                }
                                if let ima3 = selectCurrentImage.img3 {
                                    if ima3 != "" {
                                        self.images.append(baseURLString + ima3)
                                    }
                                    
                                }
                                if let ima4 = selectCurrentImage.img4 {
                                    if ima4 != "" {
                                        self.images.append(baseURLString + ima4)
                                    }
                                    
                                }
                                if let ima5 = selectCurrentImage.img5 {
                                    if ima5 != "" {
                                        self.images.append(baseURLString + ima5)
                                    }
                                    
                                }
                                if let ima6 = selectCurrentImage.img6 {
                                    if ima6 != "" {
                                        self.images.append(baseURLString + ima6)
                                    }
                                    
                                }

                                self.addImageToSliderImage(imageArray: self.images)
                            }
                        }
                    }
                }
                catch {
                    print(error)
                }
            }
            
            }.resume()
        
    }

    

}



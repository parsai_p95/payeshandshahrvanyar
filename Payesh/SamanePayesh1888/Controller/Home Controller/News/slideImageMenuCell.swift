//
//  slideImageMenuCell.swift
//  SamanePayesh1888
//
//  Created by Parsai on 9/20/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit

class slideImageMenuCell: baseCell {
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = .gray
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(imageView)
        imageView.frame = self.frame
        
    }
    
}

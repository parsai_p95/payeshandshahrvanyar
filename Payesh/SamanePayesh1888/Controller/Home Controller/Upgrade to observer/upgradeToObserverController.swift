//
//  upgradeToObserverController.swift
//  SamanePayesh1888
//
//  Created by Parsai on 10/2/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DropDown
import CDAlertView
import NVActivityIndicatorView

class upgradeToObserverController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var bottomPadding: CGFloat?
    
    var tahsilatArray: [String] = []
    var idTahsilatArray: [String] = []
    
    var takhasosArray: [String] = []
    var idTakhasosArray: [String] = []
    
    var areaListArray: [String] = []
    var idAreaListArray: [String] = []
    
    let scrollView: UIScrollView = {
        let scview = UIScrollView()
        scview.backgroundColor = .white
        return scview
    }()
    
    let educationFieldDropDown = DropDown()
    let hozeShahrdariDropDown = DropDown()
    let areaDropDown = DropDown()
    
    let whiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let sendReqButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor(r: 10, g: 96, b: 238)
        btn.tintColor = .white
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("ارسال درخواست", for: .normal)
        
        return btn
    }()
    
    let nationalCodeTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.textColor = .black
        tf.textAlignment = .right
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.subjectColor = UIColor.blueColor.blue
        tf.underLineColor = UIColor.blueColor.blue
        tf.placeholder = "کد ملی"
        return tf
    }()
    
    let educationCatTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.textColor = .black
        tf.textAlignment = .right
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.subjectColor = UIColor.blueColor.blue
        tf.underLineColor = UIColor.blueColor.blue
        tf.placeholder = "زمینه تحصیلی"
        return tf
    }()
    
    let adressTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.textColor = .black
        tf.textAlignment = .right
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.subjectColor = UIColor.blueColor.blue
        tf.underLineColor = UIColor.blueColor.blue
        tf.placeholder = "آدرس"
        return tf
    }()
    
    let tahsilatView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2
        return view
    }()
    
    let tahsilatButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.tintColor = .gray
        btn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        btn.titleLabel?.transform = CGAffineTransform(scaleX: -1, y: 1.0)
        btn.imageView?.transform = CGAffineTransform(scaleX: -1, y: 1.0)
        btn.setImage(UIImage(named: "drop")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.setTitle("انتخاب مدرک تحصیلی", for: .normal)
        return btn
    }()
    
    let hozeShahrdariView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2
        return view
    }()
    
    let hozeShahrdariButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.tintColor = .gray
        btn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        btn.titleLabel?.transform = CGAffineTransform(scaleX: -1, y: 1.0)
        btn.imageView?.transform = CGAffineTransform(scaleX: -1, y: 1.0)
        btn.setImage(UIImage(named: "drop")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.setTitle("انتخاب حوضه شهرداری", for: .normal)
        return btn
    }()
    
    let areaView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.5).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2
        return view
    }()
    
    let areaButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .white
        btn.tintColor = .gray
        btn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        btn.titleLabel?.transform = CGAffineTransform(scaleX: -1, y: 1.0)
        btn.imageView?.transform = CGAffineTransform(scaleX: -1, y: 1.0)
        btn.setImage(UIImage(named: "drop")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.setTitle("انتخاب منطقه", for: .normal)
        return btn
    }()

    let pinFileView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor(white: 0, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2.5
        return view
    }()
    
    let titleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 87, g: 155, b: 159)
        view.layer.cornerRadius = 10
        return view
    }()
    
    let pinFileLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "پیوست عکس های کارت شناسایی"
        lbl.textColor = .white
        lbl.textAlignment = .left
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let pinImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "verticalPin")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .white
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let personalCartFirstImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .lightGray
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.tintColor = .gray
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let personalCartSecondImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .lightGray
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.tintColor = .gray
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let personalCartThirdImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .lightGray
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.tintColor = .gray
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let firstImagesView = UIView()
    let secondImagesView = UIView()
    let thirdImagesView = UIView()
    
    var SaveTahsilatMode = false
    var SaveHozeShahrdariMode = false
    var SaveAreaMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        setupNavigationBar()
        setupViews()
        if let usid = userDefult.value(forKey: "userid") as? String {
            if usid == "" {
                self.dismiss(animated: true, completion: nil)
                userDefult.setValue(true, forKey: "UnacceptUserId")
                let profile = profileController()
                let root = UINavigationController(rootViewController: profile)
                self.present(root, animated: true, completion: nil)
            }
        }
        else {
            self.dismiss(animated: true, completion: nil)
            userDefult.setValue(true, forKey: "UnacceptUserId")
            let profile = profileController()
            let root = UINavigationController(rootViewController: profile)
            self.present(root, animated: true, completion: nil)
        }
    }
    
    let titleNavLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 25))
        label.textColor = .white
        label.textAlignment = .center
        label.text = "سامانه پایش"
        return label
    }()
    
    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    
    fileprivate func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.lightColor.light
        navigationController?.navigationBar.tintColor = .white
        
        let image = UIImage(named: "back")
        let cancel = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handelDismiss))
        navigationItem.setLeftBarButton(cancel, animated: true)
        
        navigationItem.titleView = titleNavLabel
        titleNavLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 50)
        
        
    }
    
    @objc fileprivate func handelDismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    var bottomConstraint: NSLayoutConstraint?
    
    fileprivate func setupBottomButton() {
        let guaid = view.safeAreaLayoutGuide
        view.addSubview(sendReqButton)
        sendReqButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        sendReqButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 1.7).isActive = true
        sendReqButton.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 25).isActive = true
        bottomConstraint = NSLayoutConstraint(item: sendReqButton, attribute: .bottom, relatedBy: .equal, toItem: guaid, attribute: .bottom, multiplier: 1, constant: -getSizeScreen().height / 40)
        view.addConstraint(bottomConstraint!)
        sendReqButton.addTarget(self, action: #selector(handelSendReqToObserver), for: .touchUpInside)
        sendReqButton.titleLabel?.font = UIFont.systemFont(ofSize: getSizeScreen().height / 60)
    }
    
    var madrakTahsili = ""
    var hozeShahrdari = ""
    var entekhabeMantaghe = ""
    var idPErson = ""
    var Mobile = ""
    
    var homeVC: HomeController?
    
    let loader: NVActivityIndicatorView = {
        let loader = NVActivityIndicatorView(frame: .zero)
        let loaderType = NVActivityIndicatorType.ballRotateChase
        loader.type = loaderType
        loader.color = .darkGray
        loader.padding = 5
        loader.translatesAutoresizingMaskIntoConstraints = false
        return loader
    }()
    
    @objc fileprivate func handelSendReqToObserver() {
        
        if let id = userDefult.value(forKey: "id") as? String {
            idPErson = id
        }
        if let mobile = userDefult.value(forKey: "phoneNum") as? String {
            Mobile = mobile
        }
        
        
        if nationalCodeTextField.text == "" || educationCatTextField.text == "" || adressTextField.text == "" || madrakTahsili == "" || hozeShahrdari == "" || entekhabeMantaghe == "" || firstImageData == nil ||  secondImageData == nil || thirdmageData == nil  {
            
            let alert = CDAlertView(title: "", message: "باید تمام موارد پر شوند", type: .warning)
            let action = CDAlertViewAction(title: "فهمیدم")
            alert.add(action: action)
            alert.show()
        }
        else {
            
//            let userID = userDefult.value(forKey: "userid") as? String

            let userInfoURL = baseURLString + "signupnazer.php"
            let encode = userInfoURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            let parameter = ["id": idPErson, "mobile": Mobile, "cm": nationalCodeTextField.text!, "adr": adressTextField.text!, "tahsili": madrakTahsili, "tahsilat": educationCatTextField.text!, "hoze": hozeShahrdari, "mantaghe": entekhabeMantaghe]
            guard let url = URL(string: encode!) else{return}
            
            loader.startAnimating()
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                multipartFormData.append(self.firstImageData!, withName: "img1", fileName: "image.jpg", mimeType: "image/jpg")
                multipartFormData.append(self.secondImageData!, withName: "img2", fileName: "image.jpg", mimeType: "image/jpg")
                multipartFormData.append(self.thirdmageData!, withName: "img3", fileName: "image.jpg", mimeType: "image/jpg")
                
                for (key, value) in parameter {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
                
            }, to: url) { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        if let data = response.result.value {
                            let json = JSON(data)
                            print(json)
                            let array = json["array"].object
                            if (array != nil) {
                                self.loader.stopAnimating()
                                let alert = CDAlertView(title: "", message: "درخواست شما با موفقیت ثبت گردید", type: .success)
                                //                    let action = CDAlertViewAction(title: "فهمیدم")
                                let action = CDAlertViewAction(title: "فهمیدم", font: UIFont.systemFont(ofSize: 16), textColor: UIColor.greenColor.green, backgroundColor: .white, handler: { (action) -> Bool in
                                    self.homeVC?.changeUpgradeToObserveOps()
                                    self.handelDismiss()
                                    return true
                                    
                                })
                                alert.add(action: action)
                                alert.show()
                                
                            }
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                }
                
            }
            
        }
    }
    
    fileprivate func setupViews() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(handelKeyBoardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handelKeyBoardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        setupScrollView()
        setupScrollViewItems()
        setupBottomButton()
        
        view.addSubview(loader)
        loader.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loader.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loader.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
        loader.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true
    }
    
    let personalCartFirstButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .clear
        btn.tintColor = .gray
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(UIImage(named: "image1")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.layer.masksToBounds = true
        return btn
    }()
    let personalCartSecondButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .clear
        btn.tintColor = .gray
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(UIImage(named: "image1")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.layer.masksToBounds = true
        return btn
    }()
    let personalCartThirdButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .clear
        btn.tintColor = .gray
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(UIImage(named: "image1")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.layer.masksToBounds = true
        return btn
    }()
    
    let copyShenasnameLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "صفحه اول شناسنامه"
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 11)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let copykartMeliLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "کارت ملی"
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 11)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let copyMadrakLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "آخرین مدرک تحصیلی"
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 11)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    fileprivate func setupScrollViewItems() {
        whiteView.addSubview(nationalCodeTextField)
        nationalCodeTextField.topAnchor.constraint(equalTo: whiteView.topAnchor, constant: getSizeScreen().height / 20).isActive = true
        nationalCodeTextField.centerXAnchor.constraint(equalTo: whiteView.centerXAnchor).isActive = true
        nationalCodeTextField.widthAnchor.constraint(equalToConstant: getSizeScreen().width - 20).isActive = true
        nationalCodeTextField.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 18).isActive = true
        
        whiteView.addSubview(educationCatTextField)
        educationCatTextField.topAnchor.constraint(equalTo: nationalCodeTextField.bottomAnchor, constant: getSizeScreen().height / 20).isActive = true
        educationCatTextField.centerXAnchor.constraint(equalTo: whiteView.centerXAnchor).isActive = true
        educationCatTextField.widthAnchor.constraint(equalToConstant: getSizeScreen().width - 20).isActive = true
        educationCatTextField.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 18).isActive = true
        
        whiteView.addSubview(tahsilatView)
        tahsilatView.topAnchor.constraint(equalTo: educationCatTextField.bottomAnchor, constant: getSizeScreen().height / 20).isActive = true
        tahsilatView.centerXAnchor.constraint(equalTo: whiteView.centerXAnchor).isActive = true
        tahsilatView.widthAnchor.constraint(equalToConstant: getSizeScreen().width - 40).isActive = true
        tahsilatView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        
        whiteView.addSubview(tahsilatView)
        tahsilatView.topAnchor.constraint(equalTo: educationCatTextField.bottomAnchor, constant: getSizeScreen().height / 20).isActive = true
        tahsilatView.centerXAnchor.constraint(equalTo: whiteView.centerXAnchor).isActive = true
        tahsilatView.widthAnchor.constraint(equalToConstant: getSizeScreen().width - 40).isActive = true
        tahsilatView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        
        tahsilatView.addSubview(tahsilatButton)
        tahsilatView.addConstraintWithFormat(format: "H:|[v0]|", views: tahsilatButton)
        tahsilatView.addConstraintWithFormat(format: "V:|[v0]|", views: tahsilatButton)
        tahsilatButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: getSizeScreen().width / 2.5)
        tahsilatButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: getSizeScreen().width / 3)
        tahsilatButton.addTarget(self, action: #selector(handelChooseFieldEducation), for: .touchUpInside)

        whiteView.addSubview(hozeShahrdariView)
        hozeShahrdariView.topAnchor.constraint(equalTo: tahsilatView.bottomAnchor, constant: getSizeScreen().height / 25).isActive = true
        hozeShahrdariView.centerXAnchor.constraint(equalTo: whiteView.centerXAnchor).isActive = true
        hozeShahrdariView.widthAnchor.constraint(equalToConstant: getSizeScreen().width - 40).isActive = true
        hozeShahrdariView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        
        hozeShahrdariView.addSubview(hozeShahrdariButton)
        hozeShahrdariView.addConstraintWithFormat(format: "H:|[v0]|", views: hozeShahrdariButton)
        hozeShahrdariView.addConstraintWithFormat(format: "V:|[v0]|", views: hozeShahrdariButton)
        hozeShahrdariButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: getSizeScreen().width / 2.5)
        hozeShahrdariButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: getSizeScreen().width / 3)
        hozeShahrdariButton.addTarget(self, action: #selector(handelChooseHozeshahrdari), for: .touchUpInside)
        
        whiteView.addSubview(areaView)
        areaView.topAnchor.constraint(equalTo: hozeShahrdariView.bottomAnchor, constant: getSizeScreen().height / 25).isActive = true
        areaView.centerXAnchor.constraint(equalTo: whiteView.centerXAnchor).isActive = true
        areaView.widthAnchor.constraint(equalToConstant: getSizeScreen().width - 40).isActive = true
        areaView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        
        areaView.addSubview(areaButton)
        areaView.addConstraintWithFormat(format: "H:|[v0]|", views: areaButton)
        areaView.addConstraintWithFormat(format: "V:|[v0]|", views: areaButton)
        areaButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: getSizeScreen().width / 1.8)
        areaButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: getSizeScreen().width / 2.1)
        areaButton.addTarget(self, action: #selector(handelChooseArea), for: .touchUpInside)
        
        whiteView.addSubview(adressTextField)
        adressTextField.topAnchor.constraint(equalTo: areaView.bottomAnchor, constant: getSizeScreen().height / 20).isActive = true
        adressTextField.centerXAnchor.constraint(equalTo: whiteView.centerXAnchor).isActive = true
        adressTextField.widthAnchor.constraint(equalToConstant: getSizeScreen().width - 20).isActive = true
        adressTextField.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 18).isActive = true
        
        let personalInfoViewHeight = getSizeScreen().width + 60
        whiteView.addSubview(pinFileView)
        pinFileView.topAnchor.constraint(equalTo: adressTextField.bottomAnchor, constant: (getSizeScreen().height / 30)).isActive = true
        pinFileView.leftAnchor.constraint(equalTo: whiteView.leftAnchor, constant: (getSizeScreen().height / 70)).isActive = true
        pinFileView.rightAnchor.constraint(equalTo: whiteView.rightAnchor, constant: -(getSizeScreen().height / 70)).isActive = true
        pinFileView.heightAnchor.constraint(equalToConstant: personalInfoViewHeight).isActive = true
        
        pinFileView.addSubview(titleView)
        pinFileView.addConstraintWithFormat(format: "H:|[v0]|", views: titleView)
        pinFileView.addConstraintWithFormat(format: "V:|[v0(\(getSizeScreen().height / 18))]", views: titleView)
        
        titleView.addSubview(pinFileLabel)
        pinFileLabel.rightAnchor.constraint(equalTo: titleView.rightAnchor, constant: -10).isActive = true
        pinFileLabel.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true

        titleView.addSubview(pinImageView)
        pinImageView.rightAnchor.constraint(equalTo: pinFileLabel.leftAnchor).isActive = true
        pinImageView.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
        pinImageView.widthAnchor.constraint(equalToConstant: getSizeScreen().height / 30).isActive = true
        pinImageView.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 30).isActive = true
        //UIImage(named: "image")?.withRenderingMode(.alwaysTemplate)
        
        pinFileView.addSubview(personalCartFirstImageView)
        personalCartFirstImageView.rightAnchor.constraint(equalTo: pinFileView.rightAnchor, constant: -10).isActive = true
        personalCartFirstImageView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 15).isActive = true
        personalCartFirstImageView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        personalCartFirstImageView.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        pinFileView.addSubview(personalCartFirstButton)
        personalCartFirstButton.rightAnchor.constraint(equalTo: pinFileView.rightAnchor, constant: -10).isActive = true
        personalCartFirstButton.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 15).isActive = true
        personalCartFirstButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        personalCartFirstButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        personalCartFirstButton.layer.cornerRadius = 5
        personalCartFirstButton.addTarget(self, action: #selector(getFirstPersonalCartImage), for: .touchUpInside)
        
        pinFileView.addSubview(copyShenasnameLabel)
        copyShenasnameLabel.centerXAnchor.constraint(equalTo: personalCartFirstButton.centerXAnchor).isActive = true
        copyShenasnameLabel.topAnchor.constraint(equalTo: personalCartFirstButton.bottomAnchor, constant: 5).isActive = true
        copyShenasnameLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        copyShenasnameLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2).isActive = true
        
        pinFileView.addSubview(personalCartSecondImageView)
        personalCartSecondImageView.leftAnchor.constraint(equalTo: pinFileView.leftAnchor, constant: 10).isActive = true
        personalCartSecondImageView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 15).isActive = true
        personalCartSecondImageView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        personalCartSecondImageView.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        pinFileView.addSubview(personalCartSecondButton)
        personalCartSecondButton.leftAnchor.constraint(equalTo: pinFileView.leftAnchor, constant: 10).isActive = true
        personalCartSecondButton.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 15).isActive = true
        personalCartSecondButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        personalCartSecondButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        personalCartSecondButton.layer.cornerRadius = 5
        personalCartSecondButton.addTarget(self, action: #selector(getSecondPersonalCartImage), for: .touchUpInside)
        
        pinFileView.addSubview(copykartMeliLabel)
        copykartMeliLabel.centerXAnchor.constraint(equalTo: personalCartSecondButton.centerXAnchor).isActive = true
        copykartMeliLabel.topAnchor.constraint(equalTo: personalCartSecondButton.bottomAnchor, constant: 5).isActive = true
        copykartMeliLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        copykartMeliLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2).isActive = true
        
        pinFileView.addSubview(copyMadrakLabel)
        copyMadrakLabel.centerXAnchor.constraint(equalTo: pinFileView.centerXAnchor).isActive = true
        copyMadrakLabel.bottomAnchor.constraint(equalTo: pinFileView.bottomAnchor, constant: -5).isActive = true
        copyMadrakLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        copyMadrakLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2).isActive = true
        
        pinFileView.addSubview(personalCartThirdImageView)
        personalCartThirdImageView.centerXAnchor.constraint(equalTo: pinFileView.centerXAnchor).isActive = true
        personalCartThirdImageView.bottomAnchor.constraint(equalTo: copyMadrakLabel.topAnchor, constant: -5).isActive = true
        personalCartThirdImageView.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        personalCartThirdImageView.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        pinFileView.addSubview(personalCartThirdButton)
        personalCartThirdButton.centerXAnchor.constraint(equalTo: pinFileView.centerXAnchor).isActive = true
        personalCartThirdButton.bottomAnchor.constraint(equalTo: copyMadrakLabel.topAnchor, constant: -5).isActive = true
        personalCartThirdButton.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        personalCartThirdButton.heightAnchor.constraint(equalToConstant: getSizeScreen().width / 2.5).isActive = true
        personalCartThirdButton.layer.cornerRadius = 5
        personalCartThirdButton.addTarget(self, action: #selector(getThirdPersonalCartImage), for: .touchUpInside)
    }
    
    lazy var firstImagePickerController: UIImagePickerController = {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        return imagePicker
    }()
    
    lazy var secondImagePickerController: UIImagePickerController = {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        return imagePicker
    }()
    
    lazy var thirdImagePickerController: UIImagePickerController = {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        return imagePicker
    }()
    
    @objc fileprivate func getFirstPersonalCartImage() {
        personalCartFirstImageView.isHidden = false
        firstImagesView.isHidden = true
        self.present(firstImagePickerController, animated: true, completion: nil)
    }
    @objc fileprivate func getSecondPersonalCartImage() {
        personalCartSecondImageView.isHidden = false
        secondImagesView.isHidden = true
        self.present(secondImagePickerController, animated: true, completion: nil)
    }
    @objc fileprivate func getThirdPersonalCartImage() {
        personalCartThirdImageView.isHidden = false
        thirdImagesView.isHidden = true
        self.present(thirdImagePickerController, animated: true, completion: nil)
    }
    
    var firstImageData: Data?
    var secondImageData: Data?
    var thirdmageData: Data?
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if picker == self.firstImagePickerController {
            let myImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            let compressedImage = myImage?.jpeg(.lowest)
            firstImageData = myImage?.jpeg(.lowest)
            personalCartFirstButton.setImage(nil, for: .normal)
            personalCartFirstImageView.image = UIImage(data: compressedImage!)
            
        }
        else if picker == self.secondImagePickerController {
            
            let myImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            let compressedImage = myImage?.jpeg(.lowest)
            secondImageData = myImage?.jpeg(.lowest)
            personalCartSecondButton.setImage(nil, for: .normal)
            personalCartSecondImageView.image = UIImage(data: compressedImage!)
        }
        else {
            let myImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            let compressedImage = myImage?.jpeg(.lowest)
            thirdmageData = myImage?.jpeg(.lowest)
            personalCartThirdButton.setImage(nil, for: .normal)
            personalCartThirdImageView.image = UIImage(data: compressedImage!)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height / 1.3)
        scrollView.addSubview(whiteView)
        whiteView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        whiteView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        whiteView.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
        whiteView.rightAnchor.constraint(equalTo: scrollView.rightAnchor).isActive = true
        whiteView.heightAnchor.constraint(equalToConstant: getSizeScreen().height * 1.35).isActive = true
        whiteView.addConstraint(NSLayoutConstraint(item: whiteView, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 0, constant: (view?.frame.width)!))
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handelDismissKeyboard))
        whiteView.addGestureRecognizer(gesture)
    }
    
    @objc fileprivate func handelDismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func handelKeyBoardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let keyBoardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
            let keyboardRectangle = keyBoardFrame.cgRectValue
            
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            bottomPadding = 0
            
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                bottomPadding = window?.safeAreaInsets.bottom
            }
            
            
            if isKeyboardShowing {
                goBottomOfScrollView(mode: isKeyboardShowing)
                bottomConstraint?.constant = -keyboardRectangle.height + bottomPadding! - 10
            }
            else {
                goBottomOfScrollView(mode: isKeyboardShowing)
                bottomConstraint?.constant = -getSizeScreen().height / 40
            }
            UIView.animate(withDuration: 1.5, delay: 0, options: .curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
                
            }) { (completion) in
                
                
            }
            
        }
    }
    
    func goBottomOfScrollView(mode: Bool) {
        
        if mode {
            UIView.animate(withDuration: 1.5, delay: 0, options: .curveEaseOut, animations: {
                
                self.scrollView.setContentOffset(CGPoint(x: 0, y: self.scrollView.contentSize.height), animated: true)
                
            }) { (completion) in
                
                
            }
            
        }
        else {
            UIView.animate(withDuration: 1.5, delay: 0, options: .curveEaseOut, animations: {
                
                self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                
            }) { (completion) in
                
                
            }
            
        }
        
    }
    
    @objc fileprivate func handelChooseFieldEducation() {
        
        getTahsilat()
    }
    @objc fileprivate func handelChooseHozeshahrdari() {
        
        getHozehShahrdari()
    }
    @objc fileprivate func handelChooseArea() {
        
        getArea()
    }
    
    let userDefult = UserDefaults()
    
    fileprivate func getTahsilat() {
        tahsilatArray.removeAll()
        idTakhasosArray.removeAll()
        let sendSmsUrlString = baseURLString + "tahsilat.php"
        guard let url = URL(string: sendSmsUrlString) else {return}
        Alamofire.request(url, method: .get).responseJSON { (response) in
            
            if let data = response.data {
                let json = JSON(data)
                let array = json["array"]
                for key in array {
                    let userInfoJson = key.1
                    let id = userInfoJson["id"].string
                    let tahsilat = userInfoJson["tahsilat"].string
                    self.tahsilatArray.append(tahsilat!)
                    self.idTahsilatArray.append(id!)
                }
                DispatchQueue.main.async {
                    self.showChooseEducateFieldView(tahsilat: self.tahsilatArray, idTahsilat: self.idTahsilatArray)
                }
                
            }
            
        }
        
    }
    
    let tahsilatBlackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.65)
        return view
    }()
    let tahsilatWhiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    
    
    fileprivate func showChooseEducateFieldView(tahsilat: [String], idTahsilat: [String]) {
        
        if let windows = UIApplication.shared.keyWindow {
            windows.addSubview(tahsilatBlackView)
            tahsilatBlackView.frame = windows.frame
            
            let blackViewGesture = UITapGestureRecognizer(target: self, action: #selector(dismissTahsilatBlackView))
            tahsilatBlackView.addGestureRecognizer(blackViewGesture)
            
            tahsilatBlackView.addSubview(tahsilatWhiteView)
            tahsilatWhiteView.frame.size.height = windows.frame.height / 2
            tahsilatWhiteView.frame.size.width = windows.frame.width / 2.5
            tahsilatWhiteView.layer.cornerRadius = 7
            tahsilatWhiteView.layer.masksToBounds = true
            tahsilatWhiteView.center = tahsilatBlackView.center
            
            educationFieldDropDown.anchorView = tahsilatWhiteView
            educationFieldDropDown.dataSource = tahsilat
            educationFieldDropDown.dismissMode = .automatic
            educationFieldDropDown.textColor = .black
            educationFieldDropDown.width = windows.frame.width / 2.5
            educationFieldDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                
                self.madrakTahsili = item
                self.tahsilatButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: self.getSizeScreen().width / 1.8)
                self.tahsilatButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.getSizeScreen().width / 2.1)
                self.tahsilatButton.setTitle(item, for: .normal)
                self.dismissTahsilatBlackView()
            }
            
            tahsilatBlackView.alpha = 0
            tahsilatWhiteView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.tahsilatBlackView.alpha = 1
                self.educationFieldDropDown.show()
                
            }, completion: nil)
        }
        
    }
    
    @objc fileprivate func dismissTahsilatBlackView() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.tahsilatBlackView.alpha = 0
            
        }, completion: nil)
    }
    
    fileprivate func getHozehShahrdari() {
        idTakhasosArray.removeAll()
        takhasosArray.removeAll()
        let sendSmsUrlString = baseURLString + "hozetakh.php"
        guard let url = URL(string: sendSmsUrlString) else {return}
        Alamofire.request(url, method: .get).responseJSON { (response) in
            
            if let data = response.data {
                let json = JSON(data)
                let array = json["array"]
                for key in array {
                    let userInfoJson = key.1
                    let idTakhasos = userInfoJson["id"].string
                    let takhasos = userInfoJson["hozetakh"].string
                    self.idTakhasosArray.append(idTakhasos!)
                    self.takhasosArray.append(takhasos!)
                }
                DispatchQueue.main.async {
                    self.showChooseHozeShahrdariView(hoze: self.takhasosArray, idHoze: self.idTakhasosArray)
                }
            }
            
        }
        
    }
    
    let hoezeShahrdariBlackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.65)
        return view
    }()
    let hoezeShahrdariWhiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    fileprivate func showChooseHozeShahrdariView(hoze: [String], idHoze: [String]) {
        
        if let windows = UIApplication.shared.keyWindow {
            windows.addSubview(hoezeShahrdariBlackView)
            hoezeShahrdariBlackView.frame = windows.frame
            
            let blackViewGesture = UITapGestureRecognizer(target: self, action: #selector(dismissHozeShahrdaiBlackView))
            hoezeShahrdariBlackView.addGestureRecognizer(blackViewGesture)
            
            hoezeShahrdariBlackView.addSubview(hoezeShahrdariWhiteView)
            hoezeShahrdariWhiteView.frame.size.height = windows.frame.height / 2
            hoezeShahrdariWhiteView.frame.size.width = windows.frame.width / 2.5
            hoezeShahrdariWhiteView.layer.cornerRadius = 7
            hoezeShahrdariWhiteView.layer.masksToBounds = true
            hoezeShahrdariWhiteView.center = hoezeShahrdariBlackView.center
            
            hozeShahrdariDropDown.anchorView = hoezeShahrdariWhiteView
            hozeShahrdariDropDown.dataSource = hoze
            hozeShahrdariDropDown.dismissMode = .automatic
            hozeShahrdariDropDown.textColor = .black
            hozeShahrdariDropDown.width = windows.frame.width / 2
            hozeShahrdariDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                
                self.hozeShahrdari = item
                
                if index == 0  {
                    self.hozeShahrdariButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: self.getSizeScreen().width / 1.6)
                    self.hozeShahrdariButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.getSizeScreen().width / 1.9)
                }
                else if index == 1 || index == 4 {
                    self.hozeShahrdariButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: self.getSizeScreen().width / 1.8)
                    self.hozeShahrdariButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.getSizeScreen().width / 2.1)
                }
                else {
                    self.hozeShahrdariButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: self.getSizeScreen().width / 2.5)
                    self.hozeShahrdariButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.getSizeScreen().width / 3)
                }
                
                self.hozeShahrdariButton.setTitle(item, for: .normal)
                self.dismissHozeShahrdaiBlackView()
            }
            
            hoezeShahrdariBlackView.alpha = 0
            hoezeShahrdariWhiteView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.hoezeShahrdariBlackView.alpha = 1
                self.hozeShahrdariDropDown.show()
                
            }, completion: nil)
        }
        
    }
    
    @objc fileprivate func dismissHozeShahrdaiBlackView() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.hoezeShahrdariBlackView.alpha = 0
            
        }, completion: nil)
    }
    
    fileprivate func getArea() {
        idAreaListArray.removeAll()
        areaListArray.removeAll()
        let sendSmsUrlString = baseURLString + "mantaghe.php"
        guard let url = URL(string: sendSmsUrlString) else {return}
        Alamofire.request(url, method: .get).responseJSON { (response) in
            
            if let data = response.data {
                let json = JSON(data)
                let array = json["array"]
                for key in array {
                    let userInfoJson = key.1
                    let idArea = userInfoJson["id"].string
                    let areaList = userInfoJson["tahsilat"].string
                    self.idAreaListArray.append(idArea!)
                    self.areaListArray.append(areaList!)
                }
                DispatchQueue.main.async {
                    self.showChooseAreaView(area: self.areaListArray, idArea: self.idAreaListArray)
                }
            }
            
        }
        
    }
    
    let areaBlackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.65)
        return view
    }()
    let areaWhiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    fileprivate func showChooseAreaView(area: [String], idArea: [String]) {
        
        if let windows = UIApplication.shared.keyWindow {
            windows.addSubview(areaBlackView)
            areaBlackView.frame = windows.frame
            
            let blackViewGesture = UITapGestureRecognizer(target: self, action: #selector(dismissAreaBlackView))
            areaBlackView.addGestureRecognizer(blackViewGesture)
            
            areaBlackView.addSubview(areaWhiteView)
            areaWhiteView.frame.size.height = windows.frame.height / 2
            areaWhiteView.frame.size.width = windows.frame.width / 2.5
            areaWhiteView.layer.cornerRadius = 7
            areaWhiteView.layer.masksToBounds = true
            areaWhiteView.center = areaBlackView.center
            
            areaDropDown.anchorView = hoezeShahrdariWhiteView
            areaDropDown.dataSource = area
            areaDropDown.dismissMode = .automatic
            areaDropDown.textColor = .black
            areaDropDown.width = windows.frame.width / 2.5
            areaDropDown.selectionAction = { [unowned self] (index: Int, item: String) in

                self.entekhabeMantaghe = item
                self.areaButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: self.getSizeScreen().width / 1.6)
                self.areaButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: self.getSizeScreen().width / 1.9)
                self.areaButton.setTitle(item, for: .normal)
                self.dismissAreaBlackView()
            }
            
            areaBlackView.alpha = 0
            areaWhiteView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.areaBlackView.alpha = 1
                self.areaDropDown.show()
                
            }, completion: nil)
        }
        
    }
    
    @objc fileprivate func dismissAreaBlackView() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.areaBlackView.alpha = 0
            
        }, completion: nil)
    }

}
 

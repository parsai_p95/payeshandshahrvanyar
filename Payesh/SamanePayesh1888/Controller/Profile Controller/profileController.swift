//
//  profileController.swift
//  SamanePayesh1888
//
//  Created by Parsai on 9/17/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit
import CDAlertView
import Alamofire
import SwiftyJSON

class profileController: UIViewController {
    
    lazy var sendMessage: sendMessageController = {
        let sendMessage = sendMessageController()
        sendMessage.profileC = self
        return sendMessage
    }()
    
    let titleNavLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 25))
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()
    
    let personalInfoView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()

    let titlePersonalInforLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "          اطلاعات شخصی"
        lbl.textColor = .white
        lbl.textAlignment = .right
        lbl.backgroundColor = UIColor(r: 87, g: 155, b: 159)
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        return lbl
    }()
    
    let nameTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.placeholder = "نام"
        tf.textColor = UIColor.lightGray
        tf.textAlignment = .right
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.subjectColor = UIColor.lightColor.light
        tf.underLineColor = UIColor.lightColor.light
        return tf
    }()
    
    let familyTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.placeholder = "نام خانوادگی"
        tf.textColor = UIColor.lightGray
        tf.textAlignment = .right
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.subjectColor = UIColor.lightColor.light
        tf.underLineColor = UIColor.lightColor.light
        return tf
    }()
    
    let telephoneTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.placeholder = "تلفن ثابت"
        tf.textColor = UIColor.lightGray
        tf.textAlignment = .right
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.subjectColor = UIColor.lightColor.light
        tf.underLineColor = UIColor.lightColor.light
        return tf
    }()

    let sexlInfoView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    
    let titleSexlInfoLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "           جنسیت"
        lbl.textColor = .white
        lbl.textAlignment = .right
        lbl.backgroundColor = UIColor(r: 87, g: 155, b: 159)
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        return lbl
    }()
    
    let verticalSexTypeLine: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let manLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "مرد"
        lbl.textColor = .black
        lbl.textAlignment = .right
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        return lbl
    }()
    
    let manSwitch: UISwitch = {
        
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.addTarget(self, action: #selector(productSwitchesDidChange(_:)), for: .valueChanged)
        Switch.layer.setValue(1, forKey: "SexRecognize")
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
    let womanLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "زن"
        lbl.textColor = .black
        lbl.textAlignment = .right
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        return lbl
    }()
    
    let womanSwitch: UISwitch = {
        
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.addTarget(self, action: #selector(productSwitchesDidChange(_:)), for: .valueChanged)
        Switch.layer.setValue(2, forKey: "SexRecognize")
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
    let typeUserView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    
    let titletypeUserLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "           نوع کاربر"
        lbl.textColor = .white
        lbl.textAlignment = .right
        lbl.backgroundColor = UIColor(r: 87, g: 155, b: 159)
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        return lbl
    }()
    
    let verticalUserTypeLine: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let civilizationLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "شهروند"
        lbl.textColor = .black
        lbl.textAlignment = .right
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        return lbl
    }()
    
    let civilizationSwitch: UISwitch = {
        
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.addTarget(self, action: #selector(productSwitchesDidChange(_:)), for: .valueChanged)
        Switch.layer.setValue(3, forKey: "UserTypeRecognize")
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
    let employeeLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "کارمند شهرداری"
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        return lbl
    }()
    
    let employeeSwitch: UISwitch = {
        let Switch = UISwitch()
        Switch.setOn(false, animated: true)
        Switch.addTarget(self, action: #selector(productSwitchesDidChange(_:)), for: .valueChanged)
        Switch.layer.setValue(4, forKey: "UserTypeRecognize")
        Switch.onTintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.tintColor = UIColor(r: 87, g: 155, b: 159)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
    let fillLateButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .clear
        btn.tintColor = UIColor(r: 44, g: 83, b: 236)
        btn.setTitle("بعدا وارد میکنم", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let sendButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor.purpuleColor.purple
        btn.tintColor = .white
        btn.layer.masksToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "done")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        return btn
    }()

    fileprivate func getSizeScreen() -> CGRect {
        
        return UIScreen.main.bounds
    }
    var unacceptUserId = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userdefult = UserDefaults()
        if let unac = userdefult.value(forKey: "UnacceptUserId") as? Bool {
            unacceptUserId = unac
        }
        
        view.backgroundColor = UIColor(r: 217, g: 220, b: 221)
        UIApplication.shared.statusBarStyle = .lightContent
        titleNavLabel.font = UIFont.systemFont(ofSize: getSizeScreen().height / 50)
        setupNavigationBar()
        setupViews()
        
    }
    
    fileprivate func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.lightColor.light
        navigationController?.navigationBar.tintColor = .white
        
//        let nextStep = UIBarButtonItem(image: #imageLiteral(resourceName: "done").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(handelDoneEditProfile))
//        self.navigationItem.setRightBarButton(nextStep, animated: true)
        
        let userDefult = UserDefaults()
        if !unacceptUserId {
            if let phoneNum = userDefult.value(forKey: "phoneNum") {
                
                let persianNum = convertEngNumToPersianNum(num: (phoneNum as? String)!)
                titleNavLabel.text = "۰" + persianNum
            }
            
            self.navigationItem.titleView = titleNavLabel
        }
        else {
            let back = UIBarButtonItem(image: #imageLiteral(resourceName: "back").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(handelBack))
            self.navigationItem.setLeftBarButton(back, animated: true)
        }
    }
    
    @objc fileprivate func handelBack() {
        dismiss(animated: true, completion: nil)
    }
    
    func convertEngNumToPersianNum(num: String) -> String {
        //let number = NSNumber(value: Int(num)!)
        
        let format = NumberFormatter()
        format.locale = Locale(identifier: "fa_IR")
        let number =   format.number(from: num)
        let faNumber = format.string(from: number!)
        return faNumber!
        
    }
    
    var sex = ""
    var userType = ""
    
    @objc fileprivate func handelDoneEditProfile() {
        
        handelDismissKeboard()
        
        if !manSwitch.isOn && !womanSwitch.isOn {
            showAlert(message: "جنسیت را انتخاب نکرده اید")
        }
        else if !civilizationSwitch.isOn && !employeeSwitch.isOn {
            showAlert(message: "نوع کاربر را مشخص نکرده اید")
        }
        else if nameTextField.text == "" {
            showAlert(message: "نام را وارد نکرده اید")
        }
        else if familyTextField.text == "" {
            showAlert(message: "نام خانوادگی را وارد نکرده اید")
        }
        else if telephoneTextField.text == "" {
            showAlert(message: "شماره ثابت را وارد نکرده اید")
        }
        else {
            let userDefult = UserDefaults()
            if let id = userDefult.value(forKey: "userID") as? String, let mobile = userDefult.value(forKey: "phoneNum") as? String{

                saveUserInfo(id: id, mobile: mobile, name: nameTextField.text!, family: familyTextField.text!, type: userType, phone: telephoneTextField.text!, sex: sex)
            }
        }
        
        
        
    }
    
    fileprivate func saveUserInfo(id: String, mobile: String, name: String, family: String, type: String, phone: String, sex: String) {

        let userInfoURL = baseURLString + "signup.php?id=\(id)&mobile=\(mobile)&name=\(name)&family=\(family)&type=\(type)&phone=\(phone)&sex=\(sex)"
        let encode = userInfoURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let url = URL(string: encode!) else{return}
        Alamofire.request(url, method: .get).responseJSON { (response) in

            if let data = response.data {
                let json = JSON(data)
                let array = json["array"]
                for key in array {
                    let userInfoJson = key.1
                    let id = userInfoJson["userid"].string
                    let idd = userInfoJson["id"].string
                    let userDefult = UserDefaults()
                    userDefult.setValue(id, forKey: "userid")
                    userDefult.setValue(idd, forKey: "id")
                }
            }
            if let err = response.result.error {
                print("error is: \(err.localizedDescription)")
            }
            
            let seccess = response.result.isSuccess
            if !seccess {
                self.showAlert(message: "ثبت نام بدرستی انجام نشد، دوباره امتحان کنید")
            }
            else {
                let userDefult = UserDefaults()
                userDefult.setValue(true, forKey: "userSignIn")
                let home = HomeController()
                self.present(home, animated: true, completion: nil)
            }
            

        }
        
    }
    
    fileprivate func showAlert(message: String) {
        let alert = CDAlertView(title: "", message: message, type: .warning)
        let action = CDAlertViewAction(title: "فهمیدم")
        alert.add(action: action)
        alert.show()
    }
    
    @objc func handelDismissKeboard() {
        view.endEditing(true)
    }
    
    fileprivate func setupViews() {
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handelDismissKeboard)))
        
        let personalInfoViewHeight = (6.5 * (getSizeScreen().height / 20)) - 10
        
        view.addSubview(personalInfoView)
        personalInfoView.topAnchor.constraint(equalTo: view.topAnchor, constant: (getSizeScreen().height / 25)).isActive = true
        personalInfoView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: (getSizeScreen().height / 70)).isActive = true
        personalInfoView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -(getSizeScreen().height / 70)).isActive = true
        personalInfoView.heightAnchor.constraint(equalToConstant: personalInfoViewHeight).isActive = true
        
        personalInfoView.addSubview(titlePersonalInforLabel)
        personalInfoView.addConstraintWithFormat(format: "H:|[v0]|", views: titlePersonalInforLabel)
        personalInfoView.addConstraintWithFormat(format: "V:|[v0(\(getSizeScreen().height / 20))]", views: titlePersonalInforLabel)
        
        personalInfoView.addSubview(nameTextField)
        nameTextField.rightAnchor.constraint(equalTo: personalInfoView.rightAnchor, constant: -(getSizeScreen().height / 70)).isActive = true
        nameTextField.topAnchor.constraint(equalTo: titlePersonalInforLabel.bottomAnchor, constant: (getSizeScreen().height / 50)).isActive = true
        nameTextField.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        nameTextField.leftAnchor.constraint(equalTo: personalInfoView.leftAnchor, constant: (getSizeScreen().height / 70)).isActive = true
        
        personalInfoView.addSubview(familyTextField)
        familyTextField.rightAnchor.constraint(equalTo: personalInfoView.rightAnchor, constant: -(getSizeScreen().height / 70)).isActive = true
        familyTextField.topAnchor.constraint(equalTo: nameTextField.bottomAnchor, constant: (getSizeScreen().height / 30)).isActive = true
        familyTextField.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        familyTextField.leftAnchor.constraint(equalTo: personalInfoView.leftAnchor, constant: (getSizeScreen().height / 70)).isActive = true
        
        personalInfoView.addSubview(telephoneTextField)
        telephoneTextField.rightAnchor.constraint(equalTo: personalInfoView.rightAnchor, constant: -(getSizeScreen().height / 70)).isActive = true
        telephoneTextField.topAnchor.constraint(equalTo: familyTextField.bottomAnchor, constant: (getSizeScreen().height / 30)).isActive = true
        telephoneTextField.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        telephoneTextField.leftAnchor.constraint(equalTo: personalInfoView.leftAnchor, constant: (getSizeScreen().height / 70)).isActive = true
        
        let sexInfoViewHeight = (3 * (getSizeScreen().height / 20)) - 10
        view.addSubview(sexlInfoView)
        sexlInfoView.topAnchor.constraint(equalTo: personalInfoView.bottomAnchor, constant: (getSizeScreen().height / 40)).isActive = true
        sexlInfoView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: (getSizeScreen().height / 70)).isActive = true
        sexlInfoView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -(getSizeScreen().height / 70)).isActive = true
        sexlInfoView.heightAnchor.constraint(equalToConstant: sexInfoViewHeight).isActive = true
        
        sexlInfoView.addSubview(titleSexlInfoLabel)
        sexlInfoView.addConstraintWithFormat(format: "H:|[v0]|", views: titleSexlInfoLabel)
        sexlInfoView.addConstraintWithFormat(format: "V:|[v0(\(getSizeScreen().height / 20))]", views: titleSexlInfoLabel)
        
        sexlInfoView.addSubview(verticalSexTypeLine)
        verticalSexTypeLine.centerXAnchor.constraint(equalTo: sexlInfoView.centerXAnchor).isActive = true
        verticalSexTypeLine.topAnchor.constraint(equalTo: titleSexlInfoLabel.bottomAnchor, constant: getSizeScreen().height / 50).isActive = true
        verticalSexTypeLine.bottomAnchor.constraint(equalTo: sexlInfoView.bottomAnchor, constant: -(getSizeScreen().height / 50)).isActive = true
        verticalSexTypeLine.widthAnchor.constraint(equalToConstant: 1).isActive = true
        
        sexlInfoView.addSubview(manSwitch)
        manSwitch.leftAnchor.constraint(equalTo: verticalSexTypeLine.rightAnchor, constant: (getSizeScreen().width / 15)).isActive = true
        manSwitch.centerYAnchor.constraint(equalTo: verticalSexTypeLine.centerYAnchor).isActive = true
        
        sexlInfoView.addSubview(manLabel)
        manLabel.leftAnchor.constraint(equalTo: manSwitch.rightAnchor, constant: 10).isActive = true
        manLabel.centerYAnchor.constraint(equalTo: manSwitch.centerYAnchor).isActive = true
        manLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        manLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 10).isActive = true
        
        sexlInfoView.addSubview(womanSwitch)
        womanSwitch.leftAnchor.constraint(equalTo: sexlInfoView.leftAnchor, constant: (getSizeScreen().width / 15)).isActive = true
        womanSwitch.centerYAnchor.constraint(equalTo: verticalSexTypeLine.centerYAnchor).isActive = true
        
        sexlInfoView.addSubview(womanLabel)
        womanLabel.leftAnchor.constraint(equalTo: womanSwitch.rightAnchor, constant: 10).isActive = true
        womanLabel.centerYAnchor.constraint(equalTo: womanSwitch.centerYAnchor).isActive = true
        womanLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        womanLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 10).isActive = true
        
        let typeUserViewHeight = (3 * (getSizeScreen().height / 20)) - 10
        view.addSubview(typeUserView)
        typeUserView.topAnchor.constraint(equalTo: sexlInfoView.bottomAnchor, constant: (getSizeScreen().height / 40)).isActive = true
        typeUserView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: (getSizeScreen().height / 70)).isActive = true
        typeUserView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -(getSizeScreen().height / 70)).isActive = true
        typeUserView.heightAnchor.constraint(equalToConstant: typeUserViewHeight).isActive = true
        
        typeUserView.addSubview(titletypeUserLabel)
        typeUserView.addConstraintWithFormat(format: "H:|[v0]|", views: titletypeUserLabel)
        typeUserView.addConstraintWithFormat(format: "V:|[v0(\(getSizeScreen().height / 20))]", views: titletypeUserLabel)
        
        typeUserView.addSubview(verticalUserTypeLine)
        verticalUserTypeLine.centerXAnchor.constraint(equalTo: typeUserView.centerXAnchor).isActive = true
        verticalUserTypeLine.topAnchor.constraint(equalTo: titletypeUserLabel.bottomAnchor, constant: getSizeScreen().height / 50).isActive = true
        verticalUserTypeLine.bottomAnchor.constraint(equalTo: typeUserView.bottomAnchor, constant: -(getSizeScreen().height / 50)).isActive = true
        verticalUserTypeLine.widthAnchor.constraint(equalToConstant: 1).isActive = true
        
        typeUserView.addSubview(civilizationSwitch)
        civilizationSwitch.leftAnchor.constraint(equalTo: verticalUserTypeLine.rightAnchor, constant: (getSizeScreen().width / 15)).isActive = true
        civilizationSwitch.centerYAnchor.constraint(equalTo: verticalUserTypeLine.centerYAnchor).isActive = true
        
        typeUserView.addSubview(civilizationLabel)
        civilizationLabel.leftAnchor.constraint(equalTo: civilizationSwitch.rightAnchor, constant: 10).isActive = true
        civilizationLabel.centerYAnchor.constraint(equalTo: civilizationSwitch.centerYAnchor).isActive = true
        civilizationLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 20).isActive = true
        civilizationLabel.widthAnchor.constraint(equalToConstant: getSizeScreen().width / 5).isActive = true

        typeUserView.addSubview(employeeSwitch)
        employeeSwitch.leftAnchor.constraint(equalTo: typeUserView.leftAnchor, constant: (getSizeScreen().width / 15)).isActive = true
        employeeSwitch.centerYAnchor.constraint(equalTo: verticalUserTypeLine.centerYAnchor).isActive = true

        typeUserView.addSubview(employeeLabel)
        employeeLabel.numberOfLines = 0
        employeeLabel.leftAnchor.constraint(equalTo: employeeSwitch.rightAnchor, constant: 5).isActive = true
        employeeLabel.rightAnchor.constraint(equalTo: verticalUserTypeLine.leftAnchor, constant: -5).isActive = true
        employeeLabel.centerYAnchor.constraint(equalTo: employeeSwitch.centerYAnchor).isActive = true
        employeeLabel.heightAnchor.constraint(equalToConstant: getSizeScreen().height / 15).isActive = true
        
        view.addSubview(sendButton)
        sendButton.addTarget(self, action: #selector(handelDoneEditProfile), for: .touchUpInside)
        sendButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        sendButton.topAnchor.constraint(equalTo: typeUserView.bottomAnchor, constant: getSizeScreen().height / 30).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: view.frame.height / 12).isActive = true
        sendButton.heightAnchor.constraint(equalToConstant: view.frame.height / 12 ).isActive = true
        sendButton.layer.cornerRadius = view.frame.height / 24
        
        view.addSubview(fillLateButton)
        fillLateButton.addTarget(self, action: #selector(handelFillLate), for: .touchUpInside)
        fillLateButton.titleLabel?.font = UIFont.systemFont(ofSize: getSizeScreen().height / 45)
        
        fillLateButton.topAnchor.constraint(equalTo: sendButton.bottomAnchor, constant: 20).isActive = true
        fillLateButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        
    }
    
    @objc fileprivate func handelFillLate() {
        
        let userDefult = UserDefaults()
        userDefult.setValue(true, forKey: "userSignIn")
        let homeController = HomeController()
        self.present(homeController, animated: true, completion: nil)
        
    }
    
    @objc func productSwitchesDidChange(_ sender: UISwitch!)
    {
        var sexRecognizeIndex = 0
        var userTypeRecognizeIndex = 0
        
        
        
        if let SexRecognizeIndex = (sender.layer.value(forKey: "SexRecognize")) {
            sexRecognizeIndex = SexRecognizeIndex as! Int
        }
        
        if let UserTypeRecognizeIndex = (sender.layer.value(forKey: "UserTypeRecognize")) {
            userTypeRecognizeIndex = UserTypeRecognizeIndex as! Int
        }
        
        
        if sender.isOn && sexRecognizeIndex == 1 {
            sex = "مرد"
            womanSwitch.isOn = false
        }
        else if sender.isOn && sexRecognizeIndex == 2 {
            
            manSwitch.isOn = false
            sex = "زن"
        }
        else if sender.isOn && userTypeRecognizeIndex == 3 {
            
            employeeSwitch.isOn = false
            userType = "شهروند"
        }
        else if sender.isOn && userTypeRecognizeIndex == 4 {
            userType = "کارمند_شهرداری"
            civilizationSwitch.isOn = false
        }
        
    }

}

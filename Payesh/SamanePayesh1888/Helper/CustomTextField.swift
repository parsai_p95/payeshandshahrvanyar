//
//  CustomTextField.swift
//  SamanePayesh1888
//
//  Created by Parsai on 9/18/1397 AP.
//  Copyright © 1397 LunaTech. All rights reserved.
//

import UIKit


public class CustomTextField: UITextField, UITextFieldDelegate {
    
    @IBInspectable public var animationDuration: Double = 0.5
    @IBInspectable public var subjectColor: UIColor = UIColor.black
    @IBInspectable public var underLineColor: UIColor = UIColor.black
    
    fileprivate let placeholderLabelFontSize: CGFloat = 12.0
    fileprivate var placeholderLabel: UILabel?
    fileprivate var titlePlaceholder: String?
    
    let textFieldLimit = 4
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        delegate = self
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        
        delegate = self
        
        
    }
    
    
    
    
    override public func draw(_ rect: CGRect) {
        drawUnderLine()
        createPlaceholderLabel()
        self.clipsToBounds = false
        self.borderStyle = .none
    }
    
    fileprivate func drawUnderLine() {
        
        let underLineView = UIView(frame: CGRect(x: 0, y: frame.size.height - 1, width: frame.size.width, height: 1))
        underLineView.backgroundColor = underLineColor
        
        self.addSubview(underLineView)
        
    }
    
    fileprivate func createPlaceholderLabel() {
        
        let origin = self.frame.origin
        let label = UILabel(frame: CGRect(x: origin.x, y: origin.y, width: self.frame.size.width, height: 15.0))
        label.center = self.center
        label.textAlignment = .right
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = subjectColor
        
        if let superView = self.superview {
            superView.insertSubview(label, belowSubview: self)
        }
        
        self.placeholderLabel = label
        
    }
    
//    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        return (textField.text?.utf16.count ?? 0) + string.utf16.count - range.length <= textFieldLimit
//
//    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {

        
        if let placeholderLabel = self.placeholderLabel, self.text == "" {
            
            if placeholderLabel.alpha == 0 {
                placeholderLabel.alpha = 1
            }
            
            if self.placeholder == "" {
                self.titlePlaceholder = placeholderLabel.text
            } else {
                self.titlePlaceholder = self.placeholder
            }
            
            self.placeholder = ""
            
            var frame = placeholderLabel.frame
            frame.origin.y = self.frame.origin.y
            UIView.animate(withDuration: animationDuration, animations: {
                placeholderLabel.text = self.titlePlaceholder
                placeholderLabel.font = UIFont.systemFont(ofSize: 14)
                placeholderLabel.textColor = self.subjectColor
                
                placeholderLabel.frame.origin.y = frame.origin.y - (placeholderLabel.frame.size.height) + 5 //+ (self.frame.size.height / 2 - frame.size.height / 2)
            }, completion: { (isComplete) in
                
            })
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        
        
        if let placeholderLabel = self.placeholderLabel, self.text == "" {
            //let frame = placeholderLabel.frame
            UIView.animate(withDuration: animationDuration, animations: {
                
                placeholderLabel.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.size.width, height: 15.0)
                placeholderLabel.alpha = 1
                //placeholderLabel.textColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0.0980392, alpha: 0.22)
                if #available(iOS 10.0, *) {
                    placeholderLabel.textColor = UIColor(displayP3Red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
                } else {
                    // Fallback on earlier versions
                }
                
                
                if let pointSize = self.font?.pointSize {
                    placeholderLabel.font = UIFont.systemFont(ofSize: pointSize)
                }
                
            }, completion: { (isComplete) in
                self.placeholder = self.titlePlaceholder
                
                placeholderLabel.alpha = 0
            })
        }
    }
    
    
    
    
    
}
